#ifndef HX711_H
#define HX711_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

uint8_t _hx711_average_times = 3;
uint8_t _hx711_interval_ms = 1;
uint8_t _hx711_gain = 1;
uint32_t _hx711_scale = 1;
uint32_t _hx711_offset = 0;

bool HX711_initialize(
        void (*delayUs)(uint16_t),
        void (*disableInterrupts)(void),
        void (*reEnableInterrupts)(void),
        bool (*read_data)(void),
        void (*write_clock)(bool)
);

bool HX711_isReady();

void HX711_waitUntilReady();

void HX711_setGain(uint8_t gain);

void HX711_setScale(uint32_t scale);

void HX711_setOffset(uint32_t offset);

void HX711_setAverageTimes(int8_t times);

uint32_t HX711_read();

uint32_t HX711_readAverage(uint8_t times, uint16_t delay);

void HX711_tare();

uint32_t HX711_readComplete();

void HX711_powerUp();

void HX711_down();

uint32_t HX711_convert(uint32_t value);

#endif    /* HX711_H */

