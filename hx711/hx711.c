#include "hx711.h"

void (*_hx711_delayUs)(uint16_t);

void (*_hx711_disableInterrupts)(void);

void (*_hx711_reEnableInterrupts)(void);

bool (*_hx711_readData)(void);

void (*_hx711_writeClock)(bool);

bool HX711_initialize(
        void (*delayUs)(uint16_t),
        void (*disableInterrupts)(void),
        void (*reEnableInterrupts)(void),
        bool (*read_data)(void),
        void (*write_clock)(bool)
) {
    _hx711_delayUs = delayUs;
    _hx711_disableInterrupts = disableInterrupts;
    _hx711_reEnableInterrupts = reEnableInterrupts;
    _hx711_readData = read_data;
    _hx711_writeClock = write_clock;
    return true;
}

bool HX711_isReady() {
    return _hx711_readData() == 0;
}

void HX711_waitUntilReady() {
    while (!HX711_isReady()) _hx711_delayUs(100);
}

void HX711_setGain(uint8_t gain) {
    if (gain == 128) _hx711_gain = 1;
    else if (gain == 64) _hx711_gain = 3;
    else if (gain == 32) _hx711_gain = 2;
}

void HX711_setScale(uint32_t scale) {
    _hx711_scale = scale;
}

void HX711_setOffset(uint32_t offset) {
    _hx711_offset = offset;
}

void HX711_setAverageTimes(int8_t times) {
    _hx711_average_times = times;
}

uint32_t HX711_read() {
    HX711_waitUntilReady();
    uint32_t value = 0u;
    uint8_t filler = 0x00u;

    // Protect the read sequence from system interrupts.
    _hx711_disableInterrupts();

    // Pulse the clock pin 24 times to read the data.
    for (uint8_t i = 0; i < 24; i++) {
        _hx711_writeClock(1);
        value <<= 1u;
        _hx711_writeClock(0);
        if (_hx711_readData()) value++;
    }

    // Set the channel and the gain factor for the next reading using the clock pin.
    for (unsigned int i = 0; i < _hx711_gain; i++) {
        _hx711_writeClock(1);
        _hx711_delayUs(1);
        _hx711_writeClock(0);
        _hx711_delayUs(1);
    }

    _hx711_reEnableInterrupts();

//    if (value & 0x800000u) filler = 0xFF;
//    else filler = 0x00;
//    return ((uint32_t)(filler) << 24u) | value;
    return value;
}

uint32_t HX711_readAverage(uint8_t times, uint16_t delay) {
    uint32_t sum = 0;
    for (uint8_t i = 0; i < times; i++) {
        sum += HX711_read();
        _hx711_delayUs(delay);
    }
    sum = sum / times;
    if (sum & 0x800000u) return ((uint32_t) (0xFF) << 24u) | sum;
    return sum;
}

uint32_t HX711_readComplete() {
    return HX711_convert(HX711_readAverage(_hx711_average_times, _hx711_interval_ms * 1000));
}

uint32_t HX711_convert(uint32_t input_value) {
    int32_t value = ((int32_t) (input_value - _hx711_offset)) / (int32_t) (_hx711_scale);
    return (uint32_t) (value);
}

void HX711_tare() {
    HX711_setOffset(HX711_readAverage(10, 1000));
}

void HX711_powerUp() {
    _hx711_writeClock(0);
    _hx711_delayUs(1);
}

void HX711_down() {
    _hx711_writeClock(0);
    _hx711_delayUs(1);
    _hx711_writeClock(1);
    _hx711_delayUs(1);
}
