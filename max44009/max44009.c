#include "max44009.h"

uint8_t (*_max4009_read)(uint8_t, uint8_t);

void (*_max4009_write)(uint8_t, uint8_t, uint8_t);

void (*_max4009_delayMs)(uint16_t);

uint8_t _max44009_address;


void MAX44009_initialize(uint8_t address,
                         uint8_t (*read)(uint8_t, uint8_t),
                         void (*write)(uint8_t, uint8_t, uint8_t),
                         void (*delayMs)(uint16_t)) {
    _max4009_read = read;
    _max4009_write = write;
    _max4009_delayMs = delayMs;
    _max44009_address = address;
}

/**
 * Read a raw base value from MAX44009.
 * @return Raw base reading, without converting it to Lux
 */
uint32_t _MAX44009_readRaw() {
    uint8_t hbr = _max4009_read(_max44009_address, MAX44009_LUX_READING_HIGH);
    uint8_t lbr = _max4009_read(_max44009_address, MAX44009_LUX_READING_LOW);
    return _MAX44009_convertToBase(hbr, lbr);
}

/**
 * Returns an average reading of the Lux value, waiting a specific amount of MS between readings.
 * @param times The number of times to average
 * @param interval The interval, in MS, to wait between readings.
 * @return the value in Lux of the reading, as Lux * 100
 */
uint32_t _MAX44009_read(uint8_t times, uint8_t interval) {
    if (times == 0) times = 1;
    if (times > 100) times = 100;
    uint64_t acc = 0;
    for (uint8_t i = 0; i < times; i++) {
        acc += _MAX44009_readRaw();
        if (interval) _max4009_delayMs(interval);
    }
    return _MAX44009_convertToLux((uint32_t)(acc / times));
}

/**
 * Converts the raw output from the high and low registers of the MAX44009 to a small base value, suitable to
 * be accumulated and later being converted into Lux. It is effectively returning 2^exponent * mantissa.
 *
 * As per the datasheet, HBR[7:4] contains the exponent and HBR[3:0] the four most significant bits of the mantissa,
 * while LBR[3:0] contains the 4 least significant bits of the mantissa.
 *
 * The max value would be ((1 << 15)*255), and therefore 23 bits.
 *
 * @param data_high High-Byte Register (HBR) reading.
 * @param data_low Low-Byte Register (LBR) reading.
 * @return the base value, from 0 to 8355840
 */
uint32_t _MAX44009_convertToBase(uint8_t data_high, uint8_t data_low) {
    uint8_t exponent = data_high >> 4;
    uint8_t mantissa = ((data_high & 0x0F) << 4) + (data_low & 0x0F);
    return (((uint32_t) 1u) << ((uint32_t) exponent)) * ((uint32_t) mantissa);
}

/**
 * Returns an integer representing Lux * 1000, corrected with global offset.
 * The datasheet states that Lux = 2^exp * man * 0.045. So this transformation must be base * 100 / 0.45.
 * To make keep it integer, we are doing base * 100000 / 22222
 * @param base The base value obtained on _MAX4409_convertToBase  from HBR and LBR
 * @return The corrected value in Lux
 */
uint32_t _MAX44009_convertToLux(uint32_t base) {
    uint64_t full_result = base;
    full_result *= 100000;
    full_result /= 22222;
    full_result = full_result ;
    return (uint32_t) full_result + _max44009_offset;
}

void _MAX44009_setInterrupt(bool status) {
    _max4009_write(_max44009_address, MAX44009_INTERRUPT_ENABLE, status ? 0 : 1);
}

void MAX44009_setContinuous(bool value) {
    uint8_t current = _max4009_read(_max44009_address, MAX44009_CONFIGURATION);
    _max4009_write(_max44009_address, MAX44009_CONFIGURATION, SET_FLAG(current, value, MAX44009_CFG_CONTINUOUS));
}

void MAX44009_setManualMode(bool cdr, max44009_tim tim) {
    if (cdr != 0) cdr = 1;    // only 0 or 1
    if (tim > 7) tim = 7;
    uint8_t config = _max4009_read(_max44009_address, MAX44009_CONFIGURATION);
    config |= MAX44009_CFG_MANUAL;
    config &= 0xF0;                     // clear old CDR & TIM bits
    config |= cdr << 3 | tim;           // set new CDR & TIM bits
    _max4009_write(_max44009_address, MAX44009_CONFIGURATION, config);
}

void MAX44009_setAutomaticMode() {
    // CDR & TIM cannot be written in automatic mode
    uint8_t config = _max4009_read(_max44009_address, MAX44009_CONFIGURATION);
    config &= ~MAX44009_CFG_MANUAL;
    _max4009_write(_max44009_address, MAX44009_CONFIGURATION, config);
}

void MAX44009_setThreshold(uint8_t threshold, uint32_t value) {

}
