#ifndef PIC8_RANDOM_BOX_MAX44009_H
#define PIC8_RANDOM_BOX_MAX44009_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#define SET_FLAG(FULL, VALUE, FLAG) (VALUE ? (FULL | FLAG) : (FULL & (~FLAG)))

// REGISTERS
#define MAX44009_INTERRUPT_STATUS   0x00
#define MAX44009_INTERRUPT_ENABLE   0x01
#define MAX44009_CONFIGURATION      0x02
#define MAX44009_LUX_READING_HIGH   0x03
#define MAX44009_LUX_READING_LOW    0x04
#define MAX44009_THRESHOLD_HIGH     0x05
#define MAX44009_THRESHOLD_LOW      0x06
#define MAX44009_THRESHOLD_TIMER    0x07


// CONFIGURATION MASKS
#define MAX44009_CFG_CONTINUOUS     0x80
#define MAX44009_CFG_MANUAL         0x40
#define MAX44009_CFG_CDR            0x08
#define MAX44009_CFG_TIMER          0x07

typedef enum {
    MAX44009_TIM_800MS = 0,
    MAX44009_TIM_400MS = 1,
    MAX44009_TIM_200MS = 2,
    MAX44009_TIM_100MS = 3,
    MAX44009_TIM_50MS = 4,
    MAX44009_TIM_25MS = 5,
    MAX44009_TIM_12_5MS = 6,
    MAX44009_TIM_6_25MS = 7,
} max44009_tim;

uint8_t _max44009_offset = 0;

void MAX44009_initialize(uint8_t address,
                         uint8_t (*read)(uint8_t, uint8_t),
                         void (*write)(uint8_t, uint8_t, uint8_t),
                         void (*delayMs)(uint16_t));

uint32_t _MAX44009_readRaw();

uint32_t _MAX44009_read(uint8_t times, uint8_t interval);

uint32_t _MAX44009_convertToBase(uint8_t data_high, uint8_t data_low);

uint32_t _MAX44009_convertToLux(uint32_t base);

/* Configurations */

void _MAX44009_setInterrupt(bool status);

void MAX44009_setContinuous(bool value);

void MAX44009_setManualMode(bool cdr, max44009_tim tim);

void MAX44009_setAutomaticMode();

void MAX44009_setThreshold(uint8_t threshold, uint32_t value);

#endif //PIC8_RANDOM_BOX_MAX44009_H
