#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include "bme280.h"

void (*_bme280_read)(uint8_t, void *, size_t);

void (*_bme280_write)(uint8_t, void *, size_t);

void (*_bme280_delay_ms)(uint16_t);

void BME280_initialize(
        void (*read)(uint8_t, void *, size_t),
        void (*write)(uint8_t, void *, size_t),
        void (*delay)(uint16_t)) {
    _bme280_read = read;
    _bme280_write = write;
    _bme280_delay_ms = delay;
}

bool BME280_setup() {
    if (!BME280_checkID()) return false;
    
    // BME280_writeRegister(BME280_RST_REG, 0xB6); // Soft reset

    _bme280_delay_ms(100);
    
    // Wait for calibration to end
    while(BME280_readRegister8(BME280_STAT_REG) & 1 != 0) _bme280_delay_ms(10);
    
    BME280_readCalibrationData();
    
    BME280_writeRegister(BME280_CTRL_MEAS_REG, 0x00); // Sleep  
    
    BME280_writeRegister(BME280_CTRL_HUMIDITY_REG, 0b00000101); // ? RE  
    BME280_writeRegister(BME280_CONFIG_REG, 0b0000000); // ? RE
    BME280_writeRegister(BME280_CTRL_MEAS_REG, 0b10110111); // ? RE
    _bme280_delay_ms(100);
    
    return true;
}

void BME280_readCalibrationData() {
    _bme280_calibration.dig_T1 = ((uint16_t)((BME280_readRegister8(BME280_DIG_T1_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_T1_LSB_REG)));
	_bme280_calibration.dig_T2 = ((int16_t)((BME280_readRegister8(BME280_DIG_T2_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_T2_LSB_REG)));
	_bme280_calibration.dig_T3 = ((int16_t)((BME280_readRegister8(BME280_DIG_T3_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_T3_LSB_REG)));

	_bme280_calibration.dig_P1 = ((uint16_t)((BME280_readRegister8(BME280_DIG_P1_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P1_LSB_REG)));
	_bme280_calibration.dig_P2 = ((int16_t)((BME280_readRegister8(BME280_DIG_P2_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P2_LSB_REG)));
	_bme280_calibration.dig_P3 = ((int16_t)((BME280_readRegister8(BME280_DIG_P3_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P3_LSB_REG)));
	_bme280_calibration.dig_P4 = ((int16_t)((BME280_readRegister8(BME280_DIG_P4_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P4_LSB_REG)));
	_bme280_calibration.dig_P5 = ((int16_t)((BME280_readRegister8(BME280_DIG_P5_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P5_LSB_REG)));
	_bme280_calibration.dig_P6 = ((int16_t)((BME280_readRegister8(BME280_DIG_P6_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P6_LSB_REG)));
	_bme280_calibration.dig_P7 = ((int16_t)((BME280_readRegister8(BME280_DIG_P7_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P7_LSB_REG)));
	_bme280_calibration.dig_P8 = ((int16_t)((BME280_readRegister8(BME280_DIG_P8_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P8_LSB_REG)));
	_bme280_calibration.dig_P9 = ((int16_t)((BME280_readRegister8(BME280_DIG_P9_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_P9_LSB_REG)));

	_bme280_calibration.dig_H1 = ((uint8_t)(BME280_readRegister8(BME280_DIG_H1_REG)));
	_bme280_calibration.dig_H2 = ((int16_t)((BME280_readRegister8(BME280_DIG_H2_MSB_REG) << 8) + BME280_readRegister8(BME280_DIG_H2_LSB_REG)));
	_bme280_calibration.dig_H3 = ((uint8_t)(BME280_readRegister8(BME280_DIG_H3_REG)));
	_bme280_calibration.dig_H4 = ((int16_t)((BME280_readRegister8(BME280_DIG_H4_MSB_REG) << 4) + (BME280_readRegister8(BME280_DIG_H4_LSB_REG) & 0x0F)));
	_bme280_calibration.dig_H5 = ((int16_t)((BME280_readRegister8(BME280_DIG_H5_MSB_REG) << 4) + ((BME280_readRegister8(BME280_DIG_H4_LSB_REG) >> 4) & 0x0F)));
	_bme280_calibration.dig_H6 = ((int8_t)BME280_readRegister8(BME280_DIG_H6_REG));
}

void BME280_writeRegister(uint8_t reg, uint8_t value) {
    uint8_t data[2] = { reg, value };
    _bme280_write(BME280_ADDRESS, data, 2);
}

uint8_t BME280_readRegister8(uint8_t reg) {
    uint8_t reg_array[1] = { reg };
    _bme280_write(BME280_ADDRESS, reg_array, 1);
    reg_array[0] = 0;
    _bme280_read(BME280_ADDRESS, &reg_array, 1);
    return reg_array[0];
}

bool BME280_checkID() {
    return BME280_readRegister8(BME280_ID_REG) == 0x60;
}

int32_t BME280_readTemperature() {
    uint8_t buffer[3] = { BME280_TEMPERATURE_MSB_REG, 0,0 };
    _bme280_write(BME280_ADDRESS, buffer, 1);
    _bme280_read(BME280_ADDRESS, &buffer, 3);
    int32_t adc_T = ((uint32_t)buffer[0] << 12) | ((uint32_t)buffer[1] << 4) | ((buffer[2] >> 4) & 0x0F);
    int64_t var1, var2;
    int32_t t_fine;
    
	var1 = ((((adc_T>>3) - ((int32_t)_bme280_calibration.dig_T1<<1))) * ((int32_t)_bme280_calibration.dig_T2)) >> 11;
	var2 = (((((adc_T>>4) - ((int32_t)_bme280_calibration.dig_T1)) * ((adc_T>>4) - ((int32_t)_bme280_calibration.dig_T1))) >> 12) *
	((int32_t)_bme280_calibration.dig_T3)) >> 14;
	t_fine = var1 + var2;
    int32_t output = (t_fine * 5 + 128) >> 8;
	return output + bme280_temp_offset;
}

uint16_t BME280_readHumidity() {
    uint8_t buffer[2] = { BME280_HUMIDITY_MSB_REG, 0 };
    _bme280_write(BME280_ADDRESS, buffer, 1);
    _bme280_read(BME280_ADDRESS, &buffer, 2);
    int32_t adc_H = ((uint24_t)buffer[0] << 8) | ((uint24_t)buffer[1]);
    int32_t var1;

	var1 = (_bme280_t_fine - ((int32_t)76800));
	var1 = (((((adc_H << 14) - (((int32_t)_bme280_calibration.dig_H4) << 20) - (((int32_t)_bme280_calibration.dig_H5) * var1)) +
	((int32_t)16384)) >> 15) * (((((((var1 * ((int32_t)_bme280_calibration.dig_H6)) >> 10) * (((var1 * ((int32_t)_bme280_calibration.dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) *
	((int32_t)_bme280_calibration.dig_H2) + 8192) >> 14));
	var1 = (var1 - (((((var1 >> 15) * (var1 >> 15)) >> 7) * ((int32_t)_bme280_calibration.dig_H1)) >> 4));
	var1 = (var1 < 0 ? 0 : var1);
	var1 = (var1 > 419430400 ? 419430400 : var1);

	return (uint16_t)(100 * (var1 >> 12) / 1024);
}
