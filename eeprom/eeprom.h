#ifndef PIC8_RANDOM_BOX_EEPROM_H
#define PIC8_RANDOM_BOX_EEPROM_H


#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

uint8_t EEPROM_Read(uint16_t address);

void EEPROM_Write(uint16_t address, uint8_t data);

#ifdef __cplusplus
}
#endif
#endif //PIC8_RANDOM_BOX_EEPROM_H
