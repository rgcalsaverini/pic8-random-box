#include "eeprom.h"

#ifdef __XC8
#else
#define __nop()

/* Mock registers for testing outside XC8 */
uint8_t NVMADRL;
uint8_t NVMADRH;
uint8_t NVMDAT;
uint8_t NVMCON2;


typedef struct {
    uint8_t WREN;
    uint8_t NVMREG;
    uint8_t WR;
    uint8_t RD;
} NVMCON1bitsTP;

NVMCON1bitsTP NVMCON1bits;
#endif


void EEPROM_Write(uint16_t address, uint8_t data) {
    // Set target address
    NVMADRL = (uint8_t) (address & 0xFF);     // Low byte of address
    NVMADRH = (uint8_t) ((address >> 8) & 0xFF); // High byte of address (if applicable)

    // Set data to write
    NVMDAT = data;

    // Enable writes
    NVMCON1bits.WREN = 1;       // Enable write operations
    NVMCON1bits.NVMREG = 0b00;  // Access Data EEPROM memory

    // Perform unlock sequence
    NVMCON2 = 0x55;             // Write 0x55 to NVMCON2
    NVMCON2 = 0xAA;             // Write 0xAA to NVMCON2
    NVMCON1bits.WR = 1;         // Set WR bit to start the write operation
    __nop();                    // Add a NOP for timing stability (optional)
    __nop();

    // Wait for write completion (WR bit will clear automatically)
    while (NVMCON1bits.WR);

    // Disable writes
    NVMCON1bits.WREN = 0;       // Clear WREN bit to prevent accidental writes
}

uint8_t EEPROM_Read(uint16_t address) {
    // Set target address
    NVMADRL = (uint8_t) (address & 0xFF);     // Low byte of address
    NVMADRH = (uint8_t) ((address >> 8) & 0xFF); // High byte of address (if applicable)

    // Select EEPROM memory
    NVMCON1bits.NVMREG = 0b00;  // Access Data EEPROM memory

    // Initiate read
    NVMCON1bits.RD = 1;         // Set RD bit to start the read operation
    __nop();                    // Add a NOP for timing stability (optional)
    __nop();

    // Return data from NVMDAT
    return NVMDAT;
}
