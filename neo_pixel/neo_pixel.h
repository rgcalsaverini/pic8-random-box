#ifndef PIC8_RANDOM_BOX_NEO_PIXEL_H
#define PIC8_RANDOM_BOX_NEO_PIXEL_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_LEDS 8
#define MAX_SEGMENTS MAX_LEDS * 24 * 3

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

void neopx_timerStart(void);
void neopx_timerStop(void);

void neopx_setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t led_number);
void neopx_send(void);
void neopx_timerISR(void);


#ifdef __cplusplus
}
#endif
#endif //PIC8_RANDOM_BOX_NEO_PIXEL_H
