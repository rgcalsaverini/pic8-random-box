#include "neo_pixel.h"

#ifdef __XC8
#include "/Users/rui/personal/projects/electronics/neo_pixel/neo_pixel.X/mcc_generated_files/system/system.h"
#else

/* These definitions are only so I can mock them to run unit-tests and linters on non XC environments */
#define __delay_us(T)

typedef struct MockLatBitsStr {
    uint8_t LATC0;
} MockLatBits;

typedef struct MockPIR0bitsStr {
    uint8_t TMR0IF;
} MockPIR0bits;

typedef struct MockPIE0bitsStr {
    uint8_t TMR0IE;
} MockPIE0bits;

typedef struct MockT0CON0bitsStr {
    uint8_t T0EN;
} MockT0CON0bits;


MockLatBits LATCbits;
MockPIR0bits PIR0bits;
MockT0CON0bits T0CON0bits;

#endif

/*
 NeoPixel expects the long and short pulses to be 0.85µs and 0.4µs respectively.
 By setting a clock interrupt for 0.4121µs and treating the clock to be 3 segments long,
 we can have each one of those 3 segments set to 1 or 0, and approximate that with only 3% error.

 To ensure precise timing, we pre-calculate the segments and just blindly propagate them on the ISR.

 Since each LED has a 24-bit color, and each bit has 3 segments, we need MAX_LEDS * 24 * 3 bytes of memory.
 */
uint8_t neopx_pwm_segments[MAX_SEGMENTS];

/*
 Sets the 3 segments of the PWM based on the bit value. Considering the reasoning above,
 a low bit would be codified into segments 100 and a high bit into 110.
 */
#define SET_3_SEGS(BIT, CONTAINER, IDX) \
    CONTAINER[IDX++] = 1; \
    CONTAINER[IDX++] = (BIT) ? 1 : 0; \
    CONTAINER[IDX++] = 0 \


uint16_t neopx_send_idx;

void neopx_timerStart(void) {
    T0CON0bits.T0EN = 1;
}

void neopx_timerStop(void) {
    T0CON0bits.T0EN = 0;
}

void neopx_setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t led_number) {
    uint16_t idx = led_number * 24 * 3;

    for (int i = 7; i >= 0; i--) {
        SET_3_SEGS((g >> i) & 1, neopx_pwm_segments, idx);
    }
    for (int i = 7; i >= 0; i--) {
        SET_3_SEGS((r >> i) & 1, neopx_pwm_segments, idx);
    }
    for (int i = 7; i >= 0; i--) {
        SET_3_SEGS((b >> i) & 1, neopx_pwm_segments, idx);
    }
}

void neopx_send(void) {
    neopx_send_idx = 0;
    neopx_timerStart();
    while (neopx_send_idx < MAX_SEGMENTS - 1) __asm__ volatile ("nop; nop;"); // Wait for transmission to finish
    __delay_us(100); // Hold down line for reset
}

void neopx_timerISR(void) {
    PIR0bits.TMR0IF = 0; // Clear interrupt flag
    LATCbits.LATC0 = neopx_pwm_segments[neopx_send_idx++];
    if (neopx_send_idx == MAX_SEGMENTS - 1) {
        LATCbits.LATC0 = 0;
        neopx_timerStop();
    }
    PIR0bits.TMR0IF = 0;
}
