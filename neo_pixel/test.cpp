#include <gtest/gtest.h>
#include "neo_pixel.h"
#include <string>
#include <map>
#include <utility>


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}