#ifndef PIC8_RANDOM_BOX_SI5351_H
#define PIC8_RANDOM_BOX_SI5351_H
#ifdef __cplusplus
extern "C" {
#endif

#define SI5351_ADDRESS 0x60
#define SI5351_REG_0_DEVICE_STATUS         0x00
#define SI5351_REG_3_OUTPUT_ENABLE_CONTROL 0x03

typedef enum {
    SI5351_READY = 0,
    SI5351_LOCK_LOSS = 1,
    SI5351_INITIALIZING = 2,
    SI5351_LOSS_OF_SIGNAL = 3,
} si5351_status;

typedef enum {
    SI5351_REV_A = 0,
    SI5351_REV_B = 1,
    SI5351_REV_C = 2,
    SI5351_REV_UNKNOWN = 3,

} si5351_revision;

void Si5351_initialize(uint8_t address, uint8_t (*read)(uint8_t, uint8_t), void (*write)(uint8_t, uint8_t, uint8_t));

si5351_status Si5351_get_status(void);

si5351_revision Si5351_get_device_revision(void);

void Si5351_disable_all_outputs(void);

void Si5351_set_output_state(void);

#ifdef __cplusplus
}
#endif
#endif //PIC8_RANDOM_BOX_SI5351_H
