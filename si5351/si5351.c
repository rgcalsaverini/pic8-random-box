#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include "si5351.h"

#define IS_BIT_SET(value, BT) ((value & (1 << BT)) != 0)

uint8_t (*si5351_read)(uint8_t, uint8_t);

void (*si5351_write)(uint8_t, uint8_t, uint8_t);

uint8_t si5351_address;

void Si5351_initialize(uint8_t address, uint8_t (*read)(uint8_t, uint8_t), void (*write)(uint8_t, uint8_t, uint8_t)) {
    si5351_address = address;
    si5351_read = read;
    si5351_write = write;
}

si5351_status Si5351_get_status(void) {
    uint8_t status = si5351_read(si5351_address, SI5351_REG_0_DEVICE_STATUS);
    uint8_t rev = status & 0x3;
    if (IS_BIT_SET(status, 7)) return SI5351_INITIALIZING;
    if (rev != SI5351_REV_B) {
        if (IS_BIT_SET(status, 6) || IS_BIT_SET(status, 5)) return SI5351_LOCK_LOSS;
    } else {
        // RevB has no PLLB
        if (IS_BIT_SET(status, 5)) return SI5351_LOCK_LOSS;
    }
    if (rev == SI5351_REV_C && IS_BIT_SET(status, 4)) return SI5351_LOSS_OF_SIGNAL;
    return SI5351_READY;
}

si5351_revision Si5351_get_device_revision(void) {
    uint8_t status = si5351_read(si5351_address, SI5351_REG_0_DEVICE_STATUS);
    return status & 0x3;
}

void Si5351_disable_all_outputs(void) {
    si5351_write(si5351_address, SI5351_REG_3_OUTPUT_ENABLE_CONTROL, 0xFF);
}