#include <gtest/gtest.h>
#include <vector>
#include <set>
#include "si5351.h"

uint8_t last_register = 0x00;
std::set<uint8_t> called_addresses;
std::vector<uint8_t> mock_memory(256, 0x00);

void reset_tests () {
    called_addresses.clear();
    mock_memory = std::vector<uint8_t>(256, 0x00);
    last_register = 0x00;
}

TEST(Si5351Test, DisableAllOutputs) {
    reset_tests();
    Si5351_disable_all_outputs();

    EXPECT_EQ(called_addresses.size(), 1);
    EXPECT_TRUE(called_addresses.contains(SI5351_ADDRESS));
    EXPECT_EQ(mock_memory[0x03], 0xFF);
}

TEST(Si5351Test, DeviceStatus) {
    reset_tests();
    mock_memory[0] = 0;
    EXPECT_EQ(Si5351_get_status(), SI5351_READY);
    mock_memory[0] = 0x80;
    EXPECT_EQ(Si5351_get_status(), SI5351_INITIALIZING);
    // Rev. A, loss of PLLA and PLLB lock
    mock_memory[0] = 0x60;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. A, loss of PLLB lock
    mock_memory[0] = 0x40;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. A, loss of PLLA lock
    mock_memory[0] = 0x20;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. B, loss of PLLA and PLLB lock
    mock_memory[0] = 0x61;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. B, loss of PLLB lock means nothing to this device
    mock_memory[0] = 0x41;
    EXPECT_EQ(Si5351_get_status(), SI5351_READY);
    // Rev. B, loss of PLLA lock
    mock_memory[0] = 0x21;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. C, loss of PLLA and PLLB lock
    mock_memory[0] = 0x62;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. C, loss of PLLB lock
    mock_memory[0] = 0x42;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOCK_LOSS);
    // Rev. C, loss of PLLA lock
    mock_memory[0] = 0x22;
    // Rev. C, loss of signal
    mock_memory[0] = 0x12;
    EXPECT_EQ(Si5351_get_status(), SI5351_LOSS_OF_SIGNAL);

    EXPECT_EQ(called_addresses.size(), 1);
    EXPECT_TRUE(called_addresses.contains(SI5351_ADDRESS));
}

TEST(Si5351Test, DeviceRevision) {
    reset_tests();
    mock_memory[0] = 0;
    EXPECT_EQ(Si5351_get_device_revision(), SI5351_REV_A);
    mock_memory[0] = 1;
    EXPECT_EQ(Si5351_get_device_revision(), SI5351_REV_B);
    mock_memory[0] = 2;
    EXPECT_EQ(Si5351_get_device_revision(), SI5351_REV_C);

    EXPECT_EQ(called_addresses.size(), 1);
    EXPECT_TRUE(called_addresses.contains(SI5351_ADDRESS));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    Si5351_initialize(
            SI5351_ADDRESS,
            [](uint8_t addr, uint8_t reg) {
                called_addresses.insert(addr);
                last_register = addr;
                return mock_memory[reg];
            },
            [](uint8_t addr, uint8_t reg, uint8_t value) {
                called_addresses.insert(addr);
                last_register = reg;
                mock_memory[reg] = value;
            }
    );
    return RUN_ALL_TESTS();
}