# PIC8 Random Box

```shell script
g++ -I. -I/opt/microchip/xc8/v2.30/pic/include -I. test.cpp eeprom/test.cpp eeprom/eeprom.c -o test -lgtest -lpthread
```

A random collection of PIC8 libraries that I had to write for projects, most adapted from other sources like from Arduinoand other MCUs.

**Contents:** 
- ESP8266 WiFi Module
- HX711 Load Cell Amplifier
- BME280 Humidity + Barometric Pressure + Temperature Sensor

Add it with:
```shell script
git submodule add git://gitlab.com/rgcalsaverini/pic8-random-box.git external/pic8-libs
```

## Testing

```
rm -rf build/*
cmake -B build
cmake --build build
build/test_si5351
```

## ESP-8266

### Using

Create the callback functions to properly use the lib:

**application.h**

```c
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "mcc_generated_files/device_config.h"
#define APP_ESP_POWER_PIN RA4

void APP_initialize(void);

void APP_UART0Write(char ch);

char APP_UART0Read();

void APP_switchESP(bool state);

bool APP_sendData(uint8_t *data, size_t len);
```

**application.c**

```c
#include <string.h>
#include "application.h"
#include "esp8266.h"
#include "mcc_generated_files/drivers/uart.h"
#include "mcc_generated_files/pin_manager.h"

void APP_initialize(void) {
    ESP8266_initialize(app_port, APP_UART0Read, APP_UART0Write, APP_switchESP);
}

void APP_UART0Write(char ch) {
    uart[UART0].Write(ch);
}

char APP_UART0Read() {
    return uart[UART0].Read();
}

void APP_switchESP(bool state) {
    APP_ESP_POWER_PIN = state ? 1 : 0;
}

bool APP_sendData(uint8_t *data, size_t len) {
    ESP8266_enable();
    if (!ESP8266_connect("SSID", "password", false)) return false;
    if (ESP8266_HTTP("POST", app_hostname, app_data_path, data, len) != 200) return false;
    return true;
}

```