#include "ha.h"
#include <string.h>


#ifdef __XC8
#include "eeprom.h"

#else

#include "eeprom/eeprom.h"

#endif



uint8_t mqtt_connect_pkg[14] = {0x10, 0x0C, 0x00, 0x04, 0x4D, 0x51, 0x54, 0x54, 0x04, 0x02, 0x00, 0x3C, 0x00, 0x00};
uint8_t mqtt_connack_pkg[4] = {0x20, 0x02, 0x00, 0x00};
uint8_t mqtt_pingreq_pkg[2] = {0xC0, 0x00};
uint8_t mqtt_pingresp_pkg[2] = {0xD0, 0x00};
uint8_t mqtt_disconnect_pkg[2] = {0xE0, 0x00};
uint8_t at_entropy[2] = {0xA5, 0x5A};

// Interface with the MCU

char (*ha_getCh)(void);

void (*ha_putCh)(char);

void (*ha_setReset)(bool);

void (*ha_delayUs)(uint16_t);

bool (*ha_isRxReady)(void);

bool (*ha_isTxReady)(void);

/*** BASIC UART INTERFACE ***/

void ha_initialize(char (*getCh)(void),
                   bool (*rxReady)(void),
                   void (*putCh)(char),
                   bool (*txReady)(void),
                   void (*setReset)(bool),
                   void (*delayUs)(uint16_t)) {
    ha_getCh = getCh;
    ha_putCh = putCh;
    ha_setReset = setReset;
    ha_delayUs = delayUs;
    ha_isRxReady = rxReady;
    ha_isTxReady = txReady;
}

bool ha_setupWifi(void) {
    ha_setReset(0);
    ha_delayMs(100);
    ha_setReset(1);
    ha_consumeReady();

    // Overall settings
    ha_sendUartCommand("AT+CIPSERVER=0"); // Disable server, this one can fail.
    ha_sendUartCommand("AT+CIPCLOSE"); // Disable server, this one can fail.
    ha_delayMs(100);
    ha_clearBuffer();

    bool success = ha_sendUartCommand("ATE1") // Turn echo off
                   && ha_sendUartCommand("AT+CIPMUX=0") // Single connection mode
                   && ha_sendUartCommand("AT+CIPMODE=0") // Normal Wi-Fi mode
                   && ha_sendUartCommand("AT+CWMODE=1") // Soft AP mode
                   && ha_sendUartCommand("AT+CIPDINFO=0") // Show less info on incoming packages
                   && ha_sendUartCommand("AT+CWMODE_CUR=1"); // Go into station mode
    ha_delayMs(500);
    return success;
}

void ha_delayMs(uint16_t ms) {
    while (ms > 1000) {
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ha_delayUs(50000);
        ms -= 1000;
    }
    while (ms > 100) {
        ha_delayUs(50000);
        ha_delayUs(50000);
        ms -= 100;
    }
    while (ms > 10) {
        ha_delayUs(10000);
        ms -= 10;
    }

    while (ms > 1) {
        ha_delayUs(1000);
        ms -= 1;
    }
}

bool ha_waitForTx(void) {
    for (uint16_t i = 0;; i++) {
        if (ha_isTxReady()) break;
        if (i > HA_UART_TX_RETRY_TIMES) return false;
        ha_delayUs(10);
    }
    return ha_isTxReady();
}

bool ha_waitForRx(void) {
    for (uint16_t i = 0;; i++) {
        if (ha_isRxReady()) break;
        if (i > HA_UART_RX_RETRY_TIMES) return false;
        ha_delayUs(10);
    }
    return ha_isRxReady();
}

void ha_clearBuffer(void) {
    while (ha_waitForRx()) ha_getCh();
}

bool ha_sendUartRaw(const char *str) {
    return ha_sendUartRawLen(str, strlen(str));
}

bool ha_sendUartRawLen(const char *str, uint16_t len) {
    for (uint16_t count = 0; count < len; count++) {
        if (!ha_waitForTx()) return false;
        ha_putCh(str[count]);
    }
    return true;
}

bool ha_sendUartCommand(const char *str) {
    ha_sendUartRaw(str);
    ha_sendUartRawLen("\r\n", 2);
    for (uint8_t i = 0; i < 100; i++) {
        uint8_t status = ha_consume2Sequence("OK", "ERROR");
        if (status == 1) return true;
        if (status == 2) return false;
        ha_delayMs(10);
    }
    return false;
}

bool ha_consumeSequence(const char *sequence) {
    return ha_consumeSequenceWithLen(sequence, strlen(sequence));
}

bool ha_consumeReady(void) {
    uint16_t progress = 0;
    char ch;
    char sequence[6] = "ready";
    uint8_t len = 5;

    while (true) {
        if (!ha_waitForRx()) return false;
        ch = ha_getCh();

        if (ch == sequence[progress]) progress++;
        else if (ch == sequence[0]) progress = 1;
        else {
            progress = 0;
            at_entropy[0] ^= ch;
            at_entropy[1] ^= ch >> 1;
        }
        if (progress == len) return true;
    }
}

bool ha_consumeSequenceWithLen(const char *sequence, uint16_t len) {
    uint16_t progress = 0;
    char ch;

    while (true) {
        if (!ha_waitForRx()) return false;
        ch = ha_getCh();
        if (ch == sequence[progress]) progress++;
        else if (ch == sequence[0]) progress = 1;
        else progress = 0;
        if (progress == len) return true;
    }
}

bool ha_readUntilDifferent(char *output_ch, const char *ignored) {
    uint8_t len = strlen(ignored);

    while (true) {
        if (!ha_isRxReady()) return false;
        *output_ch = ha_getCh();
        bool is_ignored = false;
        for (uint8_t i = 0; i < len; i++) {
            if (ignored[i] == *output_ch) {
                is_ignored = true;
                break;
            }
        }

        if (!is_ignored) return true;
    }
}

uint8_t ha_consume2Sequence(const char *sequence1, const char *sequence2) {
    uint16_t length1 = strlen(sequence1);
    uint16_t length2 = strlen(sequence2);
    uint16_t progress1 = 0;
    uint16_t progress2 = 0;

    char ch;

    while (true) {
        if (!ha_waitForRx()) return 0;
        ch = ha_getCh();
        if (ch == sequence1[progress1]) progress1++;
        else if (ch == sequence1[0]) progress1 = 1;
        else progress1 = 0;

        if (ch == sequence2[progress2]) progress2++;
        else if (ch == sequence2[0]) progress2 = 1;
        else progress2 = 0;

        if (progress1 == length1) return 1;
        if (progress2 == length2) return 2;
    }
}

uint8_t ha_consume3Sequence(const char *sequence1, const char *sequence2, const char *sequence3) {
    uint16_t length1 = strlen(sequence1);
    uint16_t length2 = strlen(sequence2);
    uint16_t length3 = strlen(sequence3);
    uint16_t progress1 = 0;
    uint16_t progress2 = 0;
    uint16_t progress3 = 0;

    char ch;

    while (true) {
        if (!ha_waitForRx()) return 0;
        ch = ha_getCh();

        if (ch == sequence1[progress1]) progress1++;
        else if (ch == sequence1[0]) progress1 = 1;
        else progress1 = 0;

        if (ch == sequence2[progress2]) progress2++;
        else if (ch == sequence2[0]) progress2 = 1;
        else progress2 = 0;

        if (ch == sequence3[progress3]) progress3++;
        else if (ch == sequence3[0]) progress3 = 1;
        else progress3 = 0;

        if (progress1 == length1) return 1;
        if (progress2 == length2) return 2;
        if (progress3 == length3) return 3;
    }
}


/*** AT COMMANDS ***/

ha_at_response ha_connectToApOnce(char *ssid, char *pwd) {
    if (strlen(ssid) >= HA_MAX_SSID_LEN || strlen(pwd) >= HA_MAX_PWD_LEN) return HA_AT_IRRECOVERABLE;
    ha_clearBuffer();
    ha_at_status status = ha_getConnectionStatus();
    if (status == HA_STATUS_GOT_IP || status == HA_STATUS_DISCONNECTED) return HA_AT_SUCCESS;

    // Send connect command
    char cmd[HA_MAX_SSID_LEN + HA_MAX_PWD_LEN + 25];
    sprintf(cmd, "AT+CWJAP_CUR=\"%s\",\"%s\"\r\n", ssid, pwd);
    if (!ha_sendUartRaw(cmd)) {
        ha_clearBuffer();
        return HA_AT_FAIL;
    }

    // Wait for IP assignment
    for (uint8_t i = 0; i < 50; i++) {
        if (ha_consumeSequence("WIFI GOT IP\r\n")) {
            ha_delayMs(3000);
            break;
        }
        ha_delayMs(100);
    }

    // Get connection status
    if (ha_getConnectionStatus() != HA_STATUS_GOT_IP) {
        ha_clearBuffer();
        return HA_AT_FAIL;
    }
    ha_clearBuffer();

    // Save to EEPROM if it is different
    char saved_ssid[HA_MAX_SSID_LEN];
    char saved_pwd[HA_MAX_PWD_LEN];
    bool has_saved = ha_getSavedAPSettings(saved_ssid, saved_pwd);
    if (!has_saved || strcmp(saved_ssid, ssid) != 0 || strcmp(saved_pwd, pwd) != 0) {
        ha_saveAPSettingsToMemory(ssid, pwd);
    }
    return HA_AT_SUCCESS;
}

bool ha_connectToAp(void) {
    char ssid[HA_MAX_SSID_LEN];
    char pwd[HA_MAX_PWD_LEN];
    ha_at_response response;

    if (!ha_isAPSettingsSaved()) {
        // TODO: Somehow enter AP search mode
        ha_saveAPSettingsToMemory(HA_SSID, HA_PWD);
    }

    ha_getSavedAPSettings(ssid, pwd);

    while (true) {
        response = ha_connectToApOnce(HA_SSID, HA_PWD);
        if (response == HA_AT_SUCCESS) return true;
        if (response == HA_AT_IRRECOVERABLE) return false;
        ha_delayMs(5000);
    }
}

bool ha_pinHost(const char *host) {
    char cmd[HA_MAX_HOST_LEN + 16];
    sprintf(cmd, "AT+PING=\"%s\"", host);
    return ha_sendUartCommand(cmd);
}

ha_at_response ha_openTCP(const char *host, uint16_t port) {
    if (strlen(host) >= HA_MAX_HOST_LEN) return HA_AT_IRRECOVERABLE;
    // Check if the host can be reached
    // if (!ha_pinHost(host)) return HA_AT_FAIL;
    // ha_delayMs(100);
    // Open TCP connection
    char cmd[HA_MAX_HOST_LEN + 35];
    ha_clearBuffer();
    sprintf(cmd, "AT+CIPSTART=\"TCP\",\"%s\",%u,60\r\n", host, port);
    if (!ha_sendUartRaw(cmd)) {
        return HA_AT_FAIL;
    }
    ha_delayMs(1000);
    uint8_t res = ha_consume2Sequence("OK", "ALREADY CONNECT");
    ha_clearBuffer();

    if (res == 2) return HA_AT_NO_CHANGE;
    if (res == 1) return HA_AT_SUCCESS;
    return HA_AT_FAIL;
}

ha_at_response ha_closeSocket(void) {
    if (ha_sendUartCommand("AT+CIPCLOSE=0")) return HA_AT_SUCCESS;
    return HA_AT_FAIL;
}

ha_at_response ha_writeSocketStart(uint16_t len) {
    char cmd[20];
    sprintf(cmd, "AT+CIPSEND=%u\r\n", len);
    ha_sendUartRaw(cmd);

    for (uint8_t i = 0; i < 50; i++) {
        if (ha_consumeSequence(">")) break;
        if (i == 49) return HA_AT_FAIL;
        ha_delayMs(100);
    }

    return HA_AT_SUCCESS;
}

ha_at_response ha_writeSocketEnd(void) {
    ha_delayMs(500);
    for (uint8_t i = 0; i < 50; i++) {
        if (ha_consume2Sequence("ERROR", "SEND OK") == 2) return HA_AT_SUCCESS;
        ha_delayMs(100);
    }
    return HA_AT_FAIL;
}

ha_at_response ha_writeSocket(uint8_t *data, uint16_t len) {
    if (ha_writeSocketStart(len) == HA_AT_FAIL) return HA_AT_FAIL;
    ha_sendUartRawLen((char *) data, len);
    return ha_writeSocketEnd();
}

uint16_t ha_readSocket(uint8_t *data, uint16_t max_len) {
    if (!ha_consumeSequence("+IPD,")) return MAX_UINT16;
    if (!ha_isRxReady()) return MAX_UINT16;
    char ch = ha_getCh();

    // Get first number
    uint16_t num_1 = 0;
    while (true) {
        if (ch < '0' || ch > '9') return MAX_UINT16;
        num_1 = num_1 * 10 + ch - '0';
        if (!ha_isRxReady()) return MAX_UINT16;
        ch = ha_getCh();
        if (ch == ',' || ch == ':') break;
    }

    // Get second number
    uint16_t num_2 = 0;
    bool has_second_num = false;
    if (ch != ':') {
        has_second_num = true;
        if (!ha_isRxReady()) return MAX_UINT16;
        ch = ha_getCh();
        while (true) {
            if (ch == '.' || ch == '"') {
                has_second_num = false;
                break;
            }
            if (ch < '0' || ch > '9') return MAX_UINT16;
            num_2 = num_2 * 10 + ch - '0';
            if (!ha_isRxReady()) return MAX_UINT16;
            ch = ha_getCh();
            if (ch == ',' || ch == ':') break;
        }
    }

    // Consume the rest until payload
    if (ch != ':') {
        while (true) {
            if (!ha_isRxReady()) return MAX_UINT16;
            ch = ha_getCh();
            if (ch == ':') break;
            if (ch != ',' && ch != '"' && ch != '.' && (ch < '0' || ch > '9')) return MAX_UINT16;
        }
    }

    // Copy payload
    uint16_t len = has_second_num ? num_2 : num_1;
    if (len > max_len) return MAX_UINT16;
    for (uint16_t i = 0; i < len; i++) {
        if (!ha_isRxReady()) return MAX_UINT16;
        data[i] = ha_getCh();
    }

    return len;
}

ha_at_status ha_getConnectionStatus(void) {
    if (!ha_sendUartRaw("AT+CIPSTATUS\r\n") || !ha_consumeSequence("STATUS:") || !ha_waitForRx()) {
        return HA_STATUS_UNKNOWN;
    }
    char ch = ha_getCh();
    ha_clearBuffer(); // Has some trailing stuff
    if (ch == '2') return HA_STATUS_GOT_IP;
    if (ch == '3') return HA_STATUS_SOCKET_CONNECTED;
    if (ch == '4') return HA_STATUS_DISCONNECTED;
    if (ch == '5') return HA_STATUS_WIFI_FAIL;

    return HA_STATUS_UNKNOWN;
}

bool ha_isAPSettingsSaved(void) {
    return EEPROM_Read(HA_AP_ADDR) == HA_AP_MAGIC_INIT_0 && EEPROM_Read(HA_AP_ADDR + 1) == HA_AP_MAGIC_INIT_1;
}

bool ha_getSavedAPSettings(char *ssid, char *pwd) {
    if (!ha_isAPSettingsSaved()) return false;
    uint8_t addr = HA_AP_ADDR + 2;
    uint8_t len_ssid = EEPROM_Read(addr++);
    for (uint8_t i = 0; i < len_ssid; i++) {
        ssid[i] = (char) EEPROM_Read(addr++);
    }
    uint8_t len_pwd = EEPROM_Read(addr++);
    for (uint8_t i = 0; i < len_pwd; i++) {
        pwd[i] = (char) EEPROM_Read(addr++);
    }
    return true;
}

void ha_saveAPSettingsToMemory(char *ssid, char *pwd) {
    uint8_t len_ssid = strlen(ssid);
    uint8_t len_pwd = strlen(pwd);
    uint8_t addr = HA_AP_ADDR;

    EEPROM_Write(addr++, HA_AP_MAGIC_INIT_0);
    EEPROM_Write(addr++, HA_AP_MAGIC_INIT_1);
    EEPROM_Write(addr++, len_ssid);
    for (uint8_t i = 0; i < len_ssid; i++) {
        EEPROM_Write(addr++, ssid[i]);
    }
    EEPROM_Write(addr++, len_pwd);
    for (uint8_t i = 0; i < len_pwd; i++) {
        EEPROM_Write(addr++, pwd[i]);
    }
}

/*********** UUID ***********/

/**
 * Checks if the UUID has been initialized in EEPROM.
 *
 * The function verifies the presence of a previously stored UUID by checking for
 * a magic number at a specific EEPROM location.
 *
 * @return true if the UUID is initialized; false otherwise.
 */
bool ha_uuidIsInitialized(void) {
    return EEPROM_Read(HA_UUID_ADDR) == HA_UUID_MAGIC_INIT_0
           && EEPROM_Read(HA_UUID_ADDR + 1) == HA_UUID_MAGIC_INIT_1;
}


/**
 * Generates an 8-byte unique identifier (UUID) using multiple sources of entropy.
 *
 * The function combines timer-based jitter, ADC noise, and ESP8266 startup
 * UART noise to create a robust and unique identifier. It leverages asynchronous
 * system behavior and analog noise to ensure variability.
 *
 * NOTE: Call this AFTER the ESP setup.
 *
 * @param counterGet A function pointer to retrieve the current 16-bit timer value.
 *                   Used for capturing system timing jitter during various operations.
 * @param adcGet A function pointer to retrieve an 8-bit ADC value. Used to add analog
 *               noise as a source of entropy.
 */
void ha_uuidGenerate(uint16_t (*counterGet)(void), uint8_t (*adcGet)(void)) {
    uint8_t uuid[HA_UUID_SIZE];
    // First timer read has more entropy since it encompasses the whole startup
    // so use the full 16 bits.
    uuid[0] = counterGet() & 0xFF;
    uuid[1] = counterGet() >> 8;
    // Subsequent 2 commands  are more predictable, so fold the timer's 16 bits into 8.
    ha_delayUs(10);
    ha_sendUartCommand("AT");
    uuid[2] = counterGet() & 0xFF;
    uuid[2] ^= counterGet() >> 8;
    ha_delayUs(10);
    ha_sendUartCommand("AT+GMR");
    uuid[3] = counterGet() & 0xFF;
    uuid[3] ^= counterGet() >> 8;
    // ADC portion of the UUID plus timer jitter
    for (uint8_t n = 4; n < 6; n++) {
        for (uint8_t i = 0; i < 8; i++) {
            ha_delayUs(10);
            uuid[n] ^= adcGet() ^ ((counterGet() & 0xFF) ^ (counterGet() >> 8));
        }
        ha_delayUs(100);
    }
    // Use ESP startup
    uuid[6] = at_entropy[0] ^ adcGet() ^ ((counterGet() & 0xFF) ^ (counterGet() >> 8));
    uuid[7] = at_entropy[1] ^ adcGet() ^ ((counterGet() & 0xFF) ^ (counterGet() >> 8));
    // Write to non-volatile memory
    EEPROM_Write(HA_UUID_ADDR, HA_UUID_MAGIC_INIT_0);
    EEPROM_Write(HA_UUID_ADDR + 1, HA_UUID_MAGIC_INIT_1);
    EEPROM_Write(HA_UUID_ADDR + 2, uuid[0]);
    EEPROM_Write(HA_UUID_ADDR + 3, uuid[1]);
    EEPROM_Write(HA_UUID_ADDR + 4, uuid[2]);
    EEPROM_Write(HA_UUID_ADDR + 5, uuid[3]);
    EEPROM_Write(HA_UUID_ADDR + 6, uuid[4]);
    EEPROM_Write(HA_UUID_ADDR + 7, uuid[5]);
    EEPROM_Write(HA_UUID_ADDR + 8, uuid[6]);
    EEPROM_Write(HA_UUID_ADDR + 9, uuid[7]);
}

/**
 * Reads the raw 8-byte UUID from EEPROM.
 *
 * If the UUID has been initialized, this function retrieves the 8-byte unique
 * identifier directly from EEPROM and checks that it is not all zeros.
 *
 * @param uuid A pointer to an 8-byte buffer where the UUID will be stored.
 * @return true if a valid UUID was retrieved; false if the UUID is uninitialized
 *         or contains only zeros.
 */
bool ha_uuidGetRaw(uint8_t uuid[HA_UUID_SIZE]) {
    if (!ha_uuidIsInitialized()) return false;
    bool is_all_zeroes = true;
    for (uint8_t i = 0; i < 8; i++) {
        uuid[i] = EEPROM_Read(HA_UUID_ADDR + 2 + i);
        is_all_zeroes = is_all_zeroes && uuid[i] == 0;
    }
    return !is_all_zeroes;
}

/**
 * Retrieves the UUID as a null-terminated hexadecimal string.
 *
 * This function reads the raw 8-byte UUID from EEPROM and converts it to a
 * 16-character hexadecimal string. The resulting string is null-terminated.
 *
 * @param uuid A pointer to a 17-byte buffer where the null-terminated
 *             hexadecimal string will be stored.
 * @return true if a valid UUID was retrieved and converted; false if the UUID
 *         is uninitialized or invalid.
 */
bool ha_uuidGetHexStr(char *uuid) {
    uint8_t uuid_raw[HA_UUID_SIZE];
    if (!ha_uuidGetRaw(uuid_raw)) return false;
    for (uint8_t i = 0; i < HA_UUID_SIZE; i++) {
        uint8_t high = uuid_raw[i] >> 4;
        uint8_t low = uuid_raw[i] & 0xF;
        uuid[i * 2] = high <= 9 ? high + '0' : high - 10 + 'A';
        uuid[i * 2 + 1] = low <= 9 ? low + '0' : low - 10 + 'A';
    }
    uuid[16] = 0;
    return true;
}


/*** MQTT ***/

uint8_t ha_mqttVariableByteInteger(uint8_t *stream, uint32_t value) {
    uint8_t encoded_byte;
    uint8_t i = 0;
    do {
        encoded_byte = value % 128;
        value >>= 7;
        if (value > 0) encoded_byte = encoded_byte | 128;
        stream[i] = encoded_byte;
        i++;
    } while (value > 0);
    return i;
}

uint16_t ha_mqttString(uint8_t *stream, const char *value) {
    uint16_t len = strlen(value);
    stream[0] = len >> 8;
    stream[1] = len & 0xFF;
    memcpy(stream + 2, value, len);
    return len + 2;
}

bool ha_mqttConnect(const char *host, uint16_t port) {
    // Open socket
    ha_at_response resp = ha_openTCP(host, port);
    // Close socket if it was already open
    if (resp == HA_AT_NO_CHANGE) {
        ha_closeSocket();
        resp = ha_openTCP(host, port);
    }
    // Check if managed to open the socket
    if (resp != HA_AT_SUCCESS) {
        is_mqtt_connected = false;
        return false;
    }
    // Send connect payload
    resp = ha_writeSocket(mqtt_connect_pkg, 14);
    if (resp != HA_AT_SUCCESS) {
        is_mqtt_connected = false;
        return false;
    }
    // Wait for CONNACK
    uint8_t len = 0;
    uint8_t data[8];
    for (uint8_t i = 0; i < 50; i++) {
        ha_delayMs(100);
        len = ha_readSocket(data, 4);
        if (len == 4) break;
    }
    // Validate CONNACK
    if (len == 4 && memcmp(mqtt_connack_pkg, data, 4) == 0) {
        is_mqtt_connected = true;
        return true;
    }
    is_mqtt_connected = false;
    return false;
}

bool ha_mqttPing(void) {
    // Send ping
    ha_at_response resp = ha_writeSocket(mqtt_pingreq_pkg, 2);
    if (resp != HA_AT_SUCCESS) return false;
    // Wait for ping response
    uint8_t len = 0;
    uint8_t data[8];
    for (uint8_t i = 0; i < 50; i++) {
        len = ha_readSocket(data, 2);
        if (len == 2) break;
        ha_delayMs(100);
    }
    // Validate PINGRESP
    if (len == 2 && memcmp(mqtt_pingresp_pkg, data, 2) == 0) return true;
    return false;
}

bool ha_mqttPingAndReconnect(const char *host, uint16_t port) {
    while (!ha_mqttPing()) {
        is_mqtt_connected = false;
        ha_delayMs(1000);
        ha_mqttConnect(host, port);
        ha_delayMs(1000);
    }
    is_mqtt_connected = true;
    return true;
}

bool ha_mqttDisconnect(void) {
    ha_at_response resp = ha_writeSocket(mqtt_disconnect_pkg, 2);
    if (resp != HA_AT_SUCCESS) return false;
    return ha_closeSocket();
}

bool ha_mqttPublishStart(const char *topic, uint16_t payload_len, bool retain) {
    uint8_t pkg[256];
    uint8_t encoded_topic[64];

    // Fixed header
    pkg[0] = 0x30 | (retain ? 0x01 : 0x00);  // Message type PUBLISH, no QoS, retain flag

    uint8_t i = 1;  // Packet index

    // Encode topic
    uint16_t encoded_topic_len = ha_mqttString(encoded_topic, topic);

    // Calculate remaining length
    uint16_t remaining_length = encoded_topic_len + payload_len;
    i += ha_mqttVariableByteInteger(&pkg[i], remaining_length);

    // Copy topic and payload into the packet
    memcpy(&pkg[i], encoded_topic, encoded_topic_len);
    i += encoded_topic_len;

    ha_at_response resp = ha_writeSocketStart(i + payload_len);
    ha_sendUartRawLen((char *) pkg, i);
    if (resp != HA_AT_SUCCESS) return false;
    return true;
}

bool ha_mqttPublish(const char *topic, const char *payload, bool retain) {
    uint16_t payload_len = strlen(payload);
    if (!ha_mqttPublishStart(topic, payload_len, retain)) return false;
    ha_sendUartRawLen(payload, payload_len);
    ha_at_response resp = ha_writeSocketEnd();
    if (resp != HA_AT_SUCCESS) return false;
    return true;
}

bool ha_mqttEnforceConnection(void) {
    ha_at_status status = ha_getConnectionStatus();
    // If not connected to an AP, try to connect
    if (status == HA_STATUS_UNKNOWN || status == HA_STATUS_WIFI_FAIL) {
        ha_connectToAp();
        ha_delayMs(2000);
        status = ha_getConnectionStatus();
    }
    // If TCP socket not open, try to open it
    if (status == HA_STATUS_GOT_IP || status == HA_STATUS_DISCONNECTED) {
        if (!ha_mqttConnect(HA_BROKER_HOST, HA_BROKER_PORT)) return false;
        ha_delayMs(500);
        status = ha_getConnectionStatus();
    }
    // If not connected by now, give up
    if (status != HA_STATUS_SOCKET_CONNECTED) {
        is_mqtt_connected = false;
        return false;
    }
    ha_mqttPingAndReconnect(HA_BROKER_HOST, HA_BROKER_PORT);
    return true;
}


bool ha_mqttPublishLoop(const char *topic, const char *payload) {
    ha_mqttEnforceConnection();
    if (strlen(topic) == 0 || strlen(payload) == 0) return false;

    // Otherwise, publish and ping + reconnect if the publish fails, and try once more
    if (!ha_mqttPublish(topic, payload, false)) {
        ha_mqttPingAndReconnect(HA_BROKER_HOST, HA_BROKER_PORT);
        if (!ha_mqttPublish(topic, payload, false)) {
            is_mqtt_connected = false;
            return false;
        }
    }
    return true;
}


void ha_discoveryTopicName(char *topic, const char *platform) {
    // <discovery_prefix>/<component>/[<node_id>/]<object_id>/config
    char device_id[18];
    ha_uuidGetHexStr(device_id);
    sprintf(topic, "homeassistant/%s/%s/config", platform, device_id);
}

bool ha_publishDiscoveryMessage1Component(const char *name, const char *platform, HAComponent *component) {
    if (!ha_mqttEnforceConnection()) return false;

    char uuid[17];
    char topic[64];
    char buffer[72];

    ha_uuidGetHexStr(uuid);
    uint16_t payload_len = 321; // base
    payload_len += 3 * strlen(uuid);
    payload_len += strlen(name);
    payload_len += 2 * strlen(HA_SW_VERSION);
    payload_len += 2 * strlen(component->name);
    payload_len += strlen(component->platform);
    payload_len += strlen(component->dev_class);
    payload_len += strlen(component->unit);
    payload_len += strlen(component->value_key);

    ha_discoveryTopicName(topic, platform);

    if (!ha_mqttPublishStart(topic, payload_len, false)) return false;

    SEND_PARTIAL("{\"dev\": {\"ids\": [\"%s\"], \"name\": \"", uuid)
    SEND_PARTIAL("%s\", \"mf\": \"Rui Calsaverini\", \"sw\": \"", name)
    SEND_PARTIAL("%s\"}, \"o\": {\"name\": \"Rui HA\", \"sw\": \"", HA_SW_VERSION)
    SEND_PARTIAL("%s\", \"url\": \"https://rui.calsaverini.com", HA_SW_VERSION)
    SEND_PARTIAL("/ha\"}, \"cmps\": {\"%s\": {\"p\": \"", component->name)
    SEND_PARTIAL("%s\", \"device_class\": \"%s\", \"unit_of_measur", component->platform, component->dev_class)
    SEND_PARTIAL("ement\": \"%s\", \"value_template\": \"{{value", component->unit)
    SEND_PARTIAL("_json.%s}}\", \"unique_id\": \"", component->value_key)
    SEND_PARTIAL("%s_%s\"}}, \"state_topic\": \"homeassistant/", component->name, uuid)
    SEND_PARTIAL("%s/state\", \"qos\": 0}", uuid)

    return ha_writeSocketEnd();
}

