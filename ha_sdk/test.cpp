#include <gtest/gtest.h>
#include "ha.h"
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <regex>

using std::string;
using std::map;
using std::vector;

std::regex CIPSTART_PATTERN(R"(^AT\+CIPSTART=\"(TCP|UDP|SSL)\",\"([^\"]+)\",(\d+),?(\d+)?$)");

std::string uint8ToHexString(uint8_t value) {
    std::stringstream ss;
    ss << std::uppercase << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(value);
    return ss.str();
}

std::string charToPrintable(uint8_t value) {
    std::string result;
    if (std::isalnum(value) || std::ispunct(value)) {
        // Alphanumeric or printable symbol
        result = "(" + std::string(1, static_cast<char>(value)) + ")";
    } else if (value == '\n') {
        result = "(\\n)";
    } else if (value == '\t') {
        result = "(\\t)";
    } else if (value == '\r') {
        result = "(\\r)";
    } else {
        // Non-printable or unknown character
        result = "(?)";
    }

    // Pad the result to 5 characters with spaces
    result.resize(5, ' ');
    return result;
}

std::string printableSequenceSimple(std::vector<uint8_t> m) {
    string output;
    for (const auto &ch: m) {
        std::string result;
        if (std::isalnum(ch) || std::ispunct(ch)) {
            // Alphanumeric or printable symbol
            output += std::string(1, static_cast<char>(ch));
        } else if (ch == '\n') {
            output += "\\n";
        } else if (ch == '\t') {
            output += "\\t";
        } else if (ch == '\r') {
            output += "\\r";
        } else {
            output += "?";
        }
    }
    return output;
}

std::string printableSequence(std::vector<uint8_t> m) {
    string output;
    for (const auto &ch: m) {
        output += uint8ToHexString(ch) + charToPrintable(ch);
    }
    return output;
}


testing::AssertionResult AssertUint8SeQ(const char *m_expr,
                                        const char *n_expr,
                                        std::vector<uint8_t> m,
                                        std::vector<uint8_t> n) {

    if (m.size() != n.size()) {
        return testing::AssertionFailure() << m_expr << " and " << n_expr << " have different lengths, "
                                           << m.size() << " and " << n.size() << " respectively.\n"
                                           << m_expr << ", " << n_expr << ":\n- " << printableSequence(m) << "\n- "
                                           << printableSequence(n);
    }
    if (memcmp(m.data(), n.data(), m.size()) == 0) return testing::AssertionSuccess();

    return testing::AssertionFailure() << m_expr << " and " << n_expr << " are different.\n"
                                       << m_expr << ", " << n_expr << ":\n- " << printableSequence(m) << "\n- "
                                       << printableSequence(n);

}


class ESPMock {
public:
    ESPMock() = default;

    void putCh(char c) {
        if (c == '\n') {
            sendCmd();
            temp_cmd_.clear();
        } else if (c != '\r') {
            temp_cmd_.push_back(c);
        }
    }

    char getCh() {
        if (rx_.empty()) return 0;
        char c = rx_[0];
        rx_.erase(rx_.begin());
        return c;
    }

    bool rxReady() {
        return !rx_.empty();
    }

private:
    void sendCmd() {
        std::smatch matches;
        std::string input;

        input = std::string(temp_cmd_.begin(), temp_cmd_.end());
        std::cout << printableSequenceSimple(temp_cmd_) << std::endl;

        // AT+CIPSTART
        if (std::regex_search(input, matches, CIPSTART_PATTERN)) {
            std::string protocol = matches[1].str();
            std::string host = matches[2].str();
            std::string port = matches[3].str();
            std::cout << protocol << " " << host << " " << port << "\n";
        } else {
            std::cerr << "No match found!" << std::endl;
        }
    }

private:
    std::vector<uint8_t> rx_;
    std::vector<uint8_t> tx_;
    std::vector<uint8_t> temp_cmd_;
};

static std::vector<uint8_t> r_stream;
static std::vector<uint8_t> w_stream;
static uint16_t ri = 0;
ESPMock esp;

static char getCh() { return (char) r_stream[ri++]; }

static char getChEsp() { return (char) esp.getCh(); }

static bool rxReady() { return ri < r_stream.size(); }

static bool rxReadyEsp() { return esp.rxReady(); }

static void putCh(char c) { w_stream.push_back(c); }

static void putChEsp(char c) { esp.putCh(c); }

static bool txReady() { return true; }

static void setReset(bool r) { /* Irrelevant */ }

static void delayUs(uint16_t d) { /* Irrelevant */ }

TEST(HAATCommands, ReadSocketBasic) {
    ha_initialize(getCh, rxReady, putCh, txReady, setReset, delayUs);
    std::string str;
    uint8_t data[1024];
    uint16_t len;

    // No mux, no IP
    ri = 0;
    str = "+IPD,4:test\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 4);
    ASSERT_TRUE(memcmp(data, "test", 4) == 0);

    // Mux, no IP
    ri = 0;
    str = "+IPD,0,4:test\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 4);
    ASSERT_TRUE(memcmp(data, "test", 4) == 0);

    // Mux, no IP, empty payload
    ri = 0;
    str = "+IPD,0,0:\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 0);

    // No Mux, with IP
    ri = 0;
    str = "+IPD,4,\"192.168.1.1\",5000:test\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 4);
    ASSERT_TRUE(memcmp(data, "test", 4) == 0);

    // No Mux, with IP without comas
    ri = 0;
    str = "+IPD,4,192.168.1.1,5000:test\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 4);
    ASSERT_TRUE(memcmp(data, "test", 4) == 0);

    // With Mux, with IP
    ri = 0;
    str = "+IPD,0,4,\"192.168.1.1\",5000:test\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 4);
    ASSERT_TRUE(memcmp(data, "test", 4) == 0);

    // With Mux, with IP, longer payload
    ri = 0;
    str = "+IPD,0,23,\"192.168.1.1\",5000:Testing a longer string\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 23);
    ASSERT_TRUE(memcmp(data, "Testing a longer string", 23) == 0);

    // Without mux, with IP, longer payload
    ri = 0;
    str = "+IPD,23,\"192.168.1.1\",5000:Testing a longer string\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 23);
    ASSERT_TRUE(memcmp(data, "Testing a longer string", 23) == 0);

    // Without mux, without IP, longer payload
    ri = 0;
    str = "+IPD,23:Testing a longer string\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 23);
    ASSERT_TRUE(memcmp(data, "Testing a longer string", 23) == 0);

    // With Mux, with IP, longer payload
    ri = 0;
    str = "+IPD,0,9,\"192.168.1.1\",5000:123456789\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 9);
    ASSERT_TRUE(memcmp(data, "123456789", 9) == 0);

    // With Mux, with IP, longer payload
    ri = 0;
    str = "+IPD,0,10,\"192.168.1.1\",5000:1234567890\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 10);
    ASSERT_TRUE(memcmp(data, "1234567890", 10) == 0);

    // With Mux, with IP, longer payload
    ri = 0;
    str = "+IPD,0,11,\"192.168.1.1\",5000:12345678901\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 11);
    ASSERT_TRUE(memcmp(data, "12345678901", 11) == 0);

    // Longer string
    ri = 0;
    str = "+IPD,110:......................................................................"
          "........................................";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(len, 110);
    ASSERT_TRUE(memcmp(data, ".........................................................."
                             "....................................................", 110) == 0);

    // No mux, no IP, above len
    ri = 0;
    str = "+IPD,4:test\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 3);
    ASSERT_EQ(len, MAX_UINT16);
}

TEST(HAATCommands, ReadSocketMalformed) {
    ha_initialize(getCh, rxReady, putCh, txReady, setReset, delayUs);
    std::string str;
    uint8_t data[1024];
    uint16_t len;

    // Read timeout
    ri = 0;
    str = "+IPD,5:test";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    len = ha_readSocket(data, 1024);
    ASSERT_EQ(ha_readSocket(data, 1024), MAX_UINT16);

    // Read Malformed package, no data
    ri = 0;
    str = "+IPD,5";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    ASSERT_EQ(ha_readSocket(data, 1024), MAX_UINT16);

    // Read Malformed package, no len
    ri = 0;
    str = "+IPD,";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    ASSERT_EQ(ha_readSocket(data, 1024), MAX_UINT16);
    ri = 0;
    str = "+IPD:123";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    ASSERT_EQ(ha_readSocket(data, 1024), MAX_UINT16);
    ri = 0;
    str = "+IPD,:123";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    ASSERT_EQ(ha_readSocket(data, 1024), MAX_UINT16);
}

TEST(HAATCommands, WriteSocketBasic) {
    ha_initialize(getCh, rxReady, putCh, txReady, setReset, delayUs);
    std::string str;
    std::vector<uint8_t> expected;

    ri = 0;
    str = "\r\nOK\r\n> \r\nRecv 4 bytes\r\n\r\nSEND OK\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    w_stream.clear();
    expected = {'A', 'T', '+', 'C', 'I', 'P', 'S', 'E', 'N', 'D', '=', '4', '\r', '\n', 't', 'e', 's', 't'};
    ASSERT_EQ(ha_writeSocket((uint8_t *) "test", 4), HA_AT_SUCCESS);
    ASSERT_PRED_FORMAT2(AssertUint8SeQ, w_stream, expected);

    ri = 0;
    str = "\r\nlink is not valid\r\n>ERROR\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    w_stream.clear();
    ASSERT_EQ(ha_writeSocket((uint8_t *) "test", 4), HA_AT_FAIL);
}

TEST(HAMQTTCommands, Connect) {
    std::string str;
    std::vector<uint8_t> expected;
    ha_initialize(getChEsp, rxReadyEsp, putChEsp, txReady, setReset, delayUs);

    ri = 0;
    str = "\r\nOK\r\n> \r\nRecv 4 bytes\r\n\r\nSEND OK\r\n";
    r_stream = std::vector<uint8_t>(str.begin(), str.end());
    w_stream.clear();
    expected = {'A', 'T', '+', 'C', 'I', 'P', 'S', 'E', 'N', 'D', '=', '4', '\r', '\n', 't', 'e', 's', 't'};
    ha_mqttConnect("broker.mqtt.local", 8883);

    ASSERT_TRUE(false);
    ASSERT_PRED_FORMAT2(AssertUint8SeQ, w_stream, expected);
}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
