#include "ha.h"

#define INTERVAL_PUBLISH_S   5
#define INTERVAL_PING_S 999
#define INTERVAL_RECONNECT_S 2
#define LED_ERROR RC6
#define LED_ACTION RA5

#ifdef __XC8
#include "mcc_generated_files/system/system.h"
#else

#include <stdbool.h>

#define CLRWDT()

#define ADC_Enable()

#define ADC_ChannelSelect(CH)

#define ADC_ConversionStart()

#define ADC_IsConversionDone() true

#define ADC_ConversionResultGet() 0

void SYSTEM_Initialize(void) { /* Just to mock */ }

void INTERRUPT_GlobalInterruptEnable(void) { /* Just to mock */ }

void INTERRUPT_PeripheralInterruptEnable(void) { /* Just to mock */ }

void TMR0_PeriodMatchCallbackRegister(void (*CallbackHandler)(void)) {}

bool TMR0_Start(void) { /* Just to mock */ }

bool TMR0_Stop(void) { /* Just to mock */ }

bool TMR1_Start(void) { /* Just to mock */ }

uint16_t TMR1_CounterGet(void) { /* Just to mock */ }

bool EUSART1_IsRxReady(void) { /* Just to mock */ }

bool EUSART1_IsTxReady(void) { /* Just to mock */ }

uint8_t EUSART1_Read(void) { /* Just to mock */ }

void EUSART1_Write(char c) { /* Just to mock */ }

void __delay_ms(uint16_t v) { /* Just to mock */ }

void __delay_us(uint16_t v) { /* Just to mock */ }

bool RC6;
bool RA5;
bool RA3;

#endif

uint16_t overflow_count = 0;
bool should_publish = false;
bool should_reconnect = true;
bool should_ping = false;

/* Top level */

typedef enum {
    LED_INDICATOR_ALL_GOOD,
    LED_INDICATOR_BLINK_ACTION,
    LED_INDICATOR_NOT_CONNECTED,
    LED_INDICATOR_WIFI_SETUP_FAILED,
    LED_INDICATOR_UUID_FAILED,
} led_err_indicator_status;

void timerOverflow(void);

void ledIndicators(led_err_indicator_status status);

/* Other */

uint8_t ADC_GetNoiseValue(void);

/* Basic UART stuff */

void delayUs(uint16_t us);

void reset(bool state);

char readUart(void);

void writeUart(char c);

/* MQTT Message */

bool sendData(void);

int main(void) {
    SYSTEM_Initialize();
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    TMR0_PeriodMatchCallbackRegister(timerOverflow);
    TMR0_Stop();
    TMR1_Start();

    /* Initialize the HA SDK */
    ledIndicators(LED_INDICATOR_BLINK_ACTION);
    ha_initialize(readUart, EUSART1_IsRxReady, writeUart, EUSART1_IsTxReady, reset, delayUs);

    /* Setup WiFi */
    ledIndicators(LED_INDICATOR_BLINK_ACTION);
    if (!ha_setupWifi()) ledIndicators(LED_INDICATOR_WIFI_SETUP_FAILED);

    /* Generate own UUID */
    ledIndicators(LED_INDICATOR_BLINK_ACTION);
    if (!ha_uuidIsInitialized()) {
        ha_uuidGenerate(TMR1_CounterGet, ADC_GetNoiseValue);
    }
    if (!ha_uuidIsInitialized()) ledIndicators(LED_INDICATOR_UUID_FAILED);

    /* Send discovery message */
    HAComponent component;
    sprintf(component.name, "temperature");
    sprintf(component.value_key, "temp");
    sprintf(component.unit, "\xC2\xB0\x43");
    sprintf(component.dev_class, HA_DC_TEMPERATURE_STR);
    sprintf(component.platform, HA_P_SENSOR_STR);
    ha_publishDiscoveryMessage1Component("test", HA_P_SENSOR_STR, &component);

    TMR0_Start();


    while (1) {
        CLRWDT();

        if (is_mqtt_connected && should_publish) {
            ledIndicators(LED_INDICATOR_BLINK_ACTION);
            should_publish = should_reconnect = should_ping = false;
            sendData();
            TMR0_Start();
        } else if (is_mqtt_connected && should_ping) {
            ledIndicators(LED_INDICATOR_BLINK_ACTION);
            should_publish = should_reconnect = should_ping = false;
            ha_mqttPingAndReconnect(HA_BROKER_HOST, HA_BROKER_PORT);
        } else if (!is_mqtt_connected && should_reconnect) {
            ledIndicators(LED_INDICATOR_BLINK_ACTION);
            should_publish = should_reconnect = should_ping = false;
            ha_connectToAp();
            ha_mqttConnect(HA_BROKER_HOST, HA_BROKER_PORT);
            TMR0_Start();
        }
    }
}

/* Top level */

void timerOverflow(void) {
    if (is_mqtt_connected && overflow_count == INTERVAL_PUBLISH_S) {
        should_publish = true;
        should_reconnect = should_ping = false;
        TMR0_Stop();
        overflow_count = 0;
        return;
    } else if (is_mqtt_connected && overflow_count % INTERVAL_PING_S == 0) {
        should_ping = true;
        should_reconnect = should_publish = false;
        overflow_count++;
        return;
    }
    if (!is_mqtt_connected && overflow_count == INTERVAL_RECONNECT_S) {
        should_publish = false;
        should_reconnect = should_ping = true;
        TMR0_Stop();
        overflow_count = 0;
        return;
    }

    should_publish = should_reconnect = should_ping = false;
    overflow_count++;
}

void ledIndicators(led_err_indicator_status status) {
    if (status == LED_INDICATOR_ALL_GOOD) {
        LED_ERROR = 0;
        LED_ACTION = 0;
        return;
    }
    if (status == LED_INDICATOR_BLINK_ACTION) {
        LED_ACTION = 1;
        __delay_ms(50);
        LED_ACTION = 0;
        __delay_ms(10);
        return;
    }
    if (status == LED_INDICATOR_NOT_CONNECTED) {
        LED_ERROR = 1;
        return;
    }
    /* These states are non-recoverable and require a reset */
    // WiFi setup failed. One blink.
    // TODO: This could be recovered from
    while (status == LED_INDICATOR_WIFI_SETUP_FAILED) {
        LED_ERROR = LED_ACTION = 1;
        __delay_ms(500);
        LED_ERROR = LED_ACTION = 0;
        __delay_ms(500);
    }
    // UUID failed. Two blinks
    while (status == LED_INDICATOR_UUID_FAILED) {
        LED_ERROR = LED_ACTION = 1;
        __delay_ms(166);
        LED_ERROR = LED_ACTION = 0;
        __delay_ms(166);
        LED_ERROR = LED_ACTION = 1;
        __delay_ms(166);
        LED_ERROR = LED_ACTION = 0;
        __delay_ms(500);
    }
}

uint8_t ADC_GetNoiseValue(void) {
    uint16_t adc_result;
    uint8_t noise_value = 0;

    // Ensure the ADC is enabled
    ADC_Enable();

    // Select the desired noise channel (floating pin)
    ADC_ChannelSelect(IO_RA4);

    // Perform multiple ADC readings to accumulate noise
    for (uint8_t i = 0; i < 8; i++) {
        // Start a single ADC conversion
        ADC_ConversionStart();

        // Wait until conversion is done
        while (!ADC_IsConversionDone());

        // Get the result and fold the lower 8 bits into noise_value using XOR
        adc_result = ADC_ConversionResultGet();
        noise_value ^= (adc_result & 0xFF);
    }

    return noise_value;
}

/* Basic UART stuff */

void delayUs(uint16_t us) {
    while (us > 10000) {
        __delay_us(10000);
        us -= 10000;
        CLRWDT();
    }
    while (us > 1000) {
        __delay_us(1000);
        us -= 1000;
        CLRWDT();
    }
    while (us > 100) {
        __delay_us(100);
        us -= 100;
        CLRWDT();
    }
    while (us > 10) {
        __delay_us(10);
        us -= 10;
        CLRWDT();
    }
    while (us > 1) {
        __delay_us(1);
        us -= 1;
        CLRWDT();
    }
}

void reset(bool state) {
    RA3 = state;
}

char readUart(void) {
    return (char) EUSART1_Read();
}

void writeUart(char c) {
    EUSART1_Write((uint8_t) c);
}

/* MQTT Message */

bool sendData(void) {
    if (!ha_mqttEnforceConnection()) return false;

    char uuid[17];
    char topic[64];
    char buffer[72];

    ha_uuidGetHexStr(uuid);
    uint16_t payload_len = 14;
    sprintf(topic, "homeassistant/%s/state", uuid);

    if (!ha_mqttPublishStart(topic, payload_len, false)) return false;
    SEND_PARTIAL_A("{\"temp\": 20.5}")
    return ha_writeSocketEnd();
}

