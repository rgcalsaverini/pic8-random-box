import json
import re
from collections import Counter

RE_FIELD = re.compile(r"\(\(([^:]+):(s):(\d+)\)\)")
LIM_LINE = 40

discovery = {
    "dev": {
        "ids": ["((uuid:s:16))"],
        "name": "((name:s:20))",
        "mf": "Rui Calsaverini",
        "sw": "((HA_SW_VERSION:s:5))",
    },
    "o": {
        "name": "Rui HA",
        "sw": "((HA_SW_VERSION:s:5))",
        "url": "https://rui.calsaverini.com/ha",
    },
    "cmps": {
        "((component->name:s:20))": {
            "p": "((component->platform:s:16))",
            "device_class": "((component->dev_class:s:16))",
            "unit_of_measurement": "((component->unit:s:8))",
            "suggested_display_precision": "1",
            "value_template": "{{value_json.((component->value_key:s:16))}}",
            "unique_id": "((component->name:s:20))_((uuid:s:16))",
        },
    },
    "state_topic": "homeassistant/((uuid:s:16))/state",
    "qos": 0,
    "unique_id": "((uuid:s:16))",
}


def generate_builder(value):
    code = "if (!ha_mqttEnforceConnection()) return false;\n\nchar uuid[17];\nchar topic[64];\n"
    code += f"char buffer[{LIM_LINE + 32}];\n\nha_uuidGetHexStr(uuid);\n"

    if isinstance(value, dict):
        value = json.dumps(value)

    fields = {f[0]: (f[1], f[2]) for f in RE_FIELD.findall(value)}
    fields_count = Counter([m[0] for m in RE_FIELD.findall(value)])
    base = RE_FIELD.sub("", value)
    code += f"uint16_t payload_len = {len(base)}; // base\n"
    for f_name, (tp, f_len) in fields.items():
        f_count = fields_count[f_name]
        if tp == "s":
            code += f"payload_len += {f_count} * strlen({f_name});\n"
    code = code.replace("1 * ", "")
    code += "\nha_discoveryTopicName(topic, platform);\n"
    code += "\nif (!ha_mqttPublishStart(topic, payload_len, false)) return false;\n\n"

    acc_fmt = ""
    acc_args = []

    def partial_send(cd):
        fmt = acc_fmt.replace('"', '\\\"')
        if acc_args:
            cd += f'SEND_PARTIAL("{fmt}", {", ".join(acc_args)})\n'
        else:
            cd += f'SEND_PARTIAL_A("{fmt}")\n'
        return cd

    def fmt_len():
        return len(acc_fmt) - acc_fmt.count("%s") * 2

    i = 0
    while i < len(value):
        c = value[i]
        if value[i: i + 2] == "((":
            last = value.find("))", i)
            if last < 0:
                raise ValueError(value[i:last + 2])
            field = RE_FIELD.findall(value[i:last + 2])
            if not field:
                raise ValueError(value[i:last + 2])
            field = field[0]
            f_name, f_type, f_len = field
            if fmt_len() + int(f_len) >= LIM_LINE - 1:
                code = partial_send(code)
                acc_fmt = ""
                acc_args = []
            acc_fmt += "%s"
            acc_args.append(f_name)
            i = last + 2
            continue
        if fmt_len() + 1 >= LIM_LINE - 1:
            code = partial_send(code)
            acc_fmt = ""
            acc_args = []
        acc_fmt += c
        i += 1

    if acc_fmt:
        code = partial_send(code)
    code += "\nreturn ha_writeSocketEnd();"
    print(code)


generate_builder(discovery)
