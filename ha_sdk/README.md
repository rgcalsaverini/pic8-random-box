## ToDos

- Break socket write into start and end portions
- Break publish to header vs payload
- Break publish loop into checking and recovering the connection and publishing
- Work out an AP search mode