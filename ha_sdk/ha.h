#ifndef PIC8_RANDOM_BOX_HA_H
#define PIC8_RANDOM_BOX_HA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define SEND_PARTIAL(FORMAT, ...) { \
    sprintf(buffer, FORMAT, __VA_ARGS__); \
    if (!ha_sendUartRaw(buffer)) return false; \
}

#define SEND_PARTIAL_A(FORMAT) { \
    sprintf(buffer, FORMAT); \
    if (!ha_sendUartRaw(buffer)) return false; \
}

#define HA_SW_VERSION "0.0.1"

#define HA_SSID "Obatzdaland"
#define HA_PWD "ilDs19On1"
#define HA_BROKER_HOST "192.168.178.42" // 192.168.178.42 192.168.178.10
#define HA_BROKER_PORT 1883

#define MAX_UINT16 65535

#define HA_UART_TX_RETRY_TIMES 50
#define HA_UART_RX_RETRY_TIMES 1000

#define HA_MAX_HOST_LEN 50
#define HA_MAX_SSID_LEN 20
#define HA_MAX_PWD_LEN 20

#define HA_UUID_SIZE 8
#define HA_UUID_ADDR  0x05 // Offset from 0 to prevent common errors
#define HA_UUID_MAGIC_INIT_0 0x21
#define HA_UUID_MAGIC_INIT_1 0x93

#define HA_AP_ADDR 0x15
#define HA_AP_MAGIC_INIT_0 0x67
#define HA_AP_MAGIC_INIT_1 0x26

// Platforms - String Constants
#define HA_P_SENSOR_STR "sensor"
#define HA_P_SWITCH_STR "switch"
#define HA_P_LIGHT_STR "light"
#define HA_P_BINARY_SENSOR_STR "binary_sensor"
#define HA_P_COVER_STR "cover"
#define HA_P_FAN_STR "fan"
#define HA_P_CLIMATE_STR "climate"

// Device Classes - String Constants (Sensor)
#define HA_DC_TEMPERATURE_STR "temperature"
#define HA_DC_HUMIDITY_STR "humidity"
#define HA_DC_PRESSURE_STR "pressure"
#define HA_DC_VOLTAGE_STR "voltage"
#define HA_DC_CURRENT_STR "current"
#define HA_DC_POWER_STR "power"
#define HA_DC_ENERGY_STR "energy"
#define HA_DC_BATTERY_STR "battery"
#define HA_DC_SIGNAL_STRENGTH_STR "signal_strength"
#define HA_DC_ILLUMINANCE_STR "illuminance"
#define HA_DC_FREQUENCY_STR "frequency"
#define HA_DC_DURATION_STR "duration"

// Device Classes - String Constants (Binary Sensor)
#define HA_DC_MOTION_STR "motion"
#define HA_DC_DOOR_STR "door"
#define HA_DC_WINDOW_STR "window"
#define HA_DC_BATTERY_LOW_STR "battery"
#define HA_DC_PROBLEM_STR "problem"
#define HA_DC_CONNECTIVITY_STR "connectivity"


bool is_mqtt_connected = false;

void ha_initialize(
        char (*getCh)(void),
        bool (*rxReady)(void),
        void (*putCh)(char),
        bool (*txReady)(void),
        void (*setReset)(bool),
        void (*delayUs)(uint16_t)
);

bool ha_setupWifi(void);

// Basic UART interface

void ha_delayMs(uint16_t ms);

bool ha_waitForTx(void);

bool ha_waitForRx(void);

void ha_clearBuffer(void);

bool ha_sendUartRaw(const char *str);

bool ha_sendUartRawLen(const char *str, uint16_t len);

bool ha_sendUartCommand(const char *str);

bool ha_consumeSequence(const char *sequence);

bool ha_consumeReady(void);

uint8_t ha_consume2Sequence(const char *sequence1, const char *sequence2);

uint8_t ha_consume3Sequence(const char *sequence1, const char *sequence2, const char *sequence3);

bool ha_consumeSequenceWithLen(const char *sequence, uint16_t len);

bool ha_readUntilDifferent(char *output_ch, const char *ignored);

// AT Commands

typedef enum {
    HA_AT_SUCCESS,
    HA_AT_IRRECOVERABLE,
    HA_AT_FAIL,
    HA_AT_NO_CHANGE,
} ha_at_response;

typedef enum {
    HA_STATUS_GOT_IP,
    HA_STATUS_SOCKET_CONNECTED,
    HA_STATUS_DISCONNECTED,
    HA_STATUS_WIFI_FAIL,
    HA_STATUS_UNKNOWN,
} ha_at_status;

ha_at_response ha_connectToApOnce(char *ssid, char *pwd);

bool ha_connectToAp(void);

bool ha_pinHost(const char *host);

ha_at_response ha_openTCP(const char *host, uint16_t port);

ha_at_response ha_closeSocket(void);

ha_at_response ha_writeSocketStart(uint16_t len);

ha_at_response ha_writeSocketEnd(void);

ha_at_response ha_writeSocket(uint8_t *data, uint16_t len);

uint16_t ha_readSocket(uint8_t *data, uint16_t max_len);

ha_at_status ha_getConnectionStatus(void);

bool ha_isAPSettingsSaved(void);

bool ha_getSavedAPSettings(char *ssid, char *pwd);

void ha_saveAPSettingsToMemory(char *ssid, char *pwd);

// Device ID Commands

bool ha_uuidIsInitialized(void);

void ha_uuidGenerate(uint16_t (*counterGet)(void), uint8_t (*adcGet)(void));

bool ha_uuidGetRaw(uint8_t uuid[HA_UUID_SIZE]);

bool ha_uuidGetHexStr(char *uuid);

// MQTT Commands

uint8_t ha_mqttVariableByteInteger(uint8_t *stream, uint32_t value);

uint16_t ha_mqttString(uint8_t *stream, const char *value);

bool ha_mqttConnect(const char *host, uint16_t port);

bool ha_mqttPing(void);

bool ha_mqttPingAndReconnect(const char *host, uint16_t port);

bool ha_mqttDisconnect(void);

bool ha_mqttPublishStart(const char *topic, uint16_t payload_len, bool retain);

bool ha_mqttPublish(const char *topic, const char *payload, bool retain);

bool ha_mqttEnforceConnection(void);

bool ha_mqttPublishLoop(const char *topic, const char *payload);

/* HA */

typedef struct HAComponentStruct {
    char name[16];
    char platform[16];
    char dev_class[16];
    char unit[8];
    char value_key[16];
} HAComponent;

void ha_discoveryTopicName(char *topic, const char *platform);

bool ha_publishDiscoveryMessage1Component(const char *name, const char *platform, HAComponent *component);

#ifdef __cplusplus
}
#endif
#endif //PIC8_RANDOM_BOX_HA_H
