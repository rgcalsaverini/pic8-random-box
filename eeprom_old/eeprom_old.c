#include "eeprom.h"

uint8_t (*_eeprom_read)(uint16_t);

void (*_eeprom_write)(uint16_t, uint8_t);

void EEPROM_initialize(uint8_t (*read_byte)(uint16_t), void (*write_byte)(uint16_t, uint8_t)) {
    _eeprom_read = read_byte;
    _eeprom_write = write_byte;
}

void EEPROM_writeNBE(uint16_t address, uint8_t *data, uint16_t len) {
    for (uint16_t i = 0; i < len; i++) {
        _eeprom_write(address + i, data[i]);
    }
}

void EEPROM_readNBE(uint16_t address, uint8_t *data, uint16_t len) {
    for (uint16_t i = 0; i < len; i++) {
        data[i] = _eeprom_read(address + i);
    }
}

void EEPROM_writeNLE(uint16_t address, uint8_t *data, uint16_t len) {
    for (uint16_t i = 0; i < len; i++) {
        _eeprom_write(address + i, data[len - i - 1]);
    }
}

void EEPROM_readNLE(uint16_t address, uint8_t *data, uint16_t len) {
    for (uint16_t i = 0; i < len; i++) {
        data[len - i - 1] = _eeprom_read(address + i);
    }
}

void EEPROM_writeU32BE(uint16_t address, uint32_t value) {
    _eeprom_write(address, value >> 24u);
    _eeprom_write(address + 1, value >> 16u);
    _eeprom_write(address + 2, value >> 8u);
    _eeprom_write(address + 3, value);
}

uint32_t EEPROM_readU32BE(uint16_t address) {
    return (
            (uint32_t)(EEPROM_readU16BE(address)) << 16
            | (uint32_t)(EEPROM_readU16BE(address + 2))
    );
}

void EEPROM_writeU32LE(uint16_t address, uint32_t value) {
    _eeprom_write(address + 3, value >> 24u);
    _eeprom_write(address + 2, value >> 16u);
    _eeprom_write(address + 1, value >> 8u);
    _eeprom_write(address, value);
}

uint32_t EEPROM_readU32LE(uint16_t address) {
    return (
            (uint32_t)(EEPROM_readU16LE(address + 2)) << 16
            | (uint32_t)(EEPROM_readU16LE(address))
    );
}

void EEPROM_writeU16BE(uint16_t address, uint16_t value) {
    _eeprom_write(address, value >> 8u);
    _eeprom_write(address + 1, value);
}

uint16_t EEPROM_readU16BE(uint16_t address) {
    return (uint16_t)(_eeprom_read(address) << 8u) | _eeprom_read(address + 1);
}

void EEPROM_writeU16LE(uint16_t address, uint16_t value) {
    _eeprom_write(address + 1, value >> 8u);
    _eeprom_write(address, value);
}

uint16_t EEPROM_readU16LE(uint16_t address) {
    return (uint16_t)(_eeprom_read(address + 1) << 8u) | _eeprom_read(address);
}

void EEPROM_writeU8(uint16_t address, uint8_t value) {
    _eeprom_write(address, value);
}

uint8_t EEPROM_readU8(uint16_t address) {
    return _eeprom_read(address);
}

void EEPROM_writeStrBE(uint16_t address, char *str, uint16_t len) {
    EEPROM_writeU16BE(address, len);
    for (uint16_t i = 0; i < len; i++) {
        _eeprom_write(address + i + 2, str[i]);
    }
}

void EEPROM_writeStrZBE(uint16_t address, char *str) {
    uint16_t i = 0;
    for (; str[i] != 0; i++) {
        _eeprom_write(address + i + 2, str[i]);
    }
    EEPROM_writeU16BE(address, i);
}

uint16_t EEPROM_readStrBE(uint16_t address, char *str) {
    uint16_t len = EEPROM_readU16BE(address);
    for (uint16_t i = 0; i < len; i++) {
        str[i] = _eeprom_read(address + i + 2);
    }
    str[len] = 0;
    return len;
}