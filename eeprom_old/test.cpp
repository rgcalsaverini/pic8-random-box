#include <gtest/gtest.h>
#include "eeprom/eeprom.h"

uint8_t mockBuffer[1024] = {0};


void mockWrite(uint16_t address, uint8_t value) {
    mockBuffer[address] = value;
}

uint8_t mockRead(uint16_t address) {
    return mockBuffer[address];
}

void setup() {
    EEPROM_initialize(mockRead, mockWrite);
    for (uint8_t &i : mockBuffer) i = 0;
}


TEST(EEPROM, WriteOne) {
    setup();
    EEPROM_writeU8(0, 1);
    EEPROM_writeU8(1, 2);
    EEPROM_writeU8(128, 3);
    EEPROM_writeU8(1023, 4);

    ASSERT_EQ(1, EEPROM_readU8(0));
    ASSERT_EQ(2, EEPROM_readU8(1));
    ASSERT_EQ(3, EEPROM_readU8(128));
    ASSERT_EQ(4, EEPROM_readU8(1023));
}


TEST(EEPROM, WriteN_BE) {
    setup();

    uint8_t data[] = {1, 2, 3, 4, 5, 6, 7, 8};
    uint8_t buffer[] = {0, 0, 0, 0, 0, 0, 0, 0};

    EEPROM_writeNBE(0, data, 8);
    EEPROM_readNBE(0, buffer, 8);

    for (uint8_t i = 0; i < 8; i++) {
        ASSERT_EQ(mockBuffer[i], i + 1);
        ASSERT_EQ(buffer[i], i + 1);
    }
}

TEST(EEPROM, WriteN_LE) {
    setup();
    uint8_t data[] = {1, 2, 3, 4, 5, 6, 7, 8};
    uint8_t buffer[] = {0, 0, 0, 0, 0, 0, 0, 0};

    EEPROM_writeNLE(0, data, 8);
    EEPROM_readNLE(0, buffer, 8);

    for (uint8_t i = 0; i < 8; i++) {
        ASSERT_EQ(mockBuffer[i], 8 - i);
        ASSERT_EQ(i + 1, buffer[i]);
    }
}

TEST(EEPROM, WriteU16_BE) {
    setup();

    EEPROM_writeU16BE(9, 0x3039);

    ASSERT_EQ(12345, EEPROM_readU16BE(9));
    ASSERT_EQ(mockBuffer[9], 0x30);
    ASSERT_EQ(mockBuffer[10], 0x39);
}

TEST(EEPROM, WriteU16_LE) {
    setup();

    EEPROM_writeU16LE(9, 0x3039);

    ASSERT_EQ(12345, EEPROM_readU16LE(9));
    ASSERT_EQ(mockBuffer[9], 0x39);
    ASSERT_EQ(mockBuffer[10], 0x30);
}

TEST(EEPROM, WriteU32_BE) {
    setup();

    EEPROM_writeU32BE(500, 0x12345678);

    ASSERT_EQ(0x12345678, EEPROM_readU32BE(500));
    ASSERT_EQ(mockBuffer[500], 0x12);
    ASSERT_EQ(mockBuffer[501], 0x34);
    ASSERT_EQ(mockBuffer[502], 0x56);
    ASSERT_EQ(mockBuffer[503], 0x78);
}

TEST(EEPROM, WriteU32_LE) {
    setup();

    EEPROM_writeU32LE(1000, 0x98765432);

    ASSERT_EQ(0x98765432, EEPROM_readU32LE(1000));
    ASSERT_EQ(mockBuffer[1000], 0x32);
    ASSERT_EQ(mockBuffer[1001], 0x54);
    ASSERT_EQ(mockBuffer[1002], 0x76);
    ASSERT_EQ(mockBuffer[1003], 0x98u);
}

TEST(EEPROM, WriteString_LE) {
    setup();
    char str[] = "Lorem ipsum 123";
    char buffer[30];

    EEPROM_writeStrBE(56, str, 15);
    uint16_t len = EEPROM_readStrBE(56, buffer);

    ASSERT_EQ(len, 15);
    ASSERT_STREQ(buffer, str);
}

TEST(EEPROM, WriteStringZ_LE) {
    setup();
    char str[] = "oans zwoa g'suffa";
    char buffer[30];

    EEPROM_writeStrZBE(20, str);
    uint16_t len = EEPROM_readStrBE(20, buffer);

    ASSERT_EQ(len, 17);
    ASSERT_STREQ(buffer, str);
}



