#ifndef HX711_EEPROM_H
#define HX711_EEPROM_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>


void EEPROM_initialize(uint8_t (*read_byte)(uint16_t), void (*write_byte)(uint16_t, uint8_t));

void EEPROM_writeNBE(uint16_t address, uint8_t *data, uint16_t len);

void EEPROM_readNBE(uint16_t address, uint8_t *data, uint16_t len);

void EEPROM_writeNLE(uint16_t address, uint8_t *data, uint16_t len);

void EEPROM_readNLE(uint16_t address, uint8_t *data, uint16_t len);

void EEPROM_writeU32BE(uint16_t address, uint32_t value);

uint32_t EEPROM_readU32BE(uint16_t address);

void EEPROM_writeU32LE(uint16_t address, uint32_t value);

uint32_t EEPROM_readU32LE(uint16_t address);

void EEPROM_writeU16BE(uint16_t address, uint16_t value);

uint16_t EEPROM_readU16BE(uint16_t address);

void EEPROM_writeU16LE(uint16_t address, uint16_t value);

uint16_t EEPROM_readU16LE(uint16_t address);

void EEPROM_writeU8(uint16_t address, uint8_t value);

uint8_t EEPROM_readU8(uint16_t address);

void EEPROM_writeStrBE(uint16_t address, char *str, uint16_t len);

void EEPROM_writeStrZBE(uint16_t address, char *str);

uint16_t EEPROM_readStrBE(uint16_t address, char *str);


#endif //HX711_EEPROM_H
