cmake_minimum_required(VERSION 3.15)

project(pic8_random_box C CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_C_STANDARD 99)
set(MICROCHIP_MCU PIC18F27K40)
set(CMAKE_TOOLCHAIN_FILE external/cmake-microchip/toolchain.cmake)

# Include directories
include_directories(
        .
        /opt/
        /Applications/microchip/xc8/v2.50/pic/include
)

# Find GTest library
find_package(GTest REQUIRED)
if (GTEST_FOUND)
    include_directories(${GTEST_INCLUDE_DIRS})
else ()
    message(FATAL_ERROR "GTest library not found.")
endif ()

# Compiler flags
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wpedantic")
set(CMAKE_C_FLAGS_DEBUG "-g")
set(CMAKE_C_FLAGS_RELEASE "-O3")

# Si5351
# rm -rf build/* && cmake -B build && cmake --build build && build/test_si5351
add_library(si5351 si5351/si5351.c)
add_executable(test_si5351 si5351/test.cpp si5351/si5351.c)
set_target_properties(test_si5351 PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(test_si5351 PUBLIC ${GTEST_LIBRARIES} pthread)

# IOT ESP8266
# rm -rf build/* && cmake -B build && cmake --build build && build/test_iot_esp8266
add_library(iot_esp8266 iot_esp8266/iot_esp8266.c)
add_executable(test_iot_esp8266 iot_esp8266/test.cpp iot_esp8266/iot_esp8266.c)
set_target_properties(test_iot_esp8266 PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(test_iot_esp8266 PUBLIC ${GTEST_LIBRARIES} pthread)


# HA SDK
# rm -rf build/* && cmake -B build && cmake --build build && build/test_ha_sdk
add_library(ha_sdk ha_sdk/ha.c ha_sdk/example_main.c
        eeprom/test.cpp)
add_executable(test_ha_sdk ha_sdk/test.cpp ha_sdk/ha.c)
set_target_properties(test_ha_sdk PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(test_ha_sdk PUBLIC ${GTEST_LIBRARIES} pthread)

# EEPROM
# rm -rf build/* && cmake -B build && cmake --build build && build/test_eeprom
add_library(eeprom eeprom/eeprom.c)
add_executable(test_eeprom eeprom/test.cpp eeprom/eeprom.c)
set_target_properties(test_eeprom PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(test_eeprom PUBLIC ${GTEST_LIBRARIES} pthread)

# LED LAMP
# rm -rf build/* && cmake -B build && cmake --build build && build/test_led_lamp
add_library(led_lamp led_lamp/led_lamp.c)
# add_executable(test_led_lamp led_lamp/test.cpp led_lamp/led_lamp.c)
# set_target_properties(test_led_lamp PROPERTIES LINKER_LANGUAGE CXX)
# target_link_libraries(test_led_lamp PUBLIC ${GTEST_LIBRARIES} pthread)

# Neo Pixel
# rm -rf build/* && cmake -B build && cmake --build build && build/test_neo_pixel
add_library(neo_pixel neo_pixel/neo_pixel.c)
add_executable(test_neo_pixel neo_pixel/test.cpp neo_pixel/neo_pixel.c)
set_target_properties(test_neo_pixel PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(test_neo_pixel PUBLIC ${GTEST_LIBRARIES} pthread)
