#include "esp8266.h"
#include <string.h>

void ESP8266_initialize(char (*getCh)(), void (*putCh)(char), void (*switchPower)(bool)) {
    _esp8266_getCh = getCh;
    _esp8266_putCh = putCh;
    _esp8266_switchPower = switchPower;
    ESP8266_disable();
}

void _ESP8266_sendUart(const char *str) {
    _ESP8266_sendUartSafe(str, strlen(str));
}

void _ESP8266_sendUartSafe(const char *str, uint16_t len) {
    for (uint16_t count = 0; count < len; count++) {
        _esp8266_putCh(str[count]);
    }
}

esp8266_response_t _ESP8266_sendUartCommand(const char *str) {
    _ESP8266_sendUart(str);
    _esp8266_putCh('\r');
    _esp8266_putCh('\n');
    return _ESP8266_getResponse();
}

esp8266_response_t ESP8266_setMode(esp8266_modes mode) {
    char mode_str[2] = {mode + '0', 0};
    _ESP8266_sendUart("AT+CWMODE_DEF=");
    _ESP8266_sendUart(mode_str);
    _ESP8266_sendUart("\r\n");
    return _ESP8266_getResponse();
}

esp8266_response_t ESP8266_setMultipleConnections(bool allow_multiple) {
    _esp8266_cip_mux = allow_multiple;
    if (allow_multiple) return _ESP8266_sendUartCommand("AT+CIPMUX=1");
    return _ESP8266_sendUartCommand("AT+CIPMUX=0");
}

esp8266_response_t ESP8266_setAutoConnect(bool enabled) {
    if (enabled) return _ESP8266_sendUartCommand("AT+CWAUTOCONN=1");
    return _ESP8266_sendUartCommand("AT+CWAUTOCONN=0");
}

esp8266_response_t ESP8266_setEcho(bool enabled) {
    if (enabled) return _ESP8266_sendUartCommand("ATE1");
    return _ESP8266_sendUartCommand("ATE0");
}

void ESP8266_disable() {
    _esp8266_switchPower(false);
}

void ESP8266_enable() {
    _esp8266_switchPower(true);
    _ESP8266_waitUntilReady();
}

bool ESP8266_connect(const char *ssid, const char *password, bool reconnect) {
    esp8266_status status = ESP8266_getStatus();

    // Connected and has a socket open, should close it
    // to restart clean
    if (status == ESP8266_CREATED) {
        _ESP8266_sendUartCommand("AT+CIPCLOSE"); // Close socket
        status = ESP8266_CONNECTED;
    }
        // Had a socket, but it was closed. No big deal, just proceed.
    else if (status == ESP8266_CLOSED) {

        status = ESP8266_CONNECTED;
    }

    // Already connected to an AP
    if (status == ESP8266_CONNECTED) {
        if (reconnect) {
            if (_ESP8266_sendUartCommand("AT+CWQAP") != ESP8266_OK) return false;
        } else {
            return true;
        }
    }
    // Connect to APad
    _ESP8266_sendUart("AT+CWJAP_DEF=\"");
    _ESP8266_sendUart(ssid);
    _ESP8266_sendUart("\",\"");
    _ESP8266_sendUart(password);
    _ESP8266_sendUart("\"\r\n");
    return _ESP8266_getResponse() == ESP8266_OK && ESP8266_getStatus() == ESP8266_CONNECTED;
}

bool ESP8266_openSocket(esp8266_socket_type socket_type, const char *hostname, const char *port, uint8_t link) {
    _ESP8266_sendUart("AT+CIPSTART=");
    if (_esp8266_cip_mux && link <= 4) {
        char link_str[2] = {link + '0', 0};
        _ESP8266_sendUart(link_str);
        _ESP8266_sendUart(",");
    }
    if (socket_type == ESP8266_TCP) {
        _ESP8266_sendUart("\"TCP\",\"");
    } else {
        _ESP8266_sendUart("\"UDP\",\"");
    }
    _ESP8266_sendUart(hostname);
    _ESP8266_sendUart("\",");
    _ESP8266_sendUart(port);

    if (socket_type == ESP8266_UDP) {
        _ESP8266_sendUart(",");
        _ESP8266_sendUart(port);
        _ESP8266_sendUart(",2");
    }

    _ESP8266_sendUart("\r\n");
    // Should I check additionally for `ESP8266_getStatus() == ESP8266_CREATED;`?
    return _ESP8266_getResponse() == ESP8266_OK;
}

bool ESP8266_closeSocket(uint8_t link) {
    _ESP8266_sendUart("AT+CIPCLOSE=");
    if (_esp8266_cip_mux && link <= 4) {
        char link_str[2] = {link + '0', 0};
        _ESP8266_sendUart(link_str);
    }
    _ESP8266_sendUart("\r\n");
    return _ESP8266_getResponse() == ESP8266_OK;
}

bool ESP8266_sendDataOnceSafe(esp8266_socket_type socket_type,
                              const char *hostname,
                              const char *port,
                              uint8_t link,
                              char *data,
                              uint16_t len,
                              bool close) {
    if (!ESP8266_openSocket(socket_type, hostname, port, link)) return false;
    if (!ESP8266_sendDataSafe(data, len, link)) return false;
    if (!close) return true;
    if (!ESP8266_closeSocket(link)) return false;

    return true;
}

bool ESP8266_sendDataOnce(esp8266_socket_type socket_type,
                          const char *hostname,
                          const char *port,
                          uint8_t link,
                          char *data,
                          bool close) {
    return ESP8266_sendDataOnceSafe(socket_type, hostname, port, link, data, strlen(data), close);
}

bool ESP8266_startServer(const char *port, uint8_t max_conns) {
    _ESP8266_sendUart("AT+CIPSERVERMAXCONN=");
    if (max_conns > 5) max_conns = 5;
    char max_conns_str[2] = {max_conns + '0', 0};
    _ESP8266_sendUart(max_conns_str);
    _ESP8266_sendUart("\r\n");
    _ESP8266_getResponse();

    _ESP8266_sendUart("AT+CIPSERVER=1,");
    _ESP8266_sendUart(port);
    _ESP8266_sendUart("\r\n");
    return _ESP8266_getResponse() == ESP8266_OK;
}

bool ESP8266_stopServer(const char *port) {
    _ESP8266_sendUart("AT+CIPSERVER=0,");
    _ESP8266_sendUart(port);
    _ESP8266_sendUart("\r\n");
    return _ESP8266_getResponse() == ESP8266_OK;
}

bool ESP8266_sendData(char *data, uint8_t link) {
    return ESP8266_sendDataSafe(data, strlen(data), link);
}

bool ESP8266_sendDataSafe(char *data, uint16_t len, uint8_t link) {
    _ESP8266_sendUart("AT+CIPSEND=");
    if (_esp8266_cip_mux && link <= 4) {
        char link_str[2] = {link + '0', 0};
        _ESP8266_sendUart(link_str);
        _ESP8266_sendUart(",");
    }
    char len_str[8];
    sprintf(len_str, "%u", len);
    _ESP8266_sendUart(len_str);
    _ESP8266_sendUart("\r\n");
    if (_ESP8266_getResponse() != ESP8266_OK) return false;

    _ESP8266_sendUartSafe(data, len);
    return _ESP8266_getResponse() == ESP8266_OK;
}

uint8_t ESP8266_HTTP(const char *method,
                     const char *hostname,
                     const char *port,
                     const char *path,
                     const char *body,
                     size_t body_len) {
    _esp8266_http_headers[0] = 0;

    // First line
    strncat(_esp8266_http_headers, method, strlen(method));
    strncat(_esp8266_http_headers, " ", 1);
    strncat(_esp8266_http_headers, path, strlen(path));
    strncat(_esp8266_http_headers, " ", 1);
    strncat(_esp8266_http_headers, "HTTP/1.1\r\n", 10);

    // Headers
    ESP8266_HTTPAddHeader("Host", hostname);
    ESP8266_HTTPAddHeader("User-Agent", "RGC WeatherStation 0.1A");
    ESP8266_HTTPAddHeader("Content-Type", "text/plain");
    if (body_len > 0) {
        char body_len_str[5];
        sprintf(body_len_str, "%zu", body_len);
        ESP8266_HTTPAddHeader("Content-Length", body_len_str);
    }
    // Total size
    uint8_t full_len = strlen(_esp8266_http_headers) + body_len + 2;
    char full_len_str[5];
    sprintf(full_len_str, "%u", full_len);

    // Passive receive mode
    _ESP8266_sendUartCommand("AT+CIPRECVMODE=0");

    // Open TCP connection and send content size
    if (!ESP8266_openSocket(ESP8266_TCP, hostname, port, 0)) return 0;
    _ESP8266_sendUart("AT+CIPSEND=");
    _ESP8266_sendUart(full_len_str);
    _ESP8266_sendUart("\r\n");
    if (_ESP8266_getResponse() != ESP8266_OK) return 1;

    // Send data
    _ESP8266_sendUart(_esp8266_http_headers);
    _ESP8266_sendUart("\r\n");
    if (body_len > 0) _ESP8266_sendUartSafe(body, body_len);

    if (_ESP8266_getResponse() != ESP8266_OK) return 2;

    // // Receive response, use only if on active response mode
    // _ESP8266_sendUartCommand('AT+CIPRECVDATA=1024');

    // Get response, body will be on _esp8266_http_response
    uint8_t status_code = _ESP8266_getHTTPResponse();

    // The server should have closed the connection, but if not, we close it
    if (ESP8266_getStatus() == ESP8266_CREATED) _ESP8266_sendUartCommand("AT+CIPCLOSE");

    return status_code;
}

uint16_t _ESP8266_getRecLen() {
    char *string = "\n+IPD,";
    uint8_t length = 6;
    uint8_t current = 0;
    uint16_t len = 0;
    char ch;

    while (true) {
        ch = _esp8266_getCh();
        if (string[current] == ch) {
            current++;
            if (current == length) break;
        } else {
            current = 0;
        }
    }

    while (true) {
        ch = _esp8266_getCh();
        if (ch < '0' || ch > '9') break;
        len = 10 * len + (ch - '0');
    };

    return len;
}

void _ESP8266_getRecLenAndLink(uint16_t *len, uint8_t *link) {
    char *string = "\n+IPD,";
    uint8_t length = 6;
    uint8_t current = 0;
    uint16_t number = 0;
    char ch;

    *len = 0;

    while (true) {
        ch = _esp8266_getCh();
        if (string[current] == ch) {
            current++;
            if (current == length) break;
        } else {
            current = 0;
        }
    }

    // get first integer
    while (true) {
        ch = _esp8266_getCh();
        if (ch < '0' || ch > '9') break;
        number = 10 * number + (ch - '0');
    };


    // No link
    if (ch == ':' || number > 4) {
        *link = 255;
        *len = number;
        return;
    }
        // Link
    else if (ch == ',') {
        *link = number;
    }
        // Error
    else {
        *link = 255;
        *len = 0;
        return;
    }

    // get second integer
    while (true) {
        ch = _esp8266_getCh();
        if (ch < '0' || ch > '9') break;
        *len = 10 * (*len) + (ch - '0');
    }
}

void _ESP8266_getData(uint16_t len, char *data) {
    for (uint16_t i = 0; i < len; i++) {
        data[i] = _esp8266_getCh();
    }
}

uint8_t _ESP8266_getHTTPStatus() {
    uint16_t len = _ESP8266_getRecLen();
    uint8_t status = 0;
    uint16_t i = 0;
    char ch;

    // Skip until first space
    for (; i < len && ch != ' '; i++) ch = _esp8266_getCh();

    // Read status
    for (; i < len; i++) {
        ch = _esp8266_getCh();
        if (ch < '0' || ch > '9') break;
        status = 10 * status + (ch - '0');
    }
    // Do I need to finish reading? Guess not.
    // for (; i < len; i++) ch = uart[UART0].Read();

    return status;
}

uint8_t _ESP8266_getHTTPResponse() {
    uint8_t status_code = _ESP8266_getHTTPStatus();
    uint16_t len = _ESP8266_getRecLen();
    uint8_t new_lines = 0;
    uint16_t i = 0;
    char ch;

    // Skip headers
    for (; i < len; i++) {
        ch = _esp8266_getCh();
        if (ch == '\n') new_lines++;
        else if (ch != '\r') new_lines = 0;

        if (new_lines == 2) break;
    }

    // Read until end of body, or end of the receive buffer.
    uint8_t j = 0;
    for (; i < len - 1 && j < ESP8266_MAX_RESPONSE_BODY; i++) {
        _esp8266_http_response[j] = _esp8266_getCh();
        j++;
    }
    _esp8266_http_response[j] = 0;

    return status_code;
}

esp8266_response_t _ESP8266_getResponse() {
    char *strings[3] = {"OK", "ERROR", "FAIL"};
    char lengths[3] = {2, 5, 4};
    uint8_t current[3] = {0, 0, 0};
    esp8266_response_t status[3] = {ESP8266_OK, ESP8266_ERROR, ESP8266_FAIL};
    char ch;

    while (true) {
        ch = _esp8266_getCh();
        for (uint8_t i = 0; i < 3; i++) {
            if (strings[i][current[i]] == ch) {
                current[i]++;
                if (current[i] == lengths[i]) {
                    return status[i];
                }
            } else {
                current[i] = 0;
            }
        }
    }
    return ESP8266_FAIL;
}

void _ESP8266_waitUntilReady() {
    char *string = "ready";
    uint8_t length = 5;
    uint8_t current = 0;

    while (true) {
        if (string[current] == _esp8266_getCh()) {
            current++;
            if (current == length) return;
        } else {
            current = 0;
        }
    }
}

esp8266_status ESP8266_getStatus() {
    char *string = "\nSTATUS:";
    uint8_t length = 8;
    uint8_t current = 0;
    esp8266_status status = ESP8266_NO_RESPONSE;
    char ch;

    _ESP8266_sendUart("AT+CIPSTATUS\r\n");

    while (true) {
        ch = _esp8266_getCh();
        if (string[current] == ch) {
            current++;
            if (current == length) {
                ch = _esp8266_getCh();
                if (ch == '2') status = ESP8266_CONNECTED;
                else if (ch == '3') status = ESP8266_CREATED;
                else if (ch == '4') status = ESP8266_CLOSED;
                else if (ch == '5') status = ESP8266_DISCONNECTED;
                else status = ESP8266_OTHER;
                current = 0;
                break;
            }
        } else {
            current = 0;
        }
    }

    if (_ESP8266_getResponse() == ESP8266_OK) {
        return status;
    }
    return ESP8266_STATUS_ERROR;
}