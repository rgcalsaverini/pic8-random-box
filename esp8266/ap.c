#include "ap.h"
#include "esp8266.h"
#include <string.h>

char esp8266_ap_headers[250];

bool ESP8266_createAP(const char *ssid,
                      const char *pwd,
                      const char *ip,
                      uint8_t channel,
                      uint8_t max_conn,
                      bool hidden,
                      bool save) {
    if (save) _ESP8266_sendUart("AT+CWSAP_DEF=\"");
    else _ESP8266_sendUart("AT+CWSAP_CUR=\"");

    // SSID
    _ESP8266_sendUart(ssid);
    _ESP8266_sendUart("\",\"");

    // Password
    if (pwd != NULL) _ESP8266_sendUart(pwd);
    _ESP8266_sendUart("\",");

    // Channel
    char channel_s[] = {0, 0, 0};
    if (channel < 10) {
        channel_s[0] = channel + '0';
    } else {
        channel_s[0] = (channel / 10) + '0';
        channel_s[1] = (channel % 10) + '0';
    }
    _ESP8266_sendUart(channel_s);
    _ESP8266_sendUart(",");

    // Encryption
    if (pwd != NULL) _ESP8266_sendUart("3");
    else _ESP8266_sendUart("0");
    _ESP8266_sendUart(",");

    // Max connections
    char max_conn_s[] = {max_conn + '0', 0};
    _ESP8266_sendUart(max_conn_s);
    _ESP8266_sendUart(",");

    // Hidden
    _ESP8266_sendUart(hidden ? "1\r\n" : "0\r\n");

    if (_ESP8266_getResponse() != ESP8266_OK) return false;

    if (save) _ESP8266_sendUart("AT+CIPAP_DEF=\"");
    else _ESP8266_sendUart("AT+CIPAP_CUR=\"");
    _ESP8266_sendUart(ip);
    _ESP8266_sendUart("\"\r\n");

    return _ESP8266_getResponse() == ESP8266_OK;
}

bool ESP8266_setAPConfig(const char *ip, const char *gateway, const char *netmask, bool save) {
    if (save) _ESP8266_sendUart("AT+CIPAP_DEF=\"");
    else _ESP8266_sendUart("AT+CIPAP_CUR=\"");
    _ESP8266_sendUart(ip);
    _ESP8266_sendUart("\",\"");
    _ESP8266_sendUart(gateway);
    _ESP8266_sendUart("\",\"");
    _ESP8266_sendUart(netmask);
    _ESP8266_sendUart("\"\r\n");
    return _ESP8266_getResponse() == ESP8266_OK;
}

bool ESP8266_getRequest(char *method, char *path, uint8_t *link) {
    uint16_t len;
    _ESP8266_getRecLenAndLink(&len, link);
    uint16_t i = 0;
    char ch = _esp8266_getCh();

    for (; i < len & ch != ' '; i++) {
        method[i] = ch;
        ch = _esp8266_getCh();
        if (i > 9) return false;
    }
    method[i] = 0;
    ch = _esp8266_getCh();
    for (; i < len & ch != ' '; i++) {
        path[i] = ch;
        ch = _esp8266_getCh();
        if (i > 64) return false;
    }
    path[i] = 0;
    for (; i < len; i++);
    return true;
}

bool ESP8266_sendHttpResponse(const char *host,
                              const char *status,
                              const char *data,
                              const char *content_type,
                              uint8_t link) {
    char content_len_str[20];
    uint16_t data_len = strlen(data);
    uint16_t host_len = strlen(host);
    uint16_t status_len = strlen(status);
    uint16_t content_type_len = strlen(content_type);
    sprintf(content_len_str, "%u", data_len);
    uint16_t content_len_str_len = strlen(content_len_str);
    uint16_t len = 89 + content_len_str_len + data_len + host_len + status_len + content_type_len;

    // Send command
    _ESP8266_sendUart("AT+CIPSEND=");
    if (_esp8266_cip_mux && link <= 4) {
        char link_str[2] = {link + '0', 0};
        _ESP8266_sendUart(link_str);
        _ESP8266_sendUart(",");
    }
    char len_str[8];
    sprintf(len_str, "%u", len);
    _ESP8266_sendUart(len_str);
    _ESP8266_sendUartSafe("\r\n", 2);
    if (_ESP8266_getResponse() != ESP8266_OK) return false;

    // Headers
    _ESP8266_sendUartSafe("HTTP/1.1 ", 9);
    _ESP8266_sendUartSafe(status, status_len);
    _ESP8266_sendUart("\r\nConnection: close\r\nContent-Type: ");
    _ESP8266_sendUartSafe(content_type, content_type_len);
    _ESP8266_sendUart("; charset=UTF-8\r\n");
    _ESP8266_sendUart("Host: ");
    _ESP8266_sendUartSafe(host, host_len);
    _ESP8266_sendUart("\r\nContent-Length: ");
    _ESP8266_sendUartSafe(content_len_str, content_len_str_len);
    _ESP8266_sendUartSafe("\r\n\r\n", 4);

    // Body
    _ESP8266_sendUartSafe(data, data_len);
    _ESP8266_sendUartSafe("\r\n\r\n\r\n", 6);

    return ESP8266_closeSocket(link);
}