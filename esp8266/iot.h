
#ifndef ESP8266_IOT_H
#define	ESP8266_IOT_H

#include "./esp8266.h"

void (*_esp8266_broadcastReply)(char*, uint16_t);

uint16_t (*_esp8266_tcpEndpoint)(char*, uint16_t, char**);

char _esp8266_endpointResponse[256];

uint8_t _esp8266_l_bc_l;
uint8_t _esp8266_l_bc_r;
uint8_t _esp8266_l_ser;

void ESP8266_startIOTServer(const char *broadcast_port,
                            const char *server_port,
                            uint8_t link_broadcast_listen,
                            uint8_t link_broadcast_reply,
                            uint8_t link_server,
                            void (*broadcastReply)(char*, uint16_t),
                            uint16_t (*tcpEndpoint)(char*, uint16_t, char**));

void ESP8266_waitForClients();

#endif // ESP8266_IOT_H