#include "esp8266.h"

void ESP8266_setupHTTP() {
    _esp8266_switchPower(true);
    // _ESP8266_waitUntilReady();
    // _ESP8266_sendUartCommand("AT");

    ESP8266_setMode(ESP8266_STATION);
    ESP8266_setMultipleConnections(false);
    ESP8266_setAutoConnect(false);
    ESP8266_setEcho(true);
}
