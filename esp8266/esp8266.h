#ifndef ESP8266_H
#define    ESP8266_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define ESP8266_HTTPAddHeader(name, value) do { strncat(_esp8266_http_headers, name, strlen(name)); strncat(_esp8266_http_headers, ": ", 2); strncat(_esp8266_http_headers, value, strlen(value)); strncat(_esp8266_http_headers, "\r\n", 2); } while(false)
#define ESP8266_MAX_RESPONSE_BODY 50

typedef enum {
    ESP8266_FAIL,
    ESP8266_ERROR,
    ESP8266_OK,
} esp8266_response_t;

typedef enum {
    ESP8266_CONNECTED,
    ESP8266_CREATED,
    ESP8266_CLOSED,
    ESP8266_DISCONNECTED,
    ESP8266_OTHER,
    ESP8266_STATUS_ERROR,
    ESP8266_NO_RESPONSE,
} esp8266_status;

typedef enum {
    ESP8266_STATION = 1,
    ESP8266_SOFTAP = 2,
    ESP8266_SOFTAP_AND_STATION = 3,
} esp8266_modes;

typedef enum {
    ESP8266_TCP,
    ESP8266_UDP,
} esp8266_socket_type;


char _esp8266_http_headers[256];

char _esp8266_http_response[ESP8266_MAX_RESPONSE_BODY];

bool _esp8266_cip_mux = false;

char (*_esp8266_getCh)();

void (*_esp8266_putCh)(char);

void (*_esp8266_switchPower)(bool);

void ESP8266_initialize(char (*getCh)(), void (*putCh)(char), void (*switchESP)(bool));

esp8266_response_t _ESP8266_getResponse();

void _ESP8266_waitUntilReady();

bool ESP8266_connect(const char *ssid, const char *password, bool reconnect);

uint8_t ESP8266_HTTP(const char *method,
                     const char *hostname,
                     const char *port,
                     const char *path,
                     const char *body,
                     size_t body_len);

uint8_t _ESP8266_getHTTPResponse();

uint8_t _ESP8266_getHTTPStatus();

uint16_t _ESP8266_getRecLen();

void _ESP8266_getRecLenAndLink(uint16_t *len, uint8_t *link);

void _ESP8266_getData(uint16_t len, char *data);

/* UART Communication with the ESP8266 */

void _ESP8266_sendUart(const char *str);

void _ESP8266_sendUartSafe(const char *str, uint16_t len);

esp8266_response_t _ESP8266_sendUartCommand(const char *str);

/* ESP8266 Configuration */

esp8266_response_t ESP8266_setMode(esp8266_modes mode);

esp8266_response_t ESP8266_setMultipleConnections(bool allow_multiple);

esp8266_response_t ESP8266_setAutoConnect(bool enabled);

esp8266_response_t ESP8266_setEcho(bool enabled);

void ESP8266_disable();

void ESP8266_enable();

/* Socket-level functions */

bool ESP8266_openSocket(esp8266_socket_type socket_type, const char *hostname, const char *port, uint8_t link);

bool ESP8266_closeSocket(uint8_t link);

/* Higher level functions */

bool ESP8266_startServer(const char *port, uint8_t max_conns);

bool ESP8266_stopServer(const char *port);

bool ESP8266_sendData(char *data, uint8_t link);

bool ESP8266_sendDataSafe(char *data, uint16_t len, uint8_t link);

bool ESP8266_sendDataOnceSafe(esp8266_socket_type socket_type,
                              const char *hostname,
                              const char *port,
                              uint8_t link,
                              char *data,
                              uint16_t len,
                              bool close);

bool ESP8266_sendDataOnce(esp8266_socket_type socket_type,
                          const char *hostname,
                          const char *port,
                          uint8_t link,
                          char *data,
                          bool close);

esp8266_status ESP8266_getStatus();


#endif    /* ESP8266_H */

