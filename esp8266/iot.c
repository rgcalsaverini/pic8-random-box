#include "./iot.h"

void ESP8266_startIOTServer(const char *broadcast_port,
                            const char *server_port,
                            uint8_t link_broadcast_listen,
                            uint8_t link_broadcast_reply,
                            uint8_t link_server,
                            void (*broadcastReply)(char*, uint16_t),
                            uint16_t (*tcpEndpoint)(char*, uint16_t, char**)) {
    _esp8266_broadcastReply = broadcastReply;
    _esp8266_tcpEndpoint = tcpEndpoint;

    _esp8266_l_bc_l = link_broadcast_listen;
    _esp8266_l_bc_r = link_broadcast_reply;
    _esp8266_l_ser = link_server;

    // Setup the ESP configs
    ESP8266_setMode(ESP8266_SOFTAP_AND_STATION);
    ESP8266_setMultipleConnections(true);
    ESP8266_setAutoConnect(false);
    ESP8266_setEcho(true);

    // Start UDP connection to respond to broadcasts
    ESP8266_openSocket(ESP8266_UDP, "0.0.0.0", broadcast_port, _esp8266_l_bc_l);
    
    // Start TCP server that will be actually used for communications
    ESP8266_startServer(server_port, _esp8266_l_ser);
}

void ESP8266_waitForClients() {
    uint8_t link;
    uint16_t len;
    uint16_t reply_len;
    
    _ESP8266_getRecLenAndLink(&len, &link);

    if(link == _esp8266_l_bc_l) {
        _ESP8266_getData(len, _esp8266_endpointResponse);
        _esp8266_broadcastReply(_esp8266_endpointResponse, len);
    } else {
        _ESP8266_getData(len, _esp8266_endpointResponse);
        reply_len = _esp8266_tcpEndpoint(_esp8266_endpointResponse, len, &_esp8266_endpointResponse);

        if (reply_len > 0) {
            ESP8266_sendDataSafe(_esp8266_endpointResponse, reply_len, link);
        }
        // ESP8266_closeSocket(link);
    }
}