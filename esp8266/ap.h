#ifndef PIC8_RANDOM_BOX_AP_H
#define PIC8_RANDOM_BOX_AP_H

#include <stdint.h>
#include <stdbool.h>

/**
 * Creates a new access point.
 *
 * @param ssid SSID name
 * @param pwd Password of the AP. Set to NONE if AP is open. Otherwise, must be at least
 *            8 characters, and must not contain only numbers
 * @param channel   The wifi channel.
 * @param max_conn Maximum number of allowed connections, from 1 to 8
 * @param hidden If set to true, SSID will not be broadcast
 * @param save If set to true, this will be saved on the flash memory
 *
 * @example
 *      // Create an open AP
 *      ESP8266_createAP("MyAP", NULL, 6, 1, false, false);
 *      // Non-broadcast password protected AP.
 *      ESP8266_createAP("Secret", "secret_password", 6, 1, true, false);
 *
 * @return true if it succeeds, and false otherwise.
 */
const char esp8266_status_200[] = "200 OK";

bool ESP8266_createAP(const char *ssid,
                      const char *pwd,
                      const char *ip,
                      uint8_t channel,
                      uint8_t max_conn,
                      bool hidden,
                      bool save);

bool ESP8266_setAPConfig(const char *ip, const char *gateway, const char *netmask, bool save);

bool ESP8266_getRequest(char *method, char *path, uint8_t *link);

bool ESP8266_sendHttpResponse(const char *host,
                              const char *status,
                              const char *data,
                              const char* content_type,
                              uint8_t link);

#endif //PIC8_RANDOM_BOX_AP_H
