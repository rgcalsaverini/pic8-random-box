#ifndef PIC8_RANDOM_BOX_TM1637_H
#define PIC8_RANDOM_BOX_TM1637_H

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

static const uint8_t TM1637_digitToSegment[] = {
        // XGFEDCBA
        0b00111111,    // 0
        0b00000110,    // 1
        0b01011011,    // 2
        0b01001111,    // 3
        0b01100110,    // 4
        0b01101101,    // 5
        0b01111101,    // 6
        0b00000111,    // 7
        0b01111111,    // 8
        0b01101111,    // 9
        0b01110111,    // A
        0b01111100,    // b
        0b00111001,    // C
        0b01011110,    // d
        0b01111001,    // E
        0b01110001     // F
};

static const uint8_t TM1637_digitToSegmentUpsideDown[] = {
        // XGFEDCBA
        0b00111111,    // 0
        0b00110000,    // 1
        0b01011011,    // 2
        0b01111001,    // 3
        0b01110100,    // 4
        0b01101101,    // 5
        0b01101111,    // 6
        0b00111000,    // 7
        0b01111111,    // 8
        0b01111101,    // 9
        0b00000000,    // A
        0b00000000,    // b
        0b00000000,    // C
        0b00000000,    // d
        0b00000000,    // E
        0b00000000     // F
};

static const uint8_t TM1637_minusSegments = 0b01000000;
static const uint8_t TM1637_ErrUpsideDown[2] = {0b01001111, 0b1000010};
static const uint8_t TM1637_Err[2] = {0b01111001, 0b1010000};

uint16_t tm1637_commDelay;
uint16_t tm1637_brightness;

bool TM1637_initialize(
        void (*delayUs)(uint16_t),
        void (*writeClk)(uint8_t),
        void (*writeDat)(uint8_t),
        uint8_t (*readDat)(void),
        uint16_t bitDelay
);

/**
 * Sets display brightness
 * @param brightness 0 (off), 1 (lowest) ... 8 (brightest)
 */
void TM1637_setBrightness(uint8_t brightness);

/**
 * Display arbitrary data on the module.
 *
 * Receives raw segment values as input (A to G) and displays them.
 *
 *       A
 *      ---
 *   F |   | B
 *      -G-
 *  E |   | C
 *     ---
 *     D
 * @param segments An array of size @ref length containing the raw segment values
 * @param length The number of digits to be modified
 * @param pos The position from which to start the modification (0 - leftmost, 3 - rightmost)
 */
void TM1637_setSegments(uint8_t *segments, uint8_t length, uint8_t pos);

/**
 * Show a number, on any base (up to 16)
 *
 * @param base the number base, from 2 to 16
 * @param num a number to display
 * @param leading_zero set to true, if leading zeros are to be displayed
 * @param length the number of digits
 * @param pos the starting digit
 * @param reverse set to true if the display is upside-down
 */
void TM1637_showNumber(int8_t base, uint16_t num, bool leading_zero, uint8_t length, uint8_t pos, bool reverse);

void TM1637_error(uint16_t num, bool reverse);

#endif //PIC8_RANDOM_BOX_TM1637_H
