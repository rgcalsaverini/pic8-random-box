//  Author: avishorp@gmail.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "tm1637.h"

#define TM1637_I2C_COMM1    0x40
#define TM1637_I2C_COMM2    0xC0
#define TM1637_I2C_COMM3    0x80

void (*tm1637_delayUs)(uint16_t);

void (*tm1637_writeClk)(uint8_t);

void (*tm1637_writeDat)(uint8_t);

uint8_t (*tm1637_readDat)(void);

bool TM1637_initialize(
        void (*delayUs)(uint16_t),
        void (*writeClk)(uint8_t),
        void (*writeDat)(uint8_t),
        uint8_t (*readDat)(void),
        uint16_t bitDelay
) {
    tm1637_delayUs = delayUs;
    tm1637_writeClk = writeClk;
    tm1637_writeDat = writeDat;
    tm1637_readDat = readDat;
    tm1637_commDelay = bitDelay;
    return true;
}


void TM1637_start() {
    tm1637_writeDat(0);
    tm1637_delayUs(tm1637_commDelay);
}

void TM1637_stop() {
    tm1637_writeDat(0);
    tm1637_delayUs(tm1637_commDelay);

    tm1637_writeClk(1);
    tm1637_delayUs(tm1637_commDelay);

    tm1637_writeDat(1);
    tm1637_delayUs(tm1637_commDelay);
}

bool TM1637_writeByte(uint8_t in_data) {
    uint8_t data = in_data;

    for (uint8_t i = 0; i < 8; i++) {
        // CLK low
        tm1637_writeClk(0);
        tm1637_delayUs(tm1637_commDelay);

        // Set data bit
        tm1637_writeDat(data & 0x01 ? 1 : 0);
        tm1637_delayUs(tm1637_commDelay);

        // CLK high
        tm1637_writeClk(1);
        tm1637_delayUs(tm1637_commDelay);
        data = data >> 1;
    }

    // Wait for acknowledge
    // CLK to zero
    tm1637_writeClk(0);
    tm1637_writeDat(1);
    tm1637_delayUs(tm1637_commDelay);

    // CLK to high
    tm1637_writeClk(1);
    tm1637_delayUs(tm1637_commDelay);
    uint8_t ack = tm1637_readDat();
    if (ack == 0) tm1637_writeDat(0);
    tm1637_delayUs(tm1637_commDelay);

    tm1637_writeClk(0);
    tm1637_delayUs(tm1637_commDelay);

    return ack;
}

void TM1637_showNumber(int8_t base, uint16_t num, bool leading_zero, uint8_t length, uint8_t pos, bool reverse) {
    bool negative = false;
    if (base < 0) {
        base = -base;
        negative = true;
    }

    uint8_t digits[4];

    if (num == 0 && !leading_zero) {
        // Singular case - take care separately
        for (uint8_t i = 0; i < (length - 1); i++)
            digits[i] = 0;
        digits[length - 1] = reverse ? TM1637_digitToSegmentUpsideDown[0] : TM1637_digitToSegment[0];
    } else {
        for (int i = length - 1; i >= 0; --i) {
            uint8_t idx = reverse ? length - i - 1 : i;
            uint8_t digit = num % base;

            if (digit == 0 && num == 0 && leading_zero == false)
                // Leading zero is blank
                digits[idx] = 0;
            else
                digits[idx] = reverse ? TM1637_digitToSegmentUpsideDown[digit & 0x0f] : TM1637_digitToSegment[digit &
                                                                                                              0x0f];

            if (digit == 0 && num == 0 && negative) {
                digits[idx] = TM1637_minusSegments;
                negative = false;
            }

            num /= base;
        }
    }

    TM1637_setSegments(digits, length, pos);
}

void TM1637_setSegments(uint8_t *segments, uint8_t length, uint8_t pos) {
    // Write COMM1
    TM1637_start();
    TM1637_writeByte(TM1637_I2C_COMM1);
    TM1637_stop();

    // Write COMM2 + first digit address
    TM1637_start();
    TM1637_writeByte(TM1637_I2C_COMM2 + (pos & 0x03));

    // Write the data bytes
    for (uint8_t k = 0; k < length; k++)
        TM1637_writeByte(segments[k]);

    TM1637_stop();

    // Write COMM3 + brightness
    TM1637_start();
    TM1637_writeByte(TM1637_I2C_COMM3 + (tm1637_brightness & 0x0f));
    TM1637_stop();
}

void TM1637_setBrightness(uint8_t brightness) {
    if (brightness > 8) brightness = 8;
    tm1637_brightness = brightness == 0 ? 0 : brightness + 7;
}

void TM1637_clear() {
    uint8_t data[] = {0, 0, 0, 0};
    TM1637_setSegments(data, 4, 0);
}

void TM1637_error(uint16_t num, bool reverse) {
    uint8_t digits[4];
    if (reverse) {
        digits[3] = TM1637_ErrUpsideDown[0];
        digits[2] = TM1637_ErrUpsideDown[1];
        digits[1] = TM1637_ErrUpsideDown[1];
        digits[0] = TM1637_digitToSegmentUpsideDown[num & 0xf];
    } else {
        digits[0] = TM1637_Err[0];
        digits[1] = TM1637_Err[1];
        digits[2] = TM1637_Err[1];
        digits[3] = TM1637_digitToSegment[num & 0xf];
    }
    TM1637_setSegments(digits, 4, 0);
}

