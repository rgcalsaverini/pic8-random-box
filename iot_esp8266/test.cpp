#include <gtest/gtest.h>
#include "iot_esp8266.h"
#include <string>
#include <map>
#include <utility>

using std::string;
using std::map;

std::pair<std::string, std::string> splitOnEqual(const std::string &input) {
    size_t equalPos = input.find('=');
    if (equalPos == std::string::npos) {
        return {input, ""};
    }
    return {input.substr(0, equalPos), input.substr(equalPos + 1)};
}

class MockESP {
public:
    static void setIncoming(string request) {
        MockESP::_idx = 0;
        MockESP::_incoming = std::move(request);
    }

    static char getCh() {
        if (MockESP::_idx >= MockESP::_incoming.size()) {
            std::cerr << "getCh called on index " << MockESP::_idx
                      << " and incoming has a len of " << MockESP::_incoming.size() << "\n";
            exit(1);
        }
        char ch = MockESP::_incoming[_idx];
        MockESP::_idx += 1;
        return ch;
    }

    static void putCh(char ch) {
        std::vector<string> validCmds = {"AT+CIPDINFO", "AT+CIPMUX", "AT+CWMODE_CUR", "AT+CIPSERVER", "AT+CWSAP_CUR",
                                         "AT+CIPAP_CUR"};
        if (ch != '\r' && ch != '\n') MockESP::_outgoing += ch;
        if (ch == '\n') {
            if (MockESP::_outgoing == "ATE0") {
                MockESP::_configs.try_emplace("ATE", "0");
                MockESP::_incoming += "\r\nOK\r\n";
            } else {
                auto [key, value] = splitOnEqual(MockESP::_outgoing);
                bool isValidCmd = std::find(validCmds.begin(), validCmds.end(), key) != validCmds.end();
                if (isValidCmd) {
                    MockESP::_configs.try_emplace(key, value);
                    MockESP::_incoming += "\r\nOK\r\n";
                } else {
                    MockESP::_incoming += "\r\nERROR\r\n";
                }
            }
            MockESP::_outgoing = "";
        }
    }

    static void delay(uint16_t delay) {/* Empty for now */}

    static void setReset(bool value) {
        MockESP::_outgoing = "";
        MockESP::_incoming = "giberish\r\nready\r\n";
        MockESP::_idx = 0;
        MockESP::_configs = {};
    }

    static void resetMock() {
        MockESP::_incoming = "giberish\r\nready\r\n";
        MockESP::_idx = 0;
        MockESP::_configs = {};
    }

    static string getConfig(const string &key) {
        if (MockESP::_configs.contains(key)) {
            return MockESP::_configs.at(key);
        }
        return "";
    }

    static string _incoming;
    static string _outgoing;
    static long _idx;
    static map<string, string, std::less<>> _configs;
};

string MockESP::_outgoing = "";
string MockESP::_incoming = "\r\nready\r\n";
long MockESP::_idx = 0;
map<string, string, std::less<>> MockESP::_configs = {};


//TEST(ESP8266HTTPTest, GetRootPath) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    MockESP::setIncoming(
//            "0,CONNECT\r\n\r\n"
//            "+IPD,0,70,192.168.1.2,57693:GET / HTTP/1.1\r\n"
//            " 192.168.1.1\r\n"
//            "User-Agent: curl/7.79.1\r\n"
//            "Accept: */*\r\n\r\n"
//    );
//
//    esp8266_http_method method;
//    char path[32];
//    char body[64];
//    uint8_t link;
//    esp8266_getHTTPRequest(&method, path, body, &link);
//    EXPECT_EQ(method, HTTP_GET);
//    EXPECT_STREQ(path, "/");
//    EXPECT_STREQ(body, "");
//}

//TEST(ESP8266HTTPTest, GetComplexPath) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    MockESP::setIncoming(
//            "0,CONNECT\r\n\r\n"
//            "+IPD,0,100,50.1.1.1,1:GET /test/longer-path?q=%20a#b HTTP/1.1\r\n"
//            "Host: 192.168.1.1\r\n"
//            "User-Agent: curl/7.79.1\r\n"
//            "Accept: */*\r\n\r\n"
//    );
//    esp8266_http_method method;
//    char path[32];
//    char body[64];
//    uint8_t link;
//    esp8266_getHTTPRequest(&method, path, body, &link);
//    EXPECT_EQ(method, HTTP_GET);
//    EXPECT_STREQ(path, "/test/longer-path?q=%20a#b");
//    EXPECT_STREQ(body, "");
//}

//TEST(ESP8266HTTPTest, GetLongTruncatedPath) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    MockESP::setIncoming(
//            "0,CONNECT\r\n\r\n"
//            "+IPD,0,111,50.1.1.1,1:GET /123456789012345678901234567890abcdefgh HTTP/1.1\r\n"
//            "Host: 192.168.1.1\r\n"
//            "User-Agent: curl/7.79.1\r\n"
//            "Accept: */*\r\n\r\n"
//    );
//    esp8266_http_method method;
//    char path[32];
//    char body[64];
//    uint8_t link;
//    esp8266_getHTTPRequest(&method, path, body, &link);
//    EXPECT_EQ(method, HTTP_GET);
//    EXPECT_STREQ(path, "/123456789012345678901234567890");
//    EXPECT_STREQ(body, "");
//}

//TEST(ESP8266HTTPTest, PostBasicBody) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    MockESP::setIncoming(
//            "0,CONNECT\r\n\r\n"
//            "+IPD,0,90,50.1.1.1,1:POST / HTTP/1.1\r\n"
//            "Host: 192.168.1.1\r\n"
//            "User-Agent: curl/7.79.1\r\n"
//            "Accept: */*\r\n\r\n"
//            "test=1&other=2"
//    );
//    esp8266_http_method method;
//    char path[32];
//    char body[64];
//    uint8_t link;
//    esp8266_getHTTPRequest(&method, path, body, &link);
//    EXPECT_EQ(method, HTTP_POST);
//    EXPECT_STREQ(path, "/");
//    EXPECT_STREQ(body, "test=1&other=2");
//}

//TEST(ESP8266HTTPTest, BrowserGet) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    MockESP::setIncoming(
//            "+IPD,0,509,192.168.1.2,63478:GET / HTTP/1.1\r\n"
//            "Host: 192.168.1.1\r\n"
//            "Connection: keep-alive\r\n"
//            "Cache-Control: max-age=0\r\n"
//            "Upgrade-Insecure-Requests: 1\r\n"
//            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36\r\n"
//            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\r\n"
//            "Accept-Encoding: gzip, deflate\r\n"
//            "Accept-Language: en-US,en;q=0.9,de-DE;q=0.8,de;q=0.7,pt-BR;q=0.6,pt;q=0.5,bn;q=0.4\r\n"
//            "\r\n"
//    );
//    esp8266_http_method method;
//    char path[32];
//    char body[64];
//    uint8_t link;
//    esp8266_getHTTPRequest(&method, path, body, &link);
//    EXPECT_EQ(method, HTTP_GET);
//    EXPECT_STREQ(path, "/");
//    EXPECT_STREQ(body, "");
//}

//TEST(ESP8266HTTPTest, GetConfigPage) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    MockESP::setIncoming(
//            "0,CONNECT\r\n\r\n"
//            "+IPD,0,67,192.168.1.2,57693:GET / HTTP/1.1\r\n"
//            " 192.168.1.1\r\n"
//            "User-Agent: curl/7.79.1\r\n"
//            "Accept: */*\r\n\r\n"
//    );
//    esp8266_setupConfigAP("ConfigTest");
//}

//TEST(ESP8266BasicTest, Initialize) {
//    MockESP::resetMock();
//    esp8266_initialize(MockESP::getCh, MockESP::putCh, MockESP::setReset, MockESP::delay);
//    EXPECT_STREQ(MockESP::getConfig("ATE").c_str(), "0");
//    EXPECT_STREQ(MockESP::getConfig("AT+CIPDINFO").c_str(), "1");
//    EXPECT_STREQ(MockESP::getConfig("AT+CIPMUX").c_str(), "1");
//}

TEST(ESP8266HTTPTest, SeparateUrl) {
    ESP8266UrlParts url_parts;

    // Empty path
    strcpy(url_parts.url, "");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    // Single slash path
    strcpy(url_parts.url, "/");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 1);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    // Single slash path
    strcpy(url_parts.url, "/abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_/%~!'*+,;=():@");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 80);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    // Only fragment
    strcpy(url_parts.url, "#fragment");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_start, 1);
    EXPECT_EQ(url_parts.frag_len, 8);
    // Large fragment
    strcpy(url_parts.url, "#abfgABWXYZ09.-_/%~!'*+,;=():@");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_start, 1);
    EXPECT_EQ(url_parts.frag_len, 29);
    // Fragment with URL
    strcpy(url_parts.url, "/some/path#frag");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 10);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_start, 11);
    EXPECT_EQ(url_parts.frag_len, 4);
    // Fragment with path and query first
    strcpy(url_parts.url, "/path?q=1#frag");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 5);
    EXPECT_EQ(url_parts.num_qa, 1);
    EXPECT_EQ(url_parts.frag_start, 10);
    EXPECT_EQ(url_parts.frag_len, 4);
    // Fragment with path and query after
    strcpy(url_parts.url, "/path#frag?q=1");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 5);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_start, 6);
    EXPECT_EQ(url_parts.frag_len, 8);
    // Fragment with no path and query before
    strcpy(url_parts.url, "?q=1#frag");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 1);
    EXPECT_EQ(url_parts.frag_start, 5);
    EXPECT_EQ(url_parts.frag_len, 4);
    // Query with value and path
    strcpy(url_parts.url, "/?q=1");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 1);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 1);
    EXPECT_EQ(url_parts.qa_key_starts[0], 2);
    EXPECT_EQ(url_parts.qa_key_lens[0], 1);
    EXPECT_EQ(url_parts.qa_value_starts[0], 4);
    EXPECT_EQ(url_parts.qa_value_lens[0], 1);
    // Larger query with value and path
    strcpy(url_parts.url, "/some/longer_path/?My-Query_;='longer%20value'");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 18);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 1);
    EXPECT_EQ(url_parts.qa_key_starts[0], 19);
    EXPECT_EQ(url_parts.qa_key_lens[0], 10);
    EXPECT_EQ(url_parts.qa_value_starts[0], 30);
    EXPECT_EQ(url_parts.qa_value_lens[0], 16);
    // Two queries with values
    strcpy(url_parts.url, "/path?q1=1&q2=2");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 5);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 2);
    EXPECT_EQ(url_parts.qa_key_starts[0], 6);
    EXPECT_EQ(url_parts.qa_key_lens[0], 2);
    EXPECT_EQ(url_parts.qa_value_starts[0], 9);
    EXPECT_EQ(url_parts.qa_value_lens[0], 1);
    EXPECT_EQ(url_parts.qa_key_starts[1], 11);
    EXPECT_EQ(url_parts.qa_key_lens[1], 2);
    EXPECT_EQ(url_parts.qa_value_starts[1], 14);
    EXPECT_EQ(url_parts.qa_value_lens[1], 1);
    // Many queries
    strcpy(url_parts.url,
           "/?k0=v0&k1=v1&k2=v2&k3=v3&k4=v4&k5=v5&k6=v6&k7=v7&k8=v8&k9=v9&k10=v10&k11=v11&k12=v12&k13=v13&k14=v14&k15=v15&k16=v16&k17=v17");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 1);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 18);
    for (int i = 0; i < 18; ++i) {
        char expected_key[4]; // "kN" (2 chars max for key)
        snprintf(expected_key, sizeof(expected_key), "k%d", i);
        EXPECT_EQ(url_parts.qa_key_lens[i], strlen(expected_key));
        EXPECT_EQ(strncmp(&url_parts.url[url_parts.qa_key_starts[i]], expected_key, url_parts.qa_key_lens[i]), 0);
        char expected_value[4]; // "vN" (2 chars max for value)
        snprintf(expected_value, sizeof(expected_value), "v%d", i);
        EXPECT_EQ(url_parts.qa_value_lens[i], strlen(expected_value));
        EXPECT_EQ(strncmp(&url_parts.url[url_parts.qa_value_starts[i]], expected_value, url_parts.qa_value_lens[i]), 0);
    }
    // Query without value
    strcpy(url_parts.url, "?q1");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 1);
    EXPECT_EQ(url_parts.qa_key_starts[0], 1);
    EXPECT_EQ(url_parts.qa_key_lens[0], 2);
    EXPECT_EQ(url_parts.qa_value_lens[0], 0);
    // Query without value but with the equal sign
    strcpy(url_parts.url, "?q1=");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 1);
    EXPECT_EQ(url_parts.qa_key_starts[0], 1);
    EXPECT_EQ(url_parts.qa_key_lens[0], 2);
    EXPECT_EQ(url_parts.qa_value_lens[0], 0);
    // 3 queries without value
    strcpy(url_parts.url, "?q1=&q2&q3=");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    EXPECT_EQ(url_parts.num_qa, 3);
    EXPECT_EQ(url_parts.qa_key_starts[0], 1);
    EXPECT_EQ(url_parts.qa_key_lens[0], 2);
    EXPECT_EQ(url_parts.qa_value_lens[0], 0);
    EXPECT_EQ(url_parts.qa_key_starts[1], 5);
    EXPECT_EQ(url_parts.qa_key_lens[1], 2);
    EXPECT_EQ(url_parts.qa_value_lens[1], 0);
    EXPECT_EQ(url_parts.qa_key_starts[2], 8);
    EXPECT_EQ(url_parts.qa_key_lens[2], 2);
    EXPECT_EQ(url_parts.qa_value_lens[2], 0);
    // Single lonely fragment
    strcpy(url_parts.url, "#");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    strcpy(url_parts.url, "?q1=v1&q2=v2#");
    // Single lonely fragment
    strcpy(url_parts.url, "#");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 0);
    EXPECT_EQ(url_parts.frag_len, 0);
    // Single fragment after stuff
    strcpy(url_parts.url, "?q1=v1&q2=v2#");
    EXPECT_TRUE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.path_end, 0);
    EXPECT_EQ(url_parts.num_qa, 2);
    EXPECT_EQ(url_parts.frag_len, 0);
    // Malformed queries
    strcpy(url_parts.url, "?key=value&=");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_QARGS_KEY);

    strcpy(url_parts.url, "??");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_QARGS_KEY);

    strcpy(url_parts.url, "?key=value?");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_QARGS_VALUE);

    strcpy(url_parts.url, "/?=value");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_QARGS_KEY);

    strcpy(url_parts.url, "/?#");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_QARGS_KEY);

    strcpy(url_parts.url, "/path?#?q=1#frag");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_QARGS_KEY);

    strcpy(url_parts.url, "/path#?q=1#frag");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_FRAGMENT);

    memset(url_parts.url, 'a', 129);
    url_parts.url[129] = '\0';
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, URL_TOO_LONG);

    strcpy(url_parts.url, "/path with spaces?q=1");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, INV_CHAR_ON_PATH);

    strcpy(url_parts.url, "#1234567890123456789012345678901234567");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, FRAG_TOO_LONG);

    strcpy(url_parts.url, "?1234567890123456789012345678901234567");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, QA_KEY_TOO_LONG);

    strcpy(url_parts.url, "?q=12345678901234567890123456789012345678901234567890123456789012345");
    EXPECT_FALSE(esp8266_separateUrl(&url_parts));
    EXPECT_EQ(url_parts.error, QA_VALUE_TOO_LONG);
}


TEST(ESP8266HTTPTest, GetQueryArgs) {
    ESP8266UrlParts url_parts;

    strcpy(url_parts.url, "/lorem-ipsum?q1");
    esp8266_separateUrl(&url_parts);
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "q1"));
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, "q"));
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, "q12"));
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, ""));
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, " q"));
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, "q "));

    strcpy(url_parts.url, "/lorem-ipsum?QueryArg_0=0&QueryArg_1=1&QueryArg_2=2");
    esp8266_separateUrl(&url_parts);
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "QueryArg_0"));
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "QueryArg_1"));
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "QueryArg_2"));

    strcpy(url_parts.url, "/lorem-ipsum?QueryArg_0=0&QueryArg_1=1&QueryArg_2=2#frag");
    esp8266_separateUrl(&url_parts);
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "QueryArg_0"));
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "QueryArg_1"));
    EXPECT_TRUE(esp8266_hasQArg(&url_parts, "QueryArg_2"));

    strcpy(url_parts.url, "/");
    esp8266_separateUrl(&url_parts);
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, "myArg"));

    strcpy(url_parts.url, "#");
    esp8266_separateUrl(&url_parts);
    EXPECT_FALSE(esp8266_hasQArg(&url_parts, "myArg"));
}


TEST(ESP8266HTTPTest, GetQueryArgsValue) {
    ESP8266UrlParts url_parts;
    char value[MAX_QA_VALUE_LEN + 1];

    strcpy(url_parts.url, "/lorem-ipsum?QUERY_ARG=123");
    esp8266_separateUrl(&url_parts);
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "QUERY_ARG", value));
    EXPECT_STREQ(value, "123");

    strcpy(url_parts.url, "?My+Arg=hi%20there");
    esp8266_separateUrl(&url_parts);
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "My+Arg", value));
    EXPECT_STREQ(value, "hi%20there");

    strcpy(url_parts.url, "longer-url-with-stuff?q1=1&q2=2&q3&q4=4&q5=");
    esp8266_separateUrl(&url_parts);
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "q1", value));
    EXPECT_STREQ(value, "1");
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "q2", value));
    EXPECT_STREQ(value, "2");
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "q3", value));
    EXPECT_STREQ(value, "");
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "q4", value));
    EXPECT_STREQ(value, "4");
    EXPECT_TRUE(esp8266_getQArgValue(&url_parts, "q5", value));
    EXPECT_STREQ(value, "");

    strcpy(url_parts.url, "/");
    esp8266_separateUrl(&url_parts);
    EXPECT_FALSE(esp8266_getQArgValue(&url_parts, "q1", value));
    EXPECT_FALSE(esp8266_getQArgValue(&url_parts, "q2", value));
    EXPECT_FALSE(esp8266_getQArgValue(&url_parts, "q3", value));
    EXPECT_FALSE(esp8266_getQArgValue(&url_parts, "q4", value));
    EXPECT_FALSE(esp8266_getQArgValue(&url_parts, "q5", value));

    strcpy(url_parts.url, "#");
    esp8266_separateUrl(&url_parts);
    EXPECT_FALSE(esp8266_getQArgValue(&url_parts, "myArg", value));
}

static std::string r_stream;
static std::string w_stream;
static uint16_t ri = 0;

// Define static helper functions to match the expected function pointer signatures
static char getCh() {
    return r_stream[ri++];
}

static bool rxReady() {
    return ri < r_stream.size();
}

static void putCh(char c) {
    w_stream.append(1, c);
}

static bool txReady() {
    return true;
}

static void setReset(bool r) { /* Irrelevant */ }

static void delayUs(uint16_t d) { /* Irrelevant */ }

TEST(ESP8266EssentialsTest, ConsumeSequence) {
    r_stream = "AT+CWJAP_CUR=\"Obatzdaland\",\"ilDs19On1\"\r\r\n\n"
               "busy p...\r\n\n"
               "WIFI CONNECTED\r\n\n"
               "WIFI GOT IP\r\n";
    w_stream.clear();
    ri = 0;

    esp8266_initialize(getCh, rxReady, putCh, txReady, setReset, delayUs);

    ASSERT_TRUE(esp8266_consumeSequence("WIFI GOT IP\r\n"));
    ASSERT_FALSE(esp8266_consumeSequence("WIFI GOT IP\r\n"));
    ri = 0;
    ASSERT_FALSE(esp8266_consumeSequence("WIFI GOT X"));
    ri = 0;
    ASSERT_TRUE(esp8266_consumeSequence("WIFI CONNECTED\r\n"));

    r_stream = "AT+CIPSTATUS\r\r\nSTATUS:2\r\n\r\nOK\r\n";
    ri = 0;
    ASSERT_TRUE(esp8266_consumeSequence("STATUS:2\r\n"));

}


TEST(ESP8266EssentialsTest, IsConnected) {
    r_stream = "AT+CIPSTATUS\r\r\nSTATUS:2\r\n\r\nOK\r\n";
    ri = 0;
    ASSERT_TRUE(esp8266_isConnectedToAP());
    r_stream = "AT+CIPSTATUS\r\r\nSTATUS:5\r\n\r\nOK\r\n";
    ri = 0;
    ASSERT_FALSE(esp8266_isConnectedToAP());
    r_stream = "STATUS:2\r\n\r\nOK\r\n";
    ri = 0;
    ASSERT_TRUE(esp8266_isConnectedToAP());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}