#include <string.h>

#ifndef __XC8

#include "iot_esp8266.h"

#else
#include "esp8266.h"
#include <xc.h>
#endif

char (*_esp8266_getCh)(void);

void (*_esp8266_putCh)(char);

void (*_esp8266_setReset)(bool);

void (*_esp8266_delayUs)(uint16_t);

bool (*_esp8266_isRxReady)(void);

bool (*_esp8266_isTxReady)(void);

void esp8266_initialize(char (*getCh)(void),
                        bool (*rxReady)(void),
                        void (*putCh)(char),
                        bool (*txReady)(void),
                        void (*setReset)(bool),
                        void (*delayUs)(uint16_t)) {
    _esp8266_getCh = getCh;
    _esp8266_putCh = putCh;
    _esp8266_setReset = setReset;
    _esp8266_delayUs = delayUs;
    _esp8266_isRxReady = rxReady;
    _esp8266_isTxReady = txReady;
}

bool esp8266_setUpAP() {
    _esp8266_setReset(0);
    esp8266_delayMs(100);
    _esp8266_setReset(1);
    esp8266_delayMs(500);
    // if(!esp8266_waitForReady()) while(1) RC6 = 1;

    return (
            // Turn on echo
            esp8266_sendUartCommand("ATE1")
            // Show remote IP and port on +IPD
            && esp8266_sendUartCommand("AT+CIPDINFO=1")
            // Turn on multiplexing
            && esp8266_sendUartCommand("AT+CIPMUX=1")
    );
}

// Low-level commands

bool esp8266_waitForTx() {
    for (uint16_t i = 0; !_esp8266_isTxReady(); i++) {
        if (i > UART_TX_RETRY_TIMES) return false;
        _esp8266_delayUs(10);
    }
    return _esp8266_isTxReady();
}

bool esp8266_waitForRx() {
    for (uint16_t i = 0; !_esp8266_isRxReady(); i++) {
        if (i > UART_RX_RETRY_TIMES) return false;
        _esp8266_delayUs(10);
    }
    return _esp8266_isRxReady();
}

bool esp8266_sendUartRaw(const char *str) {
    return esp8266_sendUartRawLen(str, strlen(str));
}

bool esp8266_sendUartRawLen(const char *str, uint16_t len) {
    for (uint16_t count = 0; count < len; count++) {
        if (!esp8266_waitForTx()) return false;
        _esp8266_putCh(str[count]);
    }
    return true;
}

bool esp8266_sendUartCommand(const char *str) {
    esp8266_sendUartRaw(str);
    esp8266_sendUartRawLen("\r\n", 2);
    return esp8266_getOk();
}

bool esp8266_getOk(void) {
    char ch;
    uint8_t ok_progress = 0;
    uint8_t error_progress = 0;

    while (true) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        if (ch == 'O' && ok_progress == 0) ok_progress = 1;
        else if (ch == 'K' && ok_progress == 1) ok_progress = 2;
        else if (ch == '\r' && ok_progress == 2) ok_progress = 3;
        else if (ch == '\n' && ok_progress == 3) return true;
        else ok_progress = 0;

        if (ch == 'E' && error_progress == 0) error_progress = 1;
        else if (ch == 'R' && error_progress == 1) error_progress = 2;
        else if (ch == 'R' && error_progress == 2) error_progress = 3;
        else if (ch == 'O' && error_progress == 3) error_progress = 4;
        else if (ch == 'R' && error_progress == 4) error_progress = 5;
        else if (ch == '\r' && error_progress == 5) error_progress = 6;
        else if (ch == '\n' && error_progress == 6) return false;
        else error_progress = 0;
    }
}

void esp8266_delayMs(uint16_t ms) {
    while (ms > 100) {
        _esp8266_delayUs(50000);
        _esp8266_delayUs(50000);
        ms -= 100;
    }
    while (ms > 10) {
        _esp8266_delayUs(10000);
        ms -= 10;
    }

    while (ms > 1) {
        _esp8266_delayUs(1000);
        ms -= 1;
    }
}

bool esp8266_consumeSequence(const char *sequence) {
    uint16_t length = strlen(sequence);
    uint16_t progress = 0;
    char ch;

    while (true) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        if (ch == sequence[progress]) progress++;
        else progress = 0;
        if (progress == length) return true;
    }
    return false;
}

bool esp8266_waitForSequence(const char *sequence, uint16_t max_ms_wait) {
    uint16_t loops = max_ms_wait / 10;
    for (uint16_t i = 0; i < loops; i++) {
        if (esp8266_consumeSequence(sequence)) return true;
        esp8266_delayMs(10);
    }
    return false;
}

// AP commands


bool esp8266_createAccessPoint(const char *ssid, const char *pwd) {
    // Configs
    uint8_t channel = 6;
    uint8_t max_conn = 1;
    bool hidden = false;
    bool saveCmd = false;
    char ip[] = "192.168.1.1";
    char gateway_ip[] = "192.168.1.1";
    char netmask_ip[] = "255.255.255.0";

    if (saveCmd) esp8266_sendUartRaw("AT+CWSAP_DEF=\"");
    else esp8266_sendUartRaw("AT+CWSAP_CUR=\"");

    // SSID
    esp8266_sendUartRaw(ssid);
    esp8266_sendUartRaw("\",\"");

    // Password
    if (pwd != NULL) esp8266_sendUartRaw(pwd);
    esp8266_sendUartRaw("\",");

    // Channel
    char channel_s[] = {0, 0, 0};
    if (channel < 10) {
        channel_s[0] = (char) (channel + '0');
    } else {
        channel_s[0] = (char) ((channel / 10) + '0');
        channel_s[1] = (char) ((channel % 10) + '0');
    }
    esp8266_sendUartRaw(channel_s);
    esp8266_sendUartRaw(",");

    // Encryption
    if (pwd != NULL) esp8266_sendUartRaw("3");
    else esp8266_sendUartRaw("0");
    esp8266_sendUartRaw(",");

    // Max connections
    char max_conn_s[] = {(char) (max_conn + '0'), 0};
    esp8266_sendUartRaw(max_conn_s);
    esp8266_sendUartRaw(",");

    // Hidden
    esp8266_sendUartRaw(hidden ? "1\r\n" : "0\r\n");

    if (!esp8266_getOk()) return false;

    // Set access point IP
    if (saveCmd) esp8266_sendUartRaw("AT+CIPAP_DEF=\"");
    else esp8266_sendUartRaw("AT+CIPAP_CUR=\"");
    esp8266_sendUartRaw(ip);
    esp8266_sendUartRaw("\",\"");
    esp8266_sendUartRaw(gateway_ip);
    esp8266_sendUartRaw("\",\"");
    esp8266_sendUartRaw(netmask_ip);
    esp8266_sendUartRaw("\"\r\n");

    return esp8266_getOk();
}

bool esp8266_setupConfigAP(const char *ssid) {
    return (
            // Set Station + SoftAP mode
            esp8266_sendUartCommand("AT+CWMODE_CUR=3")
            && esp8266_createAccessPoint(ssid, NULL)
            && esp8266_sendUartCommand("AT+CIPSERVER=1,80")
    );
}

bool esp8266_isConnectedToAP(void) {
    return esp8266_sendUartRaw("AT+CIPSTATUS\r\n") && esp8266_consumeSequence("STATUS:2\r\n");
}

bool esp8266_connectToAp(char *ssid, char *pwd) {
    char cmd[128];
    sprintf(cmd, "AT+CWJAP_CUR=\"%s\",\"%s\"", ssid, pwd);

    // if (!esp8266_sendUartCommand(cmd) || !esp8266_waitForSequence("WIFI GOT IP\r\n", 2000)) return false;
    esp8266_sendUartCommand(cmd);
    if (esp8266_waitForSequence("WIFI GOT IP\r\n", 2000)) {
        esp8266_sendUartRaw("AT+CIPSERVER=0\r\n"); // Set to station mode
        esp8266_waitForSequence("OK\r\n", 1000);
        esp8266_sendUartCommand("AT+CWMODE_CUR=1"); // Stop listening to port 80
        esp8266_waitForSequence("OK\r\n", 1000);
    }
    return esp8266_isConnectedToAP();


    /*
    char cmd[128];
    sprintf(cmd, "AT+CWJAP_CUR=\"%s\",\"%s\"", ssid, pwd);
    bool successful = false;

    for (uint8_t i = 0; i < 3 && !successful; i++) {
        if (!esp8266_sendUartCommand(cmd)) {
            esp8266_delayMs(1000);
            continue;
        }
        for (uint8_t j = 0; j < 40; j++) {
            successful = esp8266_consumeSequence("WIFI GOT IP\r\n");
            if (successful) break;
            esp8266_delayMs(50);
        }
        if (!successful) continue;
        esp8266_sendUartCommand("AT+CIPSERVER=0"); // Stop listening to port 80
        esp8266_delayMs(100);
        esp8266_sendUartCommand("AT+CWMODE_CUR=1"); // Set station mode
        esp8266_delayMs(250);

        for (uint8_t j = 0; j < 3; j++) {
            if (esp8266_isConnectedToAP()) {
                return true;
            }
            esp8266_delayMs(500);
        }
    }

    return false;*/
}

// HTTP commands
bool esp8266_getReceivedLengthAndLinkID(uint16_t *len, uint8_t *link) {
    uint8_t ipd_current = 0;
    char ch;
    uint16_t number = 0;
    *len = 0;

    while (true) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        if (ch == '+' && ipd_current == 0) ipd_current++;
        else if (ch == 'I' && ipd_current == 1) ipd_current++;
        else if (ch == 'P' && ipd_current == 2) ipd_current++;
        else if (ch == 'D' && ipd_current == 3) ipd_current++;
        else if (ch == ',' && ipd_current == 4) break;
        else ipd_current = 0;
    }

    // get first integer
    while (true) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        if (ch < '0' || ch > '9') break;
        number = 10 * number + (ch - '0');
    }

    // No link
    if (ch == ':' || number > 4) {
        *link = 255;
        *len = number;
        return true;
    } else if (ch == ',') {
        // Link
        *link = number;
    } else {
        // Error
        *link = 255;
        *len = 0;
        return false;
    }

    // get second integer
    while (true) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        if (ch < '0' || ch > '9') break;
        *len = 10 * (*len) + (ch - '0');
    }

    //  Consume text until the end
    while (true) {
        if (!esp8266_waitForRx()) return false;
        if (_esp8266_getCh() == ':') break;
    }
    return true;
}

bool esp8266_getHTTPRequest(esp8266_http_method *method, char *url, uint8_t *link) {
    uint16_t len = 0;
    uint16_t i = 0;
    if (!esp8266_getReceivedLengthAndLinkID(&len, link)) return false;
    len -= 3; // TODO Don't know why, but I seem to always be 3 short?
    char ch;
    *method = HTTP_OTHER_METHOD;
    url[0] = 0;

    // Get method

    char *strings[7] = {"GET", "HEAD", "PUT", "PATCH", "POST", "OPTIONS", "DELETE"};
    uint8_t lengths[7] = {3, 4, 3, 5, 4, 7, 6};
    uint8_t current[7] = {0, 0, 0, 0, 0, 0, 0};
    uint8_t methods[7] = {HTTP_GET, HTTP_HEAD, HTTP_PUT, HTTP_PATCH, HTTP_POST, HTTP_OPTIONS, HTTP_DELETE};
    for (; i < len; i++) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        bool atLeastOneMatch = false;
        for (uint8_t j = 0; j < 7; j++) {
            if (current[j] == i && strings[j][i] == ch) {
                current[j]++;
                atLeastOneMatch = true;
            }
        }
        if (!atLeastOneMatch || ch < 'A' || ch > 'z' || (ch > 'Z' && ch < 'a')) break;
    }
    for (uint8_t j = 0; j < 7; j++) {
        if (current[j] == lengths[j]) {
            *method = methods[j];
            break;
        }
    }

    // Get url
    uint8_t j = 0;
    for (; i < len && j < MAX_URL_LEN - 1; i++) {
        if (!esp8266_waitForRx()) return false;
        ch = _esp8266_getCh();
        if (ch == ' ') break;
        url[j] = ch;
        j++;
    }
    url[j++] = 0;

    // Skip to end
    while (esp8266_waitForRx()) _esp8266_getCh();

    return true;
}

void esp8266_404(uint8_t link_id) {
    esp8266_response(link_id, "404 Not Found", "application/json", "{\"error\": \"not_found\"}");
}

void esp8266_405(uint8_t link_id) {
    esp8266_response(link_id, "405 Method Not Allowed", "application/json", "{\"error\": \"method_not_allowed\"}");
}

void esp8266_400(uint8_t link_id, const char *err, const char *details) {
    char msg[64];
    if (details[0] != 0) sprintf(msg, "{\"error\": \"%s\", \"details\": \"%s\"}", err, details);
    else
        sprintf(msg, "{\"error\": \"%s\"}", err);
    esp8266_response(link_id, "400 Bad Request", "application/json", msg);
}

void esp8266_response(uint8_t link_id, const char *status, const char *content_type, const char *body) {
    char header[256];
    uint16_t body_length = strlen(body);

    // Create the HTTP headers
    sprintf(header,
            "HTTP/1.1 %s\r\n"
            "Content-Type: %s\r\n"
            "Content-Length: %d\r\n"
            "\r\n",
            status, content_type, body_length);
    char at_send_cmd[20];
    sprintf(at_send_cmd, "AT+CIPSEND=%u,%u", link_id, (uint16_t) strlen(header) + body_length);

    // Send the headers and body
    esp8266_sendUartCommand(at_send_cmd);
    esp8266_sendUartRaw(header);
    esp8266_sendUartRaw(body);
    esp8266_sendUartCommand("AT+CIPCLOSE=0");
}


bool esp8266_separateUrl(ESP8266UrlParts *url_parts) {
    uint16_t i;
    url_parts->num_qa = url_parts->frag_len = url_parts->frag_start = 0;
    enum {
        PATH, KEY, VALUE, FRAGMENT
    } state = PATH;
    bool first_char = false;

    for (i = 0; url_parts->url[i] != 0; i++) {
        if (i >= MAX_URL_LEN) {
            url_parts->error = URL_TOO_LONG;
            return false;
        }
        char ch = url_parts->url[i];
        bool valid_chr = (
                (ch >= 'a' && ch <= 'z') ||
                (ch >= 'A' && ch <= 'Z') ||
                (ch >= '0' && ch <= '9') ||
                ch == '-' || ch == '_' || ch == '.' || ch == '~' ||
                ch == '!' || ch == '$' || ch == '&' || ch == '\'' ||
                ch == '*' || ch == '+' || ch == ',' || ch == ';' ||
                ch == '=' || ch == '(' || ch == ')' || ch == ':' ||
                ch == '@' || ch == '%' || ch == '/'
        );

        if (state == PATH) {
            if (valid_chr) continue;
            url_parts->path_end = i;
            if (ch == '?') {
                state = KEY;
                first_char = true;
            } else if (ch == '#') {
                state = FRAGMENT;
                first_char = true;
            } else {
                url_parts->error = INV_CHAR_ON_PATH;
                return false;
            }
        } else if (state == KEY) {
            if (first_char) {
                // Start a new arg
                if (!valid_chr || ch == '=' || ch == '&') {
                    url_parts->error = INV_CHAR_ON_QARGS_KEY;
                    return false;
                }
                first_char = false;
                url_parts->num_qa++;
                url_parts->qa_key_starts[url_parts->num_qa - 1] = i;
                url_parts->qa_key_lens[url_parts->num_qa - 1] = 1;
                url_parts->qa_value_lens[url_parts->num_qa - 1] = 0;
            } else if (ch == '=') {
                // Has value, move to reading it
                first_char = true;
                state = VALUE;
            } else if (ch == '&') {
                // No value, ends this arg
                first_char = true;
            } else if (valid_chr) {
                // Key continues
                url_parts->qa_key_lens[url_parts->num_qa - 1]++;
                if (url_parts->qa_key_lens[url_parts->num_qa - 1] > MAX_QA_KEY_LEN) {
                    url_parts->error = QA_KEY_TOO_LONG;
                    return false;
                }
            } else if (ch == '#') {
                first_char = true;
                state = FRAGMENT;
            } else {
                url_parts->error = INV_CHAR_ON_QARGS_KEY;
                return false;
            }
        } else if (state == VALUE) {
            if (ch == '&') {
                // New arg
                state = KEY;
                first_char = true;
            } else if (ch == '#') {
                first_char = true;
                state = FRAGMENT;
            } else if (!valid_chr) {
                url_parts->error = INV_CHAR_ON_QARGS_VALUE;
                return false;
            } else if (first_char) {
                first_char = false;
                url_parts->qa_value_starts[url_parts->num_qa - 1] = i;
                url_parts->qa_value_lens[url_parts->num_qa - 1] = 1;
            } else {
                url_parts->qa_value_lens[url_parts->num_qa - 1]++;
                if (url_parts->qa_value_lens[url_parts->num_qa - 1] > MAX_QA_VALUE_LEN) {
                    url_parts->error = QA_VALUE_TOO_LONG;
                    return false;
                }
            }
        } else if (state == FRAGMENT) {
            if (!valid_chr && ch != '?') {
                url_parts->error = INV_CHAR_ON_FRAGMENT;
                return false;
            } else if (first_char) {
                first_char = false;
                url_parts->frag_start = i;
                url_parts->frag_len = 1;
            } else {
                url_parts->frag_len++;
                if (url_parts->frag_len > MAX_FRAG_LEN) {
                    url_parts->error = FRAG_TOO_LONG;
                    return false;
                }
            }
        }
    }
    if (state == PATH) url_parts->path_end = i;

    if (url_parts->path_end > MAX_PATH_LEN) {
        url_parts->error = PATH_TOO_LONG;
        return false;
    }

    url_parts->error = NO_ERROR;
    return true;
}

uint16_t esp8266_getQArgIdx(const ESP8266UrlParts *url_parts, const char *key) {
    uint16_t key_len = strlen(key);
    if (url_parts->error != NO_ERROR) return 255;

    for (uint8_t i = 0; i < url_parts->num_qa; i++) {
        if (url_parts->qa_key_lens[i] == key_len) {
            char key_str[MAX_QA_KEY_LEN];
            memcpy(key_str, &url_parts->url[url_parts->qa_key_starts[i]], key_len);
            key_str[key_len] = 0;
            if (strcmp(key_str, key) == 0) return i;
        }
    }

    return 255;
}

bool esp8266_hasQArg(const ESP8266UrlParts *url_parts, const char *key) {
    uint16_t idx = esp8266_getQArgIdx(url_parts, key);
    return idx != 255;
}

bool esp8266_getQArgValue(const ESP8266UrlParts *url_parts, const char *key, char *value) {
    uint16_t idx = esp8266_getQArgIdx(url_parts, key);
    if (idx == 255) return false;
    if (url_parts->qa_value_lens[idx] == 0) {
        value[0] = 0;
    } else {
        memcpy(value, &url_parts->url[url_parts->qa_value_starts[idx]], url_parts->qa_value_lens[idx]);
        value[url_parts->qa_value_lens[idx]] = 0;
    }
    return true;
}


bool esp8266_request(ESP8266Request *req) {
    char url[MAX_URL_LEN + 1];

    if (!esp8266_getHTTPRequest(&req->method, url, &req->link)) return false;
    strcpy(req->url.url, url);
    if (!esp8266_separateUrl(&req->url)) {
        if (req->url.error == URL_TOO_LONG) esp8266_400(req->link, "url_too_long", "");
        else if (req->url.error == INV_CHAR_ON_PATH) esp8266_400(req->link, "invalid_char_on_path", req->url.url);
        else if (req->url.error == INV_CHAR_ON_QARGS_KEY)
            esp8266_400(req->link, "invalid_char_in_qargs_key", req->url.url);
        else if (req->url.error == INV_CHAR_ON_QARGS_VALUE)
            esp8266_400(req->link, "invalid_char_in_qargs_value", req->url.url);
        else if (req->url.error == INV_CHAR_ON_FRAGMENT)
            esp8266_400(req->link, "invalid_char_in_fragment", req->url.url);
        else if (req->url.error == QA_KEY_TOO_LONG) esp8266_400(req->link, "qargs_key_too_long", req->url.url);
        else if (req->url.error == QA_VALUE_TOO_LONG) esp8266_400(req->link, "qargs_value_too_long", req->url.url);
        else if (req->url.error == PATH_TOO_LONG) esp8266_400(req->link, "qargs_path_too_long", req->url.url);
        else if (req->url.error == FRAG_TOO_LONG) esp8266_400(req->link, "frag_too_long", req->url.url);
        return false;
    }

    return true;
}
