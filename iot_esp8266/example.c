#include <string.h>

#ifndef __XC8

#include "../../electronics/embedded/pic18f27k40_2_wifi.X/mcc_generated_files/system/system.h"
#include "iot_esp8266.h"

#define __delay_us(A) __asm__("nop")
#define __delay_ms(A) __asm__("nop")
#define INTERRUPT_GlobalInterruptEnable() __asm__("nop")
#define INTERRUPT_PeripheralInterruptEnable() __asm__("nop")
uint8_t RA3;
uint8_t RA5;
uint8_t RC6;
#else
#include "mcc_generated_files/system/system.h"
#include "esp8266.h"
#endif

#define pathMatches(R, P) strncmp(P, R.url.url, R.url.path_end) == 0

bool is_connected = false;

const char configPage[] = "<!doctypehtml><html lang=en><tit"
                          "le>Config IoT WiFi</title><style>body{background-color:#192126;color:#fff;fon"
                          "t-family:sans-serif}#container{display:flex;position:fixed;align-items:center"
                          ";justify-content:center;top:0;left:0;bottom:0;right:0}#box{border-radius:6px;"
                          "background-color:#2b3c48;border:1px solid #5c7383;max-width:350px;width:calc("
                          "100vw - 12px);filter:drop-shadow(0 0 16px #080d10)}#header{width:100%;backgro"
                          "und-color:#5c7383;border-radius:5px 5px 0 0;padding:6px;box-sizing:border-box"
                          ";font-weight:bolder;color:#192126;text-align:center;font-size:18px}#contents{"
                          "padding:12px;display:flex;flex-direction:column}.info{padding:4px 4px 4px 8px"
                          ";margin:2px;border-left:3px solid #5c7383;color:#adc2d5;font-size:14px}.label"
                          "{margin:20px 0 8px 0}.input-text{border:none;outline:0;background-color:#3445"
                          "50;border-bottom:2px solid #5c7383;width:calc(100% - 4px);padding:4px;border-"
                          "radius:4px;box-sizing:border-box;font-size:16px;color:#fff}button{margin:12px"
                          " 0 0 0;border:none;padding:6px 12px;align-self:center;background-color:#5c738"
                          "3;border-radius:6px;font-size:18px;color:#192126;font-weight:bolder;cursor:po"
                          "inter;transition:all ease-in-out .2s}button:hover{filter:drop-shadow(0 0 4px "
                          "#080d10)}button:active{opacity:.8;transform:scale(.9);filter:drop-shadow(0 0 "
                          "0 #080d10)}#credit{position:fixed;bottom:3px;right:12px;opacity:.15;text-alig"
                          "n:right;font-size:24px}</style><div id=container><div id=box><div id=header>S"
                          "SID Configuration</div><div id=contents><div class=info>Provide the credentia"
                          "ls for the WiFi network so the device can communicate with the internet.</div"
                          "><div class=label>SSID (Network name)</div><input class=input-text id=ssid><d"
                          "iv class=label>Password</div><input class=input-text id=pwd> <button onclick="
                          "sendData()>Save</button></div></div><div id=credit>by Rui Calsaverini</div></"
                          "div><script>const sendData=()=>{fetch('/ap?ssid='+document.getElementById('ss"
                          "id').value+'&pwd='+document.getElementById('pwd').value, {method: 'PUT'});}</script>";


void delayUs(uint16_t us);

void reset(bool state);

char readUart(void);

void writeUart(char c);

void setAp(const ESP8266Request *request);


int main(void) {
    SYSTEM_Initialize();
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();

    esp8266_initialize(readUart,
                       EUSART1_IsRxReady,
                       writeUart,
                       EUSART1_IsTxReady,
                       reset,
                       delayUs);

    if (!esp8266_setUpAP()) while (1) RC6 = 1;
    if (!esp8266_setupConfigAP("Test1")) while (1) RC6 = 1;
    ESP8266Request request;
    uint16_t cycle = 0;
    is_connected = esp8266_isConnectedToAP();

    while (1) {
        cycle++;

        if (is_connected) {
            RC6 = 1;
            continue;
        } else if (cycle == 100) {
            RC6 = 1;
            cycle = 0;
            uint8_t new_is_connected = esp8266_isConnectedToAP();
            RC6 = 0;

            if (!new_is_connected && is_connected) {
                if (!esp8266_setUpAP()) while (1) RC6 = 1;
                if (!esp8266_setupConfigAP("Test1")) while (1) RC6 = 1;
            }

            is_connected = new_is_connected;
        }
        RA5 = 1;
        __delay_ms(10);
        RA5 = 0;
        __delay_ms(10);

        if (esp8266_request(&request)) {
            if (request.url.path_end == 0 || pathMatches(request, "/")) {
                if (request.method == HTTP_GET) esp8266_response(request.link, "200 OK", "text/html", configPage);
                else esp8266_405(request.link);
            } else if (pathMatches(request, "/ap") || pathMatches(request, "/ap/")) {
                setAp(&request);
            } else {
                esp8266_404(request.link);
            }
        }

    }
}


void delayUs(uint16_t us) {
    while (us > 10000) {
        __delay_us(10000);
        us -= 10000;
    }
    while (us > 1000) {
        __delay_us(1000);
        us -= 1000;
    }
    while (us > 100) {
        __delay_us(100);
        us -= 100;
    }
    while (us > 10) {
        __delay_us(10);
        us -= 10;
    }
    while (us > 1) {
        __delay_us(1);
        us -= 1;
    }
}

void reset(bool state) {
    RA3 = state;
}

char readUart(void) {
    return (char) EUSART1_Read();
}

void writeUart(char c) {
    EUSART1_Write((uint8_t) c);
}

void setAp(const ESP8266Request *request) {
    if (request->method == HTTP_GET) {
        esp8266_response(request->link, "200 OK", "application/json",
                         is_connected ? "{\"connected\": true}" : "{\"connected\": false}");
        return;
    } else if (request->method != HTTP_PUT) {
        esp8266_405(request->link);
        return;
    }
    if (!esp8266_hasQArg(&request->url, "ssid")) {
        esp8266_400(request->link, "missing_query_arg", "ssid");
        return;
    }
    if (request->url.qa_value_lens[esp8266_getQArgIdx(&request->url, "ssid")] >= 32) {
        esp8266_400(request->link, "ssid_too_long", "");
        return;
    }
    if (!esp8266_hasQArg(&request->url, "pwd")) {
        esp8266_400(request->link, "missing_query_arg", "pwd");
        return;
    }
    if (request->url.qa_value_lens[esp8266_getQArgIdx(&request->url, "pwd")] >= 32) {
        esp8266_400(request->link, "pwd_too_long", "");
        return;
    }

    char ssid[32];
    char pwd[32];
    esp8266_getQArgValue(&request->url, "ssid", ssid);
    esp8266_getQArgValue(&request->url, "pwd", pwd);

    esp8266_response(request->link, "200 OK", "application/json", "{\"status\": \"pending\"}");
    for (uint8_t i = 0; i < 3; i++) {
        is_connected = esp8266_connectToAp(ssid, pwd);
        if (is_connected) break;
    }
}
