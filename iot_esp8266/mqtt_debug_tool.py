import math
import socket
import time
from string import ascii_letters, digits, punctuation
from termcolor import colored

HOST = '127.0.0.1'
PORT = 1883


def print_hex(value: bytearray | bytes, header):
    result = '      '
    row_len = 10
    valid_chars = ascii_letters + digits + punctuation
    internal_width = row_len * 4 + 6
    show_bin = len(value) < 50

    print(colored(header, 'light_blue'))

    if len(value) == 0:
        return

    for v in range(row_len // 2):
        result += colored((str(v) + '    ')[0:3], 'light_grey')
    result += '   '
    for v in range(row_len // 2, row_len):
        result += colored((str(v) + '    ')[0:3], 'light_grey')
    result += '\n'
    result += '    ╭' + '─' * internal_width + '╮\n'

    for row_n in range(math.ceil(len(value) / row_len)):
        sub_val = value[row_n * row_len:row_n * row_len + row_len]

        block1 = [f'00{hex(v)[2:].upper()}'[-2:] for v in sub_val[0:row_len // 2]]
        # block1 = [colored(c, 'white') if i % 2 == 0 else c for i, c in enumerate(block1)]
        block2 = [f'00{hex(v)[2:].upper()}'[-2:] for v in sub_val[row_len // 2:]]
        # block2 = [colored(c, 'white') if i % 2 == 0 else c for i, c in enumerate(block2)]

        text = []
        for c_int in sub_val:
            c = chr(c_int)
            if c in valid_chars:
                text.append(c)
            elif c_int == 0:
                text.append('∅')
            elif c in '\n\r':
                text.append('⏎')
            elif c in ' \t':
                text.append('␣')
            else:
                text.append('•')

        text = (
            (''.join(text) + ' ' * 1000)[:row_len]
            .replace('•', colored('•', 'red'))
            .replace('∅', colored('∅', 'dark_grey'))
            .replace('⏎', colored('⏎', 'dark_grey'))
        )

        row_number = colored(('    ' + str(row_n * row_len))[-3:], 'light_grey') + ' │ '

        hex_part = ' '.join(block1) + '    ' + ' '.join(block2) + ' ' * 1000
        hex_part = hex_part[:row_len * 3 + 2]

        row = row_number + hex_part + '  ' + ''.join(text) + ' │'
        result += f"{row}\n"

    if show_bin:
        result += ' ───┼' + ('─' * internal_width) + '┤\n'
        bytes_fit = (internal_width - 1) // 15
        nums = []
        for v in value:
            num = ''.join([
                colored(c, 'light_grey') if c == '0' else colored(c, 'white')
                for c in list(('0000000' + bin(v)[2:])[-8:])
            ])
            num = f"{num} ❲{colored(v, 'red')}❳"
            if v < 10:
                num += '  '
            elif v < 100:
                num += ' '
            nums.append(num)
        for i in range(math.ceil(len(value) / bytes_fit)):
            row_bts = nums[i * bytes_fit:i * bytes_fit + bytes_fit]
            row = ' '.join(row_bts) + (' ' * (internal_width - 15 * len(row_bts))) + '│'
            row_number = colored(('    ' + str(i * bytes_fit))[-3:], 'light_grey') + ' │ '
            result += f'{row_number}{row}\n'
    result += '    ╰' + '─' * internal_width + '╯\n'
    print(result)


def mqtt_variable_byte_integer(stream: bytearray, value: int) -> int:
    i = 0
    while True:
        encoded_byte = value % 128
        value //= 128
        if value > 0:
            encoded_byte |= 128
        stream.append(encoded_byte)
        i += 1
        if value == 0:
            break
    return i


def mqtt_uint16(stream: bytearray, value: int) -> None:
    stream.append((value >> 8) & 0xFF)
    stream.append(value & 0xFF)


def mqtt_uint32(stream: bytearray, value: int) -> None:
    stream.append((value >> 24) & 0xFF)
    stream.append((value >> 16) & 0xFF)
    stream.append((value >> 8) & 0xFF)
    stream.append(value & 0xFF)


def mqtt_string(stream: bytearray, value: str) -> int:
    length = len(value)
    stream.append((length >> 8) & 0xFF)
    stream.append(length & 0xFF)
    stream.extend(value.encode('utf-8'))
    return length + 2


def connect_message() -> bytearray:
    return bytearray([0x10, 0x0C, 0x00, 0x04, 0x4D, 0x51, 0x54, 0x54, 0x04, 0x02, 0x00, 0x3C, 0x00, 0x00])
    # pkg = bytearray()
    # pkg.append(0b00010000)  # MQTT Control Packet Type (CONNECT)
    # mqtt_variable_byte_integer(pkg, 12)  # Remaining Length
    # mqtt_string(pkg, "MQTT")  # Protocol Name
    # pkg.append(4)  # Protocol Level (MQTT 5.0)
    # pkg.append(2)  # Connect Flags (Clean Session = 1)
    # mqtt_uint16(pkg, 60)  # Keep Alive (60 seconds)
    # pkg.append(0)  # Empty properties
    # pkg.append(0)  # Client ID
    # return pkg


def ping_req_message():
    return bytearray([0xC0, 0])


def disconnect_message():
    return bytearray([0xE0, 0])


def publish_message(topic, payload, retain=False):
    pkg = bytearray([0x30 | retain])
    topic_encoded = bytearray()
    mqtt_string(topic_encoded, topic)
    mqtt_variable_byte_integer(pkg, len(topic_encoded) + len(payload))
    pkg.extend(topic_encoded)
    pkg.extend(map(ord, payload))
    return pkg


def validate_conn_ack(msg: bytearray) -> bool:
    return msg[0] == 0x20 and msg[1] == 0x02 and msg[3] == 0x0


def validate_ping_resp(msg: bytearray) -> bool:
    return msg[0] == 0xD0 and msg[1] == 0x00


def get_response(client):
    start = time.time()
    response = client.recv(1024)  # Buffer size
    if not response:
        elapsed = time.time() - start
        if elapsed < 0.001:
            elapsed = "instantly"
        elif elapsed < 1:
            elapsed = f"after {round(elapsed * 1000)}ms"
        elif elapsed < 5:
            elapsed = f"after {round(elapsed * 10) / 10.0}s"
        else:
            elapsed = f"after {round(elapsed)}s"
        print(colored(f"Connection closed {elapsed}", "light_red"))
    else:
        print_hex(response, f"Received {len(response)} bytes response:")
        return response


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
    try:
        client_socket.connect((HOST, PORT))
        print(colored(f"Connected to broker at {HOST}:{PORT}", 'light_green'))

        # Connect message
        connect_msg = connect_message()
        client_socket.sendall(connect_msg)
        print_hex(connect_msg, f"Sent {len(connect_msg)} bytes:")

        # Receive data from the server
        if validate_conn_ack(get_response(client_socket)):
            print(colored("CONNACK received", 'light_green'))
        else:
            print(colored("Invalid CONNACK", 'light_red'))
            exit(1)

        ping_msg = ping_req_message()
        client_socket.sendall(ping_msg)
        print_hex(ping_msg, f"Sent {len(ping_msg)} bytes:")
        if validate_ping_resp(get_response(client_socket)):
            print(colored("PINGRESP received", 'light_green'))
        else:
            print(colored("Invalid PINGRESP", 'light_red'))
            exit(1)

        # Publish
        pub_msg = publish_message("foo/bar", '{"temp":18.2,"hum":30,"lux":123}')
        client_socket.sendall(pub_msg)
        print_hex(pub_msg, f"Sent {len(pub_msg)} bytes:")
        get_response(client_socket)

        # Disconnect
        disconnect_msg = disconnect_message()
        client_socket.sendall(disconnect_msg)
        print_hex(disconnect_msg, f"Sent {len(disconnect_msg)} bytes:")
    except ConnectionError as e:
        print(f"Connection error: {e}")

colors = [
    "black",
    "grey",
    "red",
    "green",
    "yellow",
    "blue",
    "magenta",
    "cyan",
    "light_grey",
    "dark_grey",
    "light_red",
    "light_green",
    "light_yellow",
    "light_blue",
    "light_magenta",
    "light_cyan",
    "white",
]
for c in colors:
    print(colored(c, c))
print("none")
