#ifndef PIC8_RANDOM_BOX_IOT_ESP8266_H
#define PIC8_RANDOM_BOX_IOT_ESP8266_H
#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define UART_TX_RETRY_TIMES 50
#define UART_RX_RETRY_TIMES 1000
#define MAX_URL_LEN 128
#define MAX_Q_ARGS 19
#define MAX_QA_KEY_LEN 32
#define MAX_QA_VALUE_LEN 64
#define MAX_PATH_LEN 128
#define MAX_FRAG_LEN 32

typedef enum {
    HTTP_GET,
    HTTP_HEAD,
    HTTP_PUT,
    HTTP_PATCH,
    HTTP_POST,
    HTTP_OPTIONS,
    HTTP_DELETE,
    HTTP_OTHER_METHOD,
} esp8266_http_method;

typedef enum {
    NO_ERROR = 0,
    URL_TOO_LONG = 1,
    INV_CHAR_ON_PATH = 2,
    INV_CHAR_ON_QARGS_KEY = 3,
    INV_CHAR_ON_QARGS_VALUE = 4,
    INV_CHAR_ON_FRAGMENT = 5,
    QA_KEY_TOO_LONG = 6,
    QA_VALUE_TOO_LONG = 7,
    PATH_TOO_LONG = 8,
    FRAG_TOO_LONG = 9,
} esp8266_url_parse_error;

typedef struct {
    char url[MAX_URL_LEN + 3];
    uint8_t path_end;
    uint8_t qa_key_starts[MAX_Q_ARGS + 1];
    uint8_t qa_key_lens[MAX_Q_ARGS + 1];
    uint8_t qa_value_starts[MAX_Q_ARGS + 1];
    uint8_t qa_value_lens[MAX_Q_ARGS + 1];
    uint8_t num_qa;
    uint8_t frag_start;
    uint8_t frag_len;
    esp8266_url_parse_error error;
} ESP8266UrlParts;

typedef struct {
    ESP8266UrlParts url;
    esp8266_http_method method;
    uint8_t link;
} ESP8266Request;

void debug_esp8266_blink(uint8_t times, uint8_t color);

void esp8266_initialize(
        char (*getCh)(void),
        bool (*rxReady)(void),
        void (*putCh)(char),
        bool (*txReady)(void),
        void (*setReset)(bool),
        void (*delayUs)(uint16_t)
);

bool esp8266_setUpAP();

// Low-level commands

bool esp8266_sendUartRaw(const char *str);

bool esp8266_sendUartRawLen(const char *str, uint16_t len);

bool esp8266_sendUartCommand(const char *str);

bool esp8266_waitForReady(void);

bool esp8266_getOk(void);

bool esp8266_waitForTx(void);

bool esp8266_waitForRx(void);

void esp8266_delayMs(uint16_t ms);

bool esp8266_consumeSequence(const char *sequence);

bool esp8266_waitForSequence(const char *sequence, uint16_t max_ms_wait);

// AP commands

bool esp8266_createAccessPoint(const char *ssid, const char *pwd);

bool esp8266_setupConfigAP(const char *ssid);

bool esp8266_isConnectedToAP(void);

bool esp8266_connectToAp(char *ssid, char *pwd);

// HTTP commands

bool esp8266_getReceivedLengthAndLinkID(uint16_t *len, uint8_t *link);

bool esp8266_getHTTPRequest(esp8266_http_method *method, char path[32], uint8_t *link);

void esp8266_404(uint8_t link_id);

void esp8266_405(uint8_t link_id);

void esp8266_400(uint8_t link_id, const char *err, const char *details);

void esp8266_response(uint8_t link_id, const char *status, const char *content_type, const char *body);

bool esp8266_separateUrl(ESP8266UrlParts *url_parts);

uint16_t esp8266_getQArgIdx(const ESP8266UrlParts *url_parts, const char *key);

bool esp8266_hasQArg(const ESP8266UrlParts *url_parts, const char *key);

bool esp8266_getQArgValue(const ESP8266UrlParts *url_parts, const char *key, char *value);

bool esp8266_request(ESP8266Request *request);

#ifdef __cplusplus
}
#endif
#endif // PIC8_RANDOM_BOX_IOT_ESP8266_H
