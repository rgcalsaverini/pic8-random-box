#include "led_lamp.h"

#ifndef __XC8
uint8_t LATC;
uint8_t RB3;
uint8_t RB5;
#define __delay_us(V)
#else
#include <xc.h>
#endif

#ifndef _XTAL_FREQ
#define _XTAL_FREQ 64000000U
#endif

uint8_t led_lamp_l0_value = 0;
uint8_t led_lamp_l0_target = 0;
uint8_t led_lamp_l1_value = 0;
uint8_t led_lamp_l1_target = 0;
uint8_t led_lamp_l2_value = 0;
uint8_t led_lamp_l2_target = 0;
uint16_t sw1_count = 0;
uint16_t sw2_count = 0;
uint16_t sw1_last_press_ticks = 0;
uint16_t sw2_last_press_ticks = 0;
uint16_t sw1_press_len_ticks = 0;
uint16_t sw2_press_len_ticks = 0;
uint16_t led_lamp_tick_idx = 0;

bool sw1_pressed = false;
bool sw2_pressed = false;


void LEDLamp_setLEDRaw(bool l0, bool l1, bool l2) {
    LATC = (l0 << MASK_L0) | (l1 << MASK_L1) | (l2 << MASK_L2);
}

void LEDLamp_setLed(uint8_t l0, uint8_t l1, uint8_t l2) {
    led_lamp_l0_value = led_lamp_l0_target = l0;
    led_lamp_l1_value = led_lamp_l1_target = l1;
    led_lamp_l2_value = led_lamp_l2_target = l2;
}

void LEDLamp_transitionTo(uint8_t l0, uint8_t l1, uint8_t l2) {
    led_lamp_l0_target = l0;
    led_lamp_l1_target = l1;
    led_lamp_l2_target = l2;
}

void LEDLamp_tick(void) {
    LEDLamp_sw1_last_event = LEDLamp_sw2_last_event = SW_NO_CHANGE;
    for (uint8_t i = 0; i < 254; i++) {
        LEDLamp_setLEDRaw(led_lamp_l0_value > i, led_lamp_l1_value > i, led_lamp_l2_value > i);
        __delay_us(1);
    }
    if ((sw1_pressed && RB3 == 1) || (!sw1_pressed && RB3 == 0)) sw1_count++;
    else if (sw1_count > 0) sw1_count--;
    if ((sw2_pressed && RB5 == 1) || (!sw2_pressed && RB5 == 0)) sw2_count++;
    else if (sw2_count > 0) sw2_count--;

    if (sw1_count > PRESS_THRESHOLD && !sw1_pressed) {
        sw1_pressed = true;
        sw1_count = 0;
        sw1_press_len_ticks = 0;
        LEDLamp_sw1_last_event = SW_PRESS_SHORT;
    } else if (sw1_count > PRESS_THRESHOLD && sw1_pressed) {
        sw1_pressed = false;
        sw1_count = 0;
        LEDLamp_sw1_last_event = sw1_press_len_ticks > LONG_PRESS_THRESHOLD ? SW_RELEASE_LONG : SW_RELEASE_SHORT;
    }
    if (sw1_pressed) sw1_press_len_ticks += 1;

    if (sw2_count > PRESS_THRESHOLD && !sw2_pressed) {
        sw2_pressed = true;
        sw2_count = 0;
        sw2_press_len_ticks = 0;
        LEDLamp_sw2_last_event = SW_PRESS_SHORT;
    } else if (sw2_count > PRESS_THRESHOLD && sw2_pressed) {
        sw2_pressed = false;
        sw2_count = 0;
        LEDLamp_sw2_last_event = sw2_press_len_ticks > LONG_PRESS_THRESHOLD ? SW_RELEASE_LONG : SW_RELEASE_SHORT;
    }
    if (sw2_pressed) sw2_press_len_ticks += 1;

    if (sw1_pressed && sw1_press_len_ticks == LONG_PRESS_THRESHOLD + 1) LEDLamp_sw1_last_event = SW_PRESS_LONG;
    if (sw2_pressed && sw2_press_len_ticks == LONG_PRESS_THRESHOLD + 1) LEDLamp_sw2_last_event = SW_PRESS_LONG;

    if (led_lamp_tick_idx % 3 == 0) {
        if (led_lamp_l0_target != led_lamp_l0_value) {
            bool negative = led_lamp_l0_target < led_lamp_l0_value;
            led_lamp_l0_value += negative ? -1 : 1;
        }
        if (led_lamp_l1_target != led_lamp_l1_value) {
            bool negative = led_lamp_l1_target < led_lamp_l1_value;
            led_lamp_l1_value += negative ? -1 : 1;
        }
        if (led_lamp_l2_target != led_lamp_l2_value) {
            bool negative = led_lamp_l1_target < led_lamp_l2_value;
            led_lamp_l2_value += negative ? -1 : 1;
        }
    }
    led_lamp_tick_idx++;
}
