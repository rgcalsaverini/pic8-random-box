#ifndef PIC8_RANDOM_BOX_LED_LAMP_H
#define PIC8_RANDOM_BOX_LED_LAMP_H
#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define PRESS_THRESHOLD 25
#define LONG_PRESS_THRESHOLD 400

#define MASK_L0 0
#define MASK_L1 3
#define MASK_L2 6


typedef enum {
    SW_NO_CHANGE = 0,
    SW_PRESS_SHORT = 1,
    SW_PRESS_LONG = 2,
    SW_RELEASE_SHORT = 3,
    SW_RELEASE_LONG = 4,
} led_lamp_sw_press;

led_lamp_sw_press LEDLamp_sw1_last_event = SW_NO_CHANGE;
led_lamp_sw_press LEDLamp_sw2_last_event = SW_NO_CHANGE;

void LEDLamp_setLed(uint8_t l0, uint8_t l1, uint8_t l2);

void LEDLamp_transitionTo(uint8_t l0, uint8_t l1, uint8_t l2);

void LEDLamp_setLEDRaw(bool l0, bool l1, bool l2);

void LEDLamp_tick(void);


#ifdef __cplusplus
}
#endif
#endif //PIC8_RANDOM_BOX_LED_LAMP_H
